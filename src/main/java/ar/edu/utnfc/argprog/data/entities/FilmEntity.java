/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ar.edu.utnfc.argprog.data.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import jakarta.persistence.Basic;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.Lob;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;

/**
 *
 * @author Felipe
 */
@Entity
@Table(name = "film")
@NamedQueries(
{
    @NamedQuery(name = "FilmEntity.findAll", query = "SELECT f FROM FilmEntity f"),
    @NamedQuery(name = "FilmEntity.findByFilmId", query = "SELECT f FROM FilmEntity f WHERE f.filmId = :filmId"),
    @NamedQuery(name = "FilmEntity.findByTitle", query = "SELECT f FROM FilmEntity f WHERE f.title = :title"),
    @NamedQuery(name = "FilmEntity.findByReleaseYear", query = "SELECT f FROM FilmEntity f WHERE f.releaseYear = :releaseYear"),
    @NamedQuery(name = "FilmEntity.findByRentalDuration", query = "SELECT f FROM FilmEntity f WHERE f.rentalDuration = :rentalDuration"),
    @NamedQuery(name = "FilmEntity.findByRentalRate", query = "SELECT f FROM FilmEntity f WHERE f.rentalRate = :rentalRate"),
    @NamedQuery(name = "FilmEntity.findByLength", query = "SELECT f FROM FilmEntity f WHERE f.length = :length"),
    @NamedQuery(name = "FilmEntity.findByReplacementCost", query = "SELECT f FROM FilmEntity f WHERE f.replacementCost = :replacementCost"),
    @NamedQuery(name = "FilmEntity.findByRating", query = "SELECT f FROM FilmEntity f WHERE f.rating = :rating"),
    @NamedQuery(name = "FilmEntity.findBySpecialFeatures", query = "SELECT f FROM FilmEntity f WHERE f.specialFeatures = :specialFeatures"),
    @NamedQuery(name = "FilmEntity.findByLastUpdate", query = "SELECT f FROM FilmEntity f WHERE f.lastUpdate = :lastUpdate")
})
public class FilmEntity implements Serializable
{
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "film_id")
    private Short filmId;
    @Basic(optional = false)
    private String title;
    @Lob
    private String description;
    @Column(name = "release_year")
    @Temporal(TemporalType.DATE)
    private Date releaseYear;
    @Basic(optional = false)
    @Column(name = "rental_duration")
    private short rentalDuration;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @Column(name = "rental_rate")
    private BigDecimal rentalRate;
    private Short length;
    @Basic(optional = false)
    @Column(name = "replacement_cost")
    private BigDecimal replacementCost;
    private String rating;
    @Column(name = "special_features")
    private String specialFeatures;
    @Basic(optional = false)
    @Column(name = "last_update")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastUpdate;
    @JoinColumn(name = "original_language_id", referencedColumnName = "language_id")
    @ManyToOne
    private LanguageEntity languageEntity;
    @JoinColumn(name = "language_id", referencedColumnName = "language_id")
    @ManyToOne(optional = false)
    private LanguageEntity languageEntity1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "filmEntity")
    private Collection<FilmActorEntity> filmActorEntityCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "filmEntity")
    private Collection<InventoryEntity> inventoryEntityCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "filmEntity")
    private Collection<FilmCategoryEntity> filmCategoryEntityCollection;

    public FilmEntity()
    {
    }

    public FilmEntity(Short filmId)
    {
        this.filmId = filmId;
    }

    public FilmEntity(Short filmId, String title, short rentalDuration, BigDecimal rentalRate, BigDecimal replacementCost, Date lastUpdate)
    {
        this.filmId = filmId;
        this.title = title;
        this.rentalDuration = rentalDuration;
        this.rentalRate = rentalRate;
        this.replacementCost = replacementCost;
        this.lastUpdate = lastUpdate;
    }

    public Short getFilmId()
    {
        return filmId;
    }

    public void setFilmId(Short filmId)
    {
        this.filmId = filmId;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public Date getReleaseYear()
    {
        return releaseYear;
    }

    public void setReleaseYear(Date releaseYear)
    {
        this.releaseYear = releaseYear;
    }

    public short getRentalDuration()
    {
        return rentalDuration;
    }

    public void setRentalDuration(short rentalDuration)
    {
        this.rentalDuration = rentalDuration;
    }

    public BigDecimal getRentalRate()
    {
        return rentalRate;
    }

    public void setRentalRate(BigDecimal rentalRate)
    {
        this.rentalRate = rentalRate;
    }

    public Short getLength()
    {
        return length;
    }

    public void setLength(Short length)
    {
        this.length = length;
    }

    public BigDecimal getReplacementCost()
    {
        return replacementCost;
    }

    public void setReplacementCost(BigDecimal replacementCost)
    {
        this.replacementCost = replacementCost;
    }

    public String getRating()
    {
        return rating;
    }

    public void setRating(String rating)
    {
        this.rating = rating;
    }

    public String getSpecialFeatures()
    {
        return specialFeatures;
    }

    public void setSpecialFeatures(String specialFeatures)
    {
        this.specialFeatures = specialFeatures;
    }

    public Date getLastUpdate()
    {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate)
    {
        this.lastUpdate = lastUpdate;
    }

    public LanguageEntity getLanguageEntity()
    {
        return languageEntity;
    }

    public void setLanguageEntity(LanguageEntity languageEntity)
    {
        this.languageEntity = languageEntity;
    }

    public LanguageEntity getLanguageEntity1()
    {
        return languageEntity1;
    }

    public void setLanguageEntity1(LanguageEntity languageEntity1)
    {
        this.languageEntity1 = languageEntity1;
    }

    public Collection<FilmActorEntity> getFilmActorEntityCollection()
    {
        return filmActorEntityCollection;
    }

    public void setFilmActorEntityCollection(Collection<FilmActorEntity> filmActorEntityCollection)
    {
        this.filmActorEntityCollection = filmActorEntityCollection;
    }

    public Collection<InventoryEntity> getInventoryEntityCollection()
    {
        return inventoryEntityCollection;
    }

    public void setInventoryEntityCollection(Collection<InventoryEntity> inventoryEntityCollection)
    {
        this.inventoryEntityCollection = inventoryEntityCollection;
    }

    public Collection<FilmCategoryEntity> getFilmCategoryEntityCollection()
    {
        return filmCategoryEntityCollection;
    }

    public void setFilmCategoryEntityCollection(Collection<FilmCategoryEntity> filmCategoryEntityCollection)
    {
        this.filmCategoryEntityCollection = filmCategoryEntityCollection;
    }

    @Override
    public int hashCode()
    {
        int hash = 0;
        hash += (filmId != null ? filmId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object)
    {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FilmEntity))
        {
            return false;
        }
        FilmEntity other = (FilmEntity) object;
        if ((this.filmId == null && other.filmId != null) || (this.filmId != null && !this.filmId.equals(other.filmId)))
        {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return "ar.edu.utnfc.argprog.data.entities.FilmEntity[ filmId=" + filmId + " ]";
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ar.edu.utnfc.argprog.data.daos;

import ar.edu.utnfc.argprog.data.daos.exceptions.IllegalOrphanException;
import ar.edu.utnfc.argprog.data.daos.exceptions.NonexistentEntityException;
import java.io.Serializable;
import jakarta.persistence.Query;
import jakarta.persistence.EntityNotFoundException;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Root;
import ar.edu.utnfc.argprog.data.entities.LanguageEntity;
import ar.edu.utnfc.argprog.data.entities.FilmActorEntity;
import java.util.ArrayList;
import java.util.Collection;
import ar.edu.utnfc.argprog.data.entities.InventoryEntity;
import ar.edu.utnfc.argprog.data.entities.FilmCategoryEntity;
import ar.edu.utnfc.argprog.data.entities.FilmEntity;
import java.util.List;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;

/**
 *
 * @author Felipe
 */
public class FilmDao implements Serializable
{

    public FilmDao(EntityManagerFactory emf)
    {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager()
    {
        return emf.createEntityManager();
    }

    public void create(FilmEntity filmEntity)
    {
        if (filmEntity.getFilmActorEntityCollection() == null)
        {
            filmEntity.setFilmActorEntityCollection(new ArrayList<FilmActorEntity>());
        }
        if (filmEntity.getInventoryEntityCollection() == null)
        {
            filmEntity.setInventoryEntityCollection(new ArrayList<InventoryEntity>());
        }
        if (filmEntity.getFilmCategoryEntityCollection() == null)
        {
            filmEntity.setFilmCategoryEntityCollection(new ArrayList<FilmCategoryEntity>());
        }
        EntityManager em = null;
        try
        {
            em = getEntityManager();
            em.getTransaction().begin();
            LanguageEntity languageEntity = filmEntity.getLanguageEntity();
            if (languageEntity != null)
            {
                languageEntity = em.getReference(languageEntity.getClass(), languageEntity.getLanguageId());
                filmEntity.setLanguageEntity(languageEntity);
            }
            LanguageEntity languageEntity1 = filmEntity.getLanguageEntity1();
            if (languageEntity1 != null)
            {
                languageEntity1 = em.getReference(languageEntity1.getClass(), languageEntity1.getLanguageId());
                filmEntity.setLanguageEntity1(languageEntity1);
            }
            Collection<FilmActorEntity> attachedFilmActorEntityCollection = new ArrayList<FilmActorEntity>();
            for (FilmActorEntity filmActorEntityCollectionFilmActorEntityToAttach : filmEntity.getFilmActorEntityCollection())
            {
                filmActorEntityCollectionFilmActorEntityToAttach = em.getReference(filmActorEntityCollectionFilmActorEntityToAttach.getClass(), filmActorEntityCollectionFilmActorEntityToAttach.getFilmActorEntityPK());
                attachedFilmActorEntityCollection.add(filmActorEntityCollectionFilmActorEntityToAttach);
            }
            filmEntity.setFilmActorEntityCollection(attachedFilmActorEntityCollection);
            Collection<InventoryEntity> attachedInventoryEntityCollection = new ArrayList<InventoryEntity>();
            for (InventoryEntity inventoryEntityCollectionInventoryEntityToAttach : filmEntity.getInventoryEntityCollection())
            {
                inventoryEntityCollectionInventoryEntityToAttach = em.getReference(inventoryEntityCollectionInventoryEntityToAttach.getClass(), inventoryEntityCollectionInventoryEntityToAttach.getInventoryId());
                attachedInventoryEntityCollection.add(inventoryEntityCollectionInventoryEntityToAttach);
            }
            filmEntity.setInventoryEntityCollection(attachedInventoryEntityCollection);
            Collection<FilmCategoryEntity> attachedFilmCategoryEntityCollection = new ArrayList<FilmCategoryEntity>();
            for (FilmCategoryEntity filmCategoryEntityCollectionFilmCategoryEntityToAttach : filmEntity.getFilmCategoryEntityCollection())
            {
                filmCategoryEntityCollectionFilmCategoryEntityToAttach = em.getReference(filmCategoryEntityCollectionFilmCategoryEntityToAttach.getClass(), filmCategoryEntityCollectionFilmCategoryEntityToAttach.getFilmCategoryEntityPK());
                attachedFilmCategoryEntityCollection.add(filmCategoryEntityCollectionFilmCategoryEntityToAttach);
            }
            filmEntity.setFilmCategoryEntityCollection(attachedFilmCategoryEntityCollection);
            em.persist(filmEntity);
            if (languageEntity != null)
            {
                languageEntity.getFilmEntityCollection().add(filmEntity);
                languageEntity = em.merge(languageEntity);
            }
            if (languageEntity1 != null)
            {
                languageEntity1.getFilmEntityCollection().add(filmEntity);
                languageEntity1 = em.merge(languageEntity1);
            }
            for (FilmActorEntity filmActorEntityCollectionFilmActorEntity : filmEntity.getFilmActorEntityCollection())
            {
                FilmEntity oldFilmEntityOfFilmActorEntityCollectionFilmActorEntity = filmActorEntityCollectionFilmActorEntity.getFilmEntity();
                filmActorEntityCollectionFilmActorEntity.setFilmEntity(filmEntity);
                filmActorEntityCollectionFilmActorEntity = em.merge(filmActorEntityCollectionFilmActorEntity);
                if (oldFilmEntityOfFilmActorEntityCollectionFilmActorEntity != null)
                {
                    oldFilmEntityOfFilmActorEntityCollectionFilmActorEntity.getFilmActorEntityCollection().remove(filmActorEntityCollectionFilmActorEntity);
                    oldFilmEntityOfFilmActorEntityCollectionFilmActorEntity = em.merge(oldFilmEntityOfFilmActorEntityCollectionFilmActorEntity);
                }
            }
            for (InventoryEntity inventoryEntityCollectionInventoryEntity : filmEntity.getInventoryEntityCollection())
            {
                FilmEntity oldFilmEntityOfInventoryEntityCollectionInventoryEntity = inventoryEntityCollectionInventoryEntity.getFilmEntity();
                inventoryEntityCollectionInventoryEntity.setFilmEntity(filmEntity);
                inventoryEntityCollectionInventoryEntity = em.merge(inventoryEntityCollectionInventoryEntity);
                if (oldFilmEntityOfInventoryEntityCollectionInventoryEntity != null)
                {
                    oldFilmEntityOfInventoryEntityCollectionInventoryEntity.getInventoryEntityCollection().remove(inventoryEntityCollectionInventoryEntity);
                    oldFilmEntityOfInventoryEntityCollectionInventoryEntity = em.merge(oldFilmEntityOfInventoryEntityCollectionInventoryEntity);
                }
            }
            for (FilmCategoryEntity filmCategoryEntityCollectionFilmCategoryEntity : filmEntity.getFilmCategoryEntityCollection())
            {
                FilmEntity oldFilmEntityOfFilmCategoryEntityCollectionFilmCategoryEntity = filmCategoryEntityCollectionFilmCategoryEntity.getFilmEntity();
                filmCategoryEntityCollectionFilmCategoryEntity.setFilmEntity(filmEntity);
                filmCategoryEntityCollectionFilmCategoryEntity = em.merge(filmCategoryEntityCollectionFilmCategoryEntity);
                if (oldFilmEntityOfFilmCategoryEntityCollectionFilmCategoryEntity != null)
                {
                    oldFilmEntityOfFilmCategoryEntityCollectionFilmCategoryEntity.getFilmCategoryEntityCollection().remove(filmCategoryEntityCollectionFilmCategoryEntity);
                    oldFilmEntityOfFilmCategoryEntityCollectionFilmCategoryEntity = em.merge(oldFilmEntityOfFilmCategoryEntityCollectionFilmCategoryEntity);
                }
            }
            em.getTransaction().commit();
        }
        finally
        {
            if (em != null)
            {
                em.close();
            }
        }
    }

    public void edit(FilmEntity filmEntity) throws IllegalOrphanException, NonexistentEntityException, Exception
    {
        EntityManager em = null;
        try
        {
            em = getEntityManager();
            em.getTransaction().begin();
            FilmEntity persistentFilmEntity = em.find(FilmEntity.class, filmEntity.getFilmId());
            LanguageEntity languageEntityOld = persistentFilmEntity.getLanguageEntity();
            LanguageEntity languageEntityNew = filmEntity.getLanguageEntity();
            LanguageEntity languageEntity1Old = persistentFilmEntity.getLanguageEntity1();
            LanguageEntity languageEntity1New = filmEntity.getLanguageEntity1();
            Collection<FilmActorEntity> filmActorEntityCollectionOld = persistentFilmEntity.getFilmActorEntityCollection();
            Collection<FilmActorEntity> filmActorEntityCollectionNew = filmEntity.getFilmActorEntityCollection();
            Collection<InventoryEntity> inventoryEntityCollectionOld = persistentFilmEntity.getInventoryEntityCollection();
            Collection<InventoryEntity> inventoryEntityCollectionNew = filmEntity.getInventoryEntityCollection();
            Collection<FilmCategoryEntity> filmCategoryEntityCollectionOld = persistentFilmEntity.getFilmCategoryEntityCollection();
            Collection<FilmCategoryEntity> filmCategoryEntityCollectionNew = filmEntity.getFilmCategoryEntityCollection();
            List<String> illegalOrphanMessages = null;
            for (FilmActorEntity filmActorEntityCollectionOldFilmActorEntity : filmActorEntityCollectionOld)
            {
                if (!filmActorEntityCollectionNew.contains(filmActorEntityCollectionOldFilmActorEntity))
                {
                    if (illegalOrphanMessages == null)
                    {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain FilmActorEntity " + filmActorEntityCollectionOldFilmActorEntity + " since its filmEntity field is not nullable.");
                }
            }
            for (InventoryEntity inventoryEntityCollectionOldInventoryEntity : inventoryEntityCollectionOld)
            {
                if (!inventoryEntityCollectionNew.contains(inventoryEntityCollectionOldInventoryEntity))
                {
                    if (illegalOrphanMessages == null)
                    {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain InventoryEntity " + inventoryEntityCollectionOldInventoryEntity + " since its filmEntity field is not nullable.");
                }
            }
            for (FilmCategoryEntity filmCategoryEntityCollectionOldFilmCategoryEntity : filmCategoryEntityCollectionOld)
            {
                if (!filmCategoryEntityCollectionNew.contains(filmCategoryEntityCollectionOldFilmCategoryEntity))
                {
                    if (illegalOrphanMessages == null)
                    {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain FilmCategoryEntity " + filmCategoryEntityCollectionOldFilmCategoryEntity + " since its filmEntity field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null)
            {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (languageEntityNew != null)
            {
                languageEntityNew = em.getReference(languageEntityNew.getClass(), languageEntityNew.getLanguageId());
                filmEntity.setLanguageEntity(languageEntityNew);
            }
            if (languageEntity1New != null)
            {
                languageEntity1New = em.getReference(languageEntity1New.getClass(), languageEntity1New.getLanguageId());
                filmEntity.setLanguageEntity1(languageEntity1New);
            }
            Collection<FilmActorEntity> attachedFilmActorEntityCollectionNew = new ArrayList<FilmActorEntity>();
            for (FilmActorEntity filmActorEntityCollectionNewFilmActorEntityToAttach : filmActorEntityCollectionNew)
            {
                filmActorEntityCollectionNewFilmActorEntityToAttach = em.getReference(filmActorEntityCollectionNewFilmActorEntityToAttach.getClass(), filmActorEntityCollectionNewFilmActorEntityToAttach.getFilmActorEntityPK());
                attachedFilmActorEntityCollectionNew.add(filmActorEntityCollectionNewFilmActorEntityToAttach);
            }
            filmActorEntityCollectionNew = attachedFilmActorEntityCollectionNew;
            filmEntity.setFilmActorEntityCollection(filmActorEntityCollectionNew);
            Collection<InventoryEntity> attachedInventoryEntityCollectionNew = new ArrayList<InventoryEntity>();
            for (InventoryEntity inventoryEntityCollectionNewInventoryEntityToAttach : inventoryEntityCollectionNew)
            {
                inventoryEntityCollectionNewInventoryEntityToAttach = em.getReference(inventoryEntityCollectionNewInventoryEntityToAttach.getClass(), inventoryEntityCollectionNewInventoryEntityToAttach.getInventoryId());
                attachedInventoryEntityCollectionNew.add(inventoryEntityCollectionNewInventoryEntityToAttach);
            }
            inventoryEntityCollectionNew = attachedInventoryEntityCollectionNew;
            filmEntity.setInventoryEntityCollection(inventoryEntityCollectionNew);
            Collection<FilmCategoryEntity> attachedFilmCategoryEntityCollectionNew = new ArrayList<FilmCategoryEntity>();
            for (FilmCategoryEntity filmCategoryEntityCollectionNewFilmCategoryEntityToAttach : filmCategoryEntityCollectionNew)
            {
                filmCategoryEntityCollectionNewFilmCategoryEntityToAttach = em.getReference(filmCategoryEntityCollectionNewFilmCategoryEntityToAttach.getClass(), filmCategoryEntityCollectionNewFilmCategoryEntityToAttach.getFilmCategoryEntityPK());
                attachedFilmCategoryEntityCollectionNew.add(filmCategoryEntityCollectionNewFilmCategoryEntityToAttach);
            }
            filmCategoryEntityCollectionNew = attachedFilmCategoryEntityCollectionNew;
            filmEntity.setFilmCategoryEntityCollection(filmCategoryEntityCollectionNew);
            filmEntity = em.merge(filmEntity);
            if (languageEntityOld != null && !languageEntityOld.equals(languageEntityNew))
            {
                languageEntityOld.getFilmEntityCollection().remove(filmEntity);
                languageEntityOld = em.merge(languageEntityOld);
            }
            if (languageEntityNew != null && !languageEntityNew.equals(languageEntityOld))
            {
                languageEntityNew.getFilmEntityCollection().add(filmEntity);
                languageEntityNew = em.merge(languageEntityNew);
            }
            if (languageEntity1Old != null && !languageEntity1Old.equals(languageEntity1New))
            {
                languageEntity1Old.getFilmEntityCollection().remove(filmEntity);
                languageEntity1Old = em.merge(languageEntity1Old);
            }
            if (languageEntity1New != null && !languageEntity1New.equals(languageEntity1Old))
            {
                languageEntity1New.getFilmEntityCollection().add(filmEntity);
                languageEntity1New = em.merge(languageEntity1New);
            }
            for (FilmActorEntity filmActorEntityCollectionNewFilmActorEntity : filmActorEntityCollectionNew)
            {
                if (!filmActorEntityCollectionOld.contains(filmActorEntityCollectionNewFilmActorEntity))
                {
                    FilmEntity oldFilmEntityOfFilmActorEntityCollectionNewFilmActorEntity = filmActorEntityCollectionNewFilmActorEntity.getFilmEntity();
                    filmActorEntityCollectionNewFilmActorEntity.setFilmEntity(filmEntity);
                    filmActorEntityCollectionNewFilmActorEntity = em.merge(filmActorEntityCollectionNewFilmActorEntity);
                    if (oldFilmEntityOfFilmActorEntityCollectionNewFilmActorEntity != null && !oldFilmEntityOfFilmActorEntityCollectionNewFilmActorEntity.equals(filmEntity))
                    {
                        oldFilmEntityOfFilmActorEntityCollectionNewFilmActorEntity.getFilmActorEntityCollection().remove(filmActorEntityCollectionNewFilmActorEntity);
                        oldFilmEntityOfFilmActorEntityCollectionNewFilmActorEntity = em.merge(oldFilmEntityOfFilmActorEntityCollectionNewFilmActorEntity);
                    }
                }
            }
            for (InventoryEntity inventoryEntityCollectionNewInventoryEntity : inventoryEntityCollectionNew)
            {
                if (!inventoryEntityCollectionOld.contains(inventoryEntityCollectionNewInventoryEntity))
                {
                    FilmEntity oldFilmEntityOfInventoryEntityCollectionNewInventoryEntity = inventoryEntityCollectionNewInventoryEntity.getFilmEntity();
                    inventoryEntityCollectionNewInventoryEntity.setFilmEntity(filmEntity);
                    inventoryEntityCollectionNewInventoryEntity = em.merge(inventoryEntityCollectionNewInventoryEntity);
                    if (oldFilmEntityOfInventoryEntityCollectionNewInventoryEntity != null && !oldFilmEntityOfInventoryEntityCollectionNewInventoryEntity.equals(filmEntity))
                    {
                        oldFilmEntityOfInventoryEntityCollectionNewInventoryEntity.getInventoryEntityCollection().remove(inventoryEntityCollectionNewInventoryEntity);
                        oldFilmEntityOfInventoryEntityCollectionNewInventoryEntity = em.merge(oldFilmEntityOfInventoryEntityCollectionNewInventoryEntity);
                    }
                }
            }
            for (FilmCategoryEntity filmCategoryEntityCollectionNewFilmCategoryEntity : filmCategoryEntityCollectionNew)
            {
                if (!filmCategoryEntityCollectionOld.contains(filmCategoryEntityCollectionNewFilmCategoryEntity))
                {
                    FilmEntity oldFilmEntityOfFilmCategoryEntityCollectionNewFilmCategoryEntity = filmCategoryEntityCollectionNewFilmCategoryEntity.getFilmEntity();
                    filmCategoryEntityCollectionNewFilmCategoryEntity.setFilmEntity(filmEntity);
                    filmCategoryEntityCollectionNewFilmCategoryEntity = em.merge(filmCategoryEntityCollectionNewFilmCategoryEntity);
                    if (oldFilmEntityOfFilmCategoryEntityCollectionNewFilmCategoryEntity != null && !oldFilmEntityOfFilmCategoryEntityCollectionNewFilmCategoryEntity.equals(filmEntity))
                    {
                        oldFilmEntityOfFilmCategoryEntityCollectionNewFilmCategoryEntity.getFilmCategoryEntityCollection().remove(filmCategoryEntityCollectionNewFilmCategoryEntity);
                        oldFilmEntityOfFilmCategoryEntityCollectionNewFilmCategoryEntity = em.merge(oldFilmEntityOfFilmCategoryEntityCollectionNewFilmCategoryEntity);
                    }
                }
            }
            em.getTransaction().commit();
        }
        catch (Exception ex)
        {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0)
            {
                Short id = filmEntity.getFilmId();
                if (findFilmEntity(id) == null)
                {
                    throw new NonexistentEntityException("The filmEntity with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
        finally
        {
            if (em != null)
            {
                em.close();
            }
        }
    }

    public void destroy(Short id) throws IllegalOrphanException, NonexistentEntityException
    {
        EntityManager em = null;
        try
        {
            em = getEntityManager();
            em.getTransaction().begin();
            FilmEntity filmEntity;
            try
            {
                filmEntity = em.getReference(FilmEntity.class, id);
                filmEntity.getFilmId();
            }
            catch (EntityNotFoundException enfe)
            {
                throw new NonexistentEntityException("The filmEntity with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<FilmActorEntity> filmActorEntityCollectionOrphanCheck = filmEntity.getFilmActorEntityCollection();
            for (FilmActorEntity filmActorEntityCollectionOrphanCheckFilmActorEntity : filmActorEntityCollectionOrphanCheck)
            {
                if (illegalOrphanMessages == null)
                {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This FilmEntity (" + filmEntity + ") cannot be destroyed since the FilmActorEntity " + filmActorEntityCollectionOrphanCheckFilmActorEntity + " in its filmActorEntityCollection field has a non-nullable filmEntity field.");
            }
            Collection<InventoryEntity> inventoryEntityCollectionOrphanCheck = filmEntity.getInventoryEntityCollection();
            for (InventoryEntity inventoryEntityCollectionOrphanCheckInventoryEntity : inventoryEntityCollectionOrphanCheck)
            {
                if (illegalOrphanMessages == null)
                {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This FilmEntity (" + filmEntity + ") cannot be destroyed since the InventoryEntity " + inventoryEntityCollectionOrphanCheckInventoryEntity + " in its inventoryEntityCollection field has a non-nullable filmEntity field.");
            }
            Collection<FilmCategoryEntity> filmCategoryEntityCollectionOrphanCheck = filmEntity.getFilmCategoryEntityCollection();
            for (FilmCategoryEntity filmCategoryEntityCollectionOrphanCheckFilmCategoryEntity : filmCategoryEntityCollectionOrphanCheck)
            {
                if (illegalOrphanMessages == null)
                {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This FilmEntity (" + filmEntity + ") cannot be destroyed since the FilmCategoryEntity " + filmCategoryEntityCollectionOrphanCheckFilmCategoryEntity + " in its filmCategoryEntityCollection field has a non-nullable filmEntity field.");
            }
            if (illegalOrphanMessages != null)
            {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            LanguageEntity languageEntity = filmEntity.getLanguageEntity();
            if (languageEntity != null)
            {
                languageEntity.getFilmEntityCollection().remove(filmEntity);
                languageEntity = em.merge(languageEntity);
            }
            LanguageEntity languageEntity1 = filmEntity.getLanguageEntity1();
            if (languageEntity1 != null)
            {
                languageEntity1.getFilmEntityCollection().remove(filmEntity);
                languageEntity1 = em.merge(languageEntity1);
            }
            em.remove(filmEntity);
            em.getTransaction().commit();
        }
        finally
        {
            if (em != null)
            {
                em.close();
            }
        }
    }

    public List<FilmEntity> findFilmEntityEntities()
    {
        return findFilmEntityEntities(true, -1, -1);
    }

    public List<FilmEntity> findFilmEntityEntities(int maxResults, int firstResult)
    {
        return findFilmEntityEntities(false, maxResults, firstResult);
    }

    private List<FilmEntity> findFilmEntityEntities(boolean all, int maxResults, int firstResult)
    {
        EntityManager em = getEntityManager();
        try
        {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(FilmEntity.class));
            Query q = em.createQuery(cq);
            if (!all)
            {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        }
        finally
        {
            em.close();
        }
    }

    public FilmEntity findFilmEntity(Short id)
    {
        EntityManager em = getEntityManager();
        try
        {
            return em.find(FilmEntity.class, id);
        }
        finally
        {
            em.close();
        }
    }

    public int getFilmEntityCount()
    {
        EntityManager em = getEntityManager();
        try
        {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<FilmEntity> rt = cq.from(FilmEntity.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        }
        finally
        {
            em.close();
        }
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ar.edu.utnfc.argprog;

import ar.edu.utnfc.argprog.data.daos.LanguageDao;
import ar.edu.utnfc.argprog.data.daos.exceptions.IllegalOrphanException;
import ar.edu.utnfc.argprog.data.daos.exceptions.NonexistentEntityException;
import ar.edu.utnfc.argprog.data.entities.LanguageEntity;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;

/**
 * @author Felipe
 */
public class PruebaDaos {
    public static void main(String[] args) {
        //Para crear obtener el EntityManager en un entorno no administrado
        //usamos el EntityManagerFactory que podemos obtener de la clase
        //Persistence
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("DemoJPAPU");

        LanguageEntity entidad = new LanguageEntity();
        entidad.setName("Español");

        LanguageDao dao = new LanguageDao(emf);
        dao.create(entidad);

        System.out.println("Lista de lenguajes");
        dao.findLanguageEntityEntities()
                .stream()
                .forEach(System.out::println);

        //Manejar el ciclo de vida de las entidades
        try {
            dao.destroy(entidad.getLanguageId());
        }
        catch (IllegalOrphanException ex) {
            Logger.getLogger(PruebaDaos.class.getName()).log(Level.SEVERE, null, ex);
        }
        catch (NonexistentEntityException ex) {
            Logger.getLogger(PruebaDaos.class.getName()).log(Level.SEVERE, null, ex);
        }

        System.out.println("\nLista de lenguajes");
        dao.findLanguageEntityEntities()
                .stream()
                .forEach(System.out::println);


    }
}

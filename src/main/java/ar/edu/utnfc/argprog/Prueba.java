/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ar.edu.utnfc.argprog;


import java.util.List;

import ar.edu.utnfc.argprog.data.entities.LanguageEntity;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.ParameterMode;
import jakarta.persistence.Persistence;
import jakarta.persistence.StoredProcedureQuery;

/**
 * @author Felipe
 */
public class Prueba {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //Para crear obtener el EntityManager en un entorno no administrado
        //usamos el EntityManagerFactory que podemos obtener de la clase
        //Persistence
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("DemoJPAPU");

        // Dicha clase me permite fabricar el EntityManager
        EntityManager em = emf.createEntityManager();
        System.err.println("Clase concreta del EM: " + em.getClass());

        List<LanguageEntity> listaLenguajes = em.createNamedQuery("LanguageEntity.findAll")
                                                    .getResultList();
        System.out.println("Lista de lenguajes antes del insert");
        listaLenguajes.stream().forEach(System.out::println);

        // A partir del EntityManager puedo obtener y administrar entidades

        LanguageEntity entidad = new LanguageEntity();
        entidad.setName("Esperanto");

        em.getTransaction().begin();
        em.persist(entidad);
        //em.flush();
        em.getTransaction().commit();
        System.out.println("Lenguaje creado: " + entidad);

        System.out.println("\nListado después de Crear");
        em.createNamedQuery("LanguageEntity.findAll")
                .getResultList()
                .stream()
                .forEach(System.out::println);

        // Modificación
        em.detach(entidad);
        entidad.setName("Esperanto(modi)");

        entidad = em.merge(entidad);

        em.getTransaction().begin();
        em.persist(entidad);
        em.getTransaction().commit();

        System.out.println("\nListado después de Modificar");
        em.createNamedQuery("LanguageEntity.findAll")
                .getResultList()
                .stream()
                .forEach(System.out::println);

        //Manejar el ciclo de vida de las entidades
        em.getTransaction().begin();
        em.remove(entidad);
        em.flush();
        em.getTransaction().commit();

        System.out.println("\nListado después de Borrar");
        em.createNamedQuery("LanguageEntity.findAll")
                .getResultList()
                .stream()
                .forEach(System.out::println);


        //Ejecución de Procedimientos Almacenados
        //Configuro la invocación del sp
        StoredProcedureQuery filmInStock = em.createStoredProcedureQuery("film_in_stock");
        filmInStock.registerStoredProcedureParameter("p_film_id", Integer.class, ParameterMode.IN);
        filmInStock.registerStoredProcedureParameter("p_store_id", Integer.class, ParameterMode.IN);
        filmInStock.registerStoredProcedureParameter("p_film_count", Integer.class, ParameterMode.OUT);

        //Establezco los valores de entrada para los prámetros
        filmInStock.setParameter("p_film_id", 1);
        filmInStock.setParameter("p_store_id", 1);

        //Ejecuto
        filmInStock.execute();

        //Consumo el parámetro de salida
        System.out.println("\nResultado de ejecución del sp: " + filmInStock.getOutputParameterValue("p_film_count").toString());

        em.close();
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ar.edu.utnfc.argprog.data.daos;

import ar.edu.utnfc.argprog.data.daos.exceptions.IllegalOrphanException;
import ar.edu.utnfc.argprog.data.daos.exceptions.NonexistentEntityException;
import java.io.Serializable;
import jakarta.persistence.Query;
import jakarta.persistence.EntityNotFoundException;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Root;
import ar.edu.utnfc.argprog.data.entities.AddressEntity;
import ar.edu.utnfc.argprog.data.entities.StaffEntity;
import ar.edu.utnfc.argprog.data.entities.CustomerEntity;
import java.util.ArrayList;
import java.util.Collection;
import ar.edu.utnfc.argprog.data.entities.InventoryEntity;
import ar.edu.utnfc.argprog.data.entities.StoreEntity;
import java.util.List;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;

/**
 *
 * @author Felipe
 */
public class StoreDao implements Serializable
{

    public StoreDao(EntityManagerFactory emf)
    {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager()
    {
        return emf.createEntityManager();
    }

    public void create(StoreEntity storeEntity) throws IllegalOrphanException
    {
        if (storeEntity.getCustomerEntityCollection() == null)
        {
            storeEntity.setCustomerEntityCollection(new ArrayList<CustomerEntity>());
        }
        if (storeEntity.getStaffEntityCollection() == null)
        {
            storeEntity.setStaffEntityCollection(new ArrayList<StaffEntity>());
        }
        if (storeEntity.getInventoryEntityCollection() == null)
        {
            storeEntity.setInventoryEntityCollection(new ArrayList<InventoryEntity>());
        }
        List<String> illegalOrphanMessages = null;
        StaffEntity staffEntityOrphanCheck = storeEntity.getStaffEntity();
        if (staffEntityOrphanCheck != null)
        {
            StoreEntity oldStoreEntityOfStaffEntity = staffEntityOrphanCheck.getStoreEntity();
            if (oldStoreEntityOfStaffEntity != null)
            {
                if (illegalOrphanMessages == null)
                {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("The StaffEntity " + staffEntityOrphanCheck + " already has an item of type StoreEntity whose staffEntity column cannot be null. Please make another selection for the staffEntity field.");
            }
        }
        if (illegalOrphanMessages != null)
        {
            throw new IllegalOrphanException(illegalOrphanMessages);
        }
        EntityManager em = null;
        try
        {
            em = getEntityManager();
            em.getTransaction().begin();
            AddressEntity addressEntity = storeEntity.getAddressEntity();
            if (addressEntity != null)
            {
                addressEntity = em.getReference(addressEntity.getClass(), addressEntity.getAddressId());
                storeEntity.setAddressEntity(addressEntity);
            }
            StaffEntity staffEntity = storeEntity.getStaffEntity();
            if (staffEntity != null)
            {
                staffEntity = em.getReference(staffEntity.getClass(), staffEntity.getStaffId());
                storeEntity.setStaffEntity(staffEntity);
            }
            Collection<CustomerEntity> attachedCustomerEntityCollection = new ArrayList<CustomerEntity>();
            for (CustomerEntity customerEntityCollectionCustomerEntityToAttach : storeEntity.getCustomerEntityCollection())
            {
                customerEntityCollectionCustomerEntityToAttach = em.getReference(customerEntityCollectionCustomerEntityToAttach.getClass(), customerEntityCollectionCustomerEntityToAttach.getCustomerId());
                attachedCustomerEntityCollection.add(customerEntityCollectionCustomerEntityToAttach);
            }
            storeEntity.setCustomerEntityCollection(attachedCustomerEntityCollection);
            Collection<StaffEntity> attachedStaffEntityCollection = new ArrayList<StaffEntity>();
            for (StaffEntity staffEntityCollectionStaffEntityToAttach : storeEntity.getStaffEntityCollection())
            {
                staffEntityCollectionStaffEntityToAttach = em.getReference(staffEntityCollectionStaffEntityToAttach.getClass(), staffEntityCollectionStaffEntityToAttach.getStaffId());
                attachedStaffEntityCollection.add(staffEntityCollectionStaffEntityToAttach);
            }
            storeEntity.setStaffEntityCollection(attachedStaffEntityCollection);
            Collection<InventoryEntity> attachedInventoryEntityCollection = new ArrayList<InventoryEntity>();
            for (InventoryEntity inventoryEntityCollectionInventoryEntityToAttach : storeEntity.getInventoryEntityCollection())
            {
                inventoryEntityCollectionInventoryEntityToAttach = em.getReference(inventoryEntityCollectionInventoryEntityToAttach.getClass(), inventoryEntityCollectionInventoryEntityToAttach.getInventoryId());
                attachedInventoryEntityCollection.add(inventoryEntityCollectionInventoryEntityToAttach);
            }
            storeEntity.setInventoryEntityCollection(attachedInventoryEntityCollection);
            em.persist(storeEntity);
            if (addressEntity != null)
            {
                addressEntity.getStoreEntityCollection().add(storeEntity);
                addressEntity = em.merge(addressEntity);
            }
            if (staffEntity != null)
            {
                staffEntity.setStoreEntity(storeEntity);
                staffEntity = em.merge(staffEntity);
            }
            for (CustomerEntity customerEntityCollectionCustomerEntity : storeEntity.getCustomerEntityCollection())
            {
                StoreEntity oldStoreEntityOfCustomerEntityCollectionCustomerEntity = customerEntityCollectionCustomerEntity.getStoreEntity();
                customerEntityCollectionCustomerEntity.setStoreEntity(storeEntity);
                customerEntityCollectionCustomerEntity = em.merge(customerEntityCollectionCustomerEntity);
                if (oldStoreEntityOfCustomerEntityCollectionCustomerEntity != null)
                {
                    oldStoreEntityOfCustomerEntityCollectionCustomerEntity.getCustomerEntityCollection().remove(customerEntityCollectionCustomerEntity);
                    oldStoreEntityOfCustomerEntityCollectionCustomerEntity = em.merge(oldStoreEntityOfCustomerEntityCollectionCustomerEntity);
                }
            }
            for (StaffEntity staffEntityCollectionStaffEntity : storeEntity.getStaffEntityCollection())
            {
                StoreEntity oldStoreEntity1OfStaffEntityCollectionStaffEntity = staffEntityCollectionStaffEntity.getStoreEntity1();
                staffEntityCollectionStaffEntity.setStoreEntity1(storeEntity);
                staffEntityCollectionStaffEntity = em.merge(staffEntityCollectionStaffEntity);
                if (oldStoreEntity1OfStaffEntityCollectionStaffEntity != null)
                {
                    oldStoreEntity1OfStaffEntityCollectionStaffEntity.getStaffEntityCollection().remove(staffEntityCollectionStaffEntity);
                    oldStoreEntity1OfStaffEntityCollectionStaffEntity = em.merge(oldStoreEntity1OfStaffEntityCollectionStaffEntity);
                }
            }
            for (InventoryEntity inventoryEntityCollectionInventoryEntity : storeEntity.getInventoryEntityCollection())
            {
                StoreEntity oldStoreEntityOfInventoryEntityCollectionInventoryEntity = inventoryEntityCollectionInventoryEntity.getStoreEntity();
                inventoryEntityCollectionInventoryEntity.setStoreEntity(storeEntity);
                inventoryEntityCollectionInventoryEntity = em.merge(inventoryEntityCollectionInventoryEntity);
                if (oldStoreEntityOfInventoryEntityCollectionInventoryEntity != null)
                {
                    oldStoreEntityOfInventoryEntityCollectionInventoryEntity.getInventoryEntityCollection().remove(inventoryEntityCollectionInventoryEntity);
                    oldStoreEntityOfInventoryEntityCollectionInventoryEntity = em.merge(oldStoreEntityOfInventoryEntityCollectionInventoryEntity);
                }
            }
            em.getTransaction().commit();
        }
        finally
        {
            if (em != null)
            {
                em.close();
            }
        }
    }

    public void edit(StoreEntity storeEntity) throws IllegalOrphanException, NonexistentEntityException, Exception
    {
        EntityManager em = null;
        try
        {
            em = getEntityManager();
            em.getTransaction().begin();
            StoreEntity persistentStoreEntity = em.find(StoreEntity.class, storeEntity.getStoreId());
            AddressEntity addressEntityOld = persistentStoreEntity.getAddressEntity();
            AddressEntity addressEntityNew = storeEntity.getAddressEntity();
            StaffEntity staffEntityOld = persistentStoreEntity.getStaffEntity();
            StaffEntity staffEntityNew = storeEntity.getStaffEntity();
            Collection<CustomerEntity> customerEntityCollectionOld = persistentStoreEntity.getCustomerEntityCollection();
            Collection<CustomerEntity> customerEntityCollectionNew = storeEntity.getCustomerEntityCollection();
            Collection<StaffEntity> staffEntityCollectionOld = persistentStoreEntity.getStaffEntityCollection();
            Collection<StaffEntity> staffEntityCollectionNew = storeEntity.getStaffEntityCollection();
            Collection<InventoryEntity> inventoryEntityCollectionOld = persistentStoreEntity.getInventoryEntityCollection();
            Collection<InventoryEntity> inventoryEntityCollectionNew = storeEntity.getInventoryEntityCollection();
            List<String> illegalOrphanMessages = null;
            if (staffEntityNew != null && !staffEntityNew.equals(staffEntityOld))
            {
                StoreEntity oldStoreEntityOfStaffEntity = staffEntityNew.getStoreEntity();
                if (oldStoreEntityOfStaffEntity != null)
                {
                    if (illegalOrphanMessages == null)
                    {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("The StaffEntity " + staffEntityNew + " already has an item of type StoreEntity whose staffEntity column cannot be null. Please make another selection for the staffEntity field.");
                }
            }
            for (CustomerEntity customerEntityCollectionOldCustomerEntity : customerEntityCollectionOld)
            {
                if (!customerEntityCollectionNew.contains(customerEntityCollectionOldCustomerEntity))
                {
                    if (illegalOrphanMessages == null)
                    {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain CustomerEntity " + customerEntityCollectionOldCustomerEntity + " since its storeEntity field is not nullable.");
                }
            }
            for (StaffEntity staffEntityCollectionOldStaffEntity : staffEntityCollectionOld)
            {
                if (!staffEntityCollectionNew.contains(staffEntityCollectionOldStaffEntity))
                {
                    if (illegalOrphanMessages == null)
                    {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain StaffEntity " + staffEntityCollectionOldStaffEntity + " since its storeEntity1 field is not nullable.");
                }
            }
            for (InventoryEntity inventoryEntityCollectionOldInventoryEntity : inventoryEntityCollectionOld)
            {
                if (!inventoryEntityCollectionNew.contains(inventoryEntityCollectionOldInventoryEntity))
                {
                    if (illegalOrphanMessages == null)
                    {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain InventoryEntity " + inventoryEntityCollectionOldInventoryEntity + " since its storeEntity field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null)
            {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (addressEntityNew != null)
            {
                addressEntityNew = em.getReference(addressEntityNew.getClass(), addressEntityNew.getAddressId());
                storeEntity.setAddressEntity(addressEntityNew);
            }
            if (staffEntityNew != null)
            {
                staffEntityNew = em.getReference(staffEntityNew.getClass(), staffEntityNew.getStaffId());
                storeEntity.setStaffEntity(staffEntityNew);
            }
            Collection<CustomerEntity> attachedCustomerEntityCollectionNew = new ArrayList<CustomerEntity>();
            for (CustomerEntity customerEntityCollectionNewCustomerEntityToAttach : customerEntityCollectionNew)
            {
                customerEntityCollectionNewCustomerEntityToAttach = em.getReference(customerEntityCollectionNewCustomerEntityToAttach.getClass(), customerEntityCollectionNewCustomerEntityToAttach.getCustomerId());
                attachedCustomerEntityCollectionNew.add(customerEntityCollectionNewCustomerEntityToAttach);
            }
            customerEntityCollectionNew = attachedCustomerEntityCollectionNew;
            storeEntity.setCustomerEntityCollection(customerEntityCollectionNew);
            Collection<StaffEntity> attachedStaffEntityCollectionNew = new ArrayList<StaffEntity>();
            for (StaffEntity staffEntityCollectionNewStaffEntityToAttach : staffEntityCollectionNew)
            {
                staffEntityCollectionNewStaffEntityToAttach = em.getReference(staffEntityCollectionNewStaffEntityToAttach.getClass(), staffEntityCollectionNewStaffEntityToAttach.getStaffId());
                attachedStaffEntityCollectionNew.add(staffEntityCollectionNewStaffEntityToAttach);
            }
            staffEntityCollectionNew = attachedStaffEntityCollectionNew;
            storeEntity.setStaffEntityCollection(staffEntityCollectionNew);
            Collection<InventoryEntity> attachedInventoryEntityCollectionNew = new ArrayList<InventoryEntity>();
            for (InventoryEntity inventoryEntityCollectionNewInventoryEntityToAttach : inventoryEntityCollectionNew)
            {
                inventoryEntityCollectionNewInventoryEntityToAttach = em.getReference(inventoryEntityCollectionNewInventoryEntityToAttach.getClass(), inventoryEntityCollectionNewInventoryEntityToAttach.getInventoryId());
                attachedInventoryEntityCollectionNew.add(inventoryEntityCollectionNewInventoryEntityToAttach);
            }
            inventoryEntityCollectionNew = attachedInventoryEntityCollectionNew;
            storeEntity.setInventoryEntityCollection(inventoryEntityCollectionNew);
            storeEntity = em.merge(storeEntity);
            if (addressEntityOld != null && !addressEntityOld.equals(addressEntityNew))
            {
                addressEntityOld.getStoreEntityCollection().remove(storeEntity);
                addressEntityOld = em.merge(addressEntityOld);
            }
            if (addressEntityNew != null && !addressEntityNew.equals(addressEntityOld))
            {
                addressEntityNew.getStoreEntityCollection().add(storeEntity);
                addressEntityNew = em.merge(addressEntityNew);
            }
            if (staffEntityOld != null && !staffEntityOld.equals(staffEntityNew))
            {
                staffEntityOld.setStoreEntity(null);
                staffEntityOld = em.merge(staffEntityOld);
            }
            if (staffEntityNew != null && !staffEntityNew.equals(staffEntityOld))
            {
                staffEntityNew.setStoreEntity(storeEntity);
                staffEntityNew = em.merge(staffEntityNew);
            }
            for (CustomerEntity customerEntityCollectionNewCustomerEntity : customerEntityCollectionNew)
            {
                if (!customerEntityCollectionOld.contains(customerEntityCollectionNewCustomerEntity))
                {
                    StoreEntity oldStoreEntityOfCustomerEntityCollectionNewCustomerEntity = customerEntityCollectionNewCustomerEntity.getStoreEntity();
                    customerEntityCollectionNewCustomerEntity.setStoreEntity(storeEntity);
                    customerEntityCollectionNewCustomerEntity = em.merge(customerEntityCollectionNewCustomerEntity);
                    if (oldStoreEntityOfCustomerEntityCollectionNewCustomerEntity != null && !oldStoreEntityOfCustomerEntityCollectionNewCustomerEntity.equals(storeEntity))
                    {
                        oldStoreEntityOfCustomerEntityCollectionNewCustomerEntity.getCustomerEntityCollection().remove(customerEntityCollectionNewCustomerEntity);
                        oldStoreEntityOfCustomerEntityCollectionNewCustomerEntity = em.merge(oldStoreEntityOfCustomerEntityCollectionNewCustomerEntity);
                    }
                }
            }
            for (StaffEntity staffEntityCollectionNewStaffEntity : staffEntityCollectionNew)
            {
                if (!staffEntityCollectionOld.contains(staffEntityCollectionNewStaffEntity))
                {
                    StoreEntity oldStoreEntity1OfStaffEntityCollectionNewStaffEntity = staffEntityCollectionNewStaffEntity.getStoreEntity1();
                    staffEntityCollectionNewStaffEntity.setStoreEntity1(storeEntity);
                    staffEntityCollectionNewStaffEntity = em.merge(staffEntityCollectionNewStaffEntity);
                    if (oldStoreEntity1OfStaffEntityCollectionNewStaffEntity != null && !oldStoreEntity1OfStaffEntityCollectionNewStaffEntity.equals(storeEntity))
                    {
                        oldStoreEntity1OfStaffEntityCollectionNewStaffEntity.getStaffEntityCollection().remove(staffEntityCollectionNewStaffEntity);
                        oldStoreEntity1OfStaffEntityCollectionNewStaffEntity = em.merge(oldStoreEntity1OfStaffEntityCollectionNewStaffEntity);
                    }
                }
            }
            for (InventoryEntity inventoryEntityCollectionNewInventoryEntity : inventoryEntityCollectionNew)
            {
                if (!inventoryEntityCollectionOld.contains(inventoryEntityCollectionNewInventoryEntity))
                {
                    StoreEntity oldStoreEntityOfInventoryEntityCollectionNewInventoryEntity = inventoryEntityCollectionNewInventoryEntity.getStoreEntity();
                    inventoryEntityCollectionNewInventoryEntity.setStoreEntity(storeEntity);
                    inventoryEntityCollectionNewInventoryEntity = em.merge(inventoryEntityCollectionNewInventoryEntity);
                    if (oldStoreEntityOfInventoryEntityCollectionNewInventoryEntity != null && !oldStoreEntityOfInventoryEntityCollectionNewInventoryEntity.equals(storeEntity))
                    {
                        oldStoreEntityOfInventoryEntityCollectionNewInventoryEntity.getInventoryEntityCollection().remove(inventoryEntityCollectionNewInventoryEntity);
                        oldStoreEntityOfInventoryEntityCollectionNewInventoryEntity = em.merge(oldStoreEntityOfInventoryEntityCollectionNewInventoryEntity);
                    }
                }
            }
            em.getTransaction().commit();
        }
        catch (Exception ex)
        {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0)
            {
                Short id = storeEntity.getStoreId();
                if (findStoreEntity(id) == null)
                {
                    throw new NonexistentEntityException("The storeEntity with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
        finally
        {
            if (em != null)
            {
                em.close();
            }
        }
    }

    public void destroy(Short id) throws IllegalOrphanException, NonexistentEntityException
    {
        EntityManager em = null;
        try
        {
            em = getEntityManager();
            em.getTransaction().begin();
            StoreEntity storeEntity;
            try
            {
                storeEntity = em.getReference(StoreEntity.class, id);
                storeEntity.getStoreId();
            }
            catch (EntityNotFoundException enfe)
            {
                throw new NonexistentEntityException("The storeEntity with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<CustomerEntity> customerEntityCollectionOrphanCheck = storeEntity.getCustomerEntityCollection();
            for (CustomerEntity customerEntityCollectionOrphanCheckCustomerEntity : customerEntityCollectionOrphanCheck)
            {
                if (illegalOrphanMessages == null)
                {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This StoreEntity (" + storeEntity + ") cannot be destroyed since the CustomerEntity " + customerEntityCollectionOrphanCheckCustomerEntity + " in its customerEntityCollection field has a non-nullable storeEntity field.");
            }
            Collection<StaffEntity> staffEntityCollectionOrphanCheck = storeEntity.getStaffEntityCollection();
            for (StaffEntity staffEntityCollectionOrphanCheckStaffEntity : staffEntityCollectionOrphanCheck)
            {
                if (illegalOrphanMessages == null)
                {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This StoreEntity (" + storeEntity + ") cannot be destroyed since the StaffEntity " + staffEntityCollectionOrphanCheckStaffEntity + " in its staffEntityCollection field has a non-nullable storeEntity1 field.");
            }
            Collection<InventoryEntity> inventoryEntityCollectionOrphanCheck = storeEntity.getInventoryEntityCollection();
            for (InventoryEntity inventoryEntityCollectionOrphanCheckInventoryEntity : inventoryEntityCollectionOrphanCheck)
            {
                if (illegalOrphanMessages == null)
                {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This StoreEntity (" + storeEntity + ") cannot be destroyed since the InventoryEntity " + inventoryEntityCollectionOrphanCheckInventoryEntity + " in its inventoryEntityCollection field has a non-nullable storeEntity field.");
            }
            if (illegalOrphanMessages != null)
            {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            AddressEntity addressEntity = storeEntity.getAddressEntity();
            if (addressEntity != null)
            {
                addressEntity.getStoreEntityCollection().remove(storeEntity);
                addressEntity = em.merge(addressEntity);
            }
            StaffEntity staffEntity = storeEntity.getStaffEntity();
            if (staffEntity != null)
            {
                staffEntity.setStoreEntity(null);
                staffEntity = em.merge(staffEntity);
            }
            em.remove(storeEntity);
            em.getTransaction().commit();
        }
        finally
        {
            if (em != null)
            {
                em.close();
            }
        }
    }

    public List<StoreEntity> findStoreEntityEntities()
    {
        return findStoreEntityEntities(true, -1, -1);
    }

    public List<StoreEntity> findStoreEntityEntities(int maxResults, int firstResult)
    {
        return findStoreEntityEntities(false, maxResults, firstResult);
    }

    private List<StoreEntity> findStoreEntityEntities(boolean all, int maxResults, int firstResult)
    {
        EntityManager em = getEntityManager();
        try
        {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(StoreEntity.class));
            Query q = em.createQuery(cq);
            if (!all)
            {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        }
        finally
        {
            em.close();
        }
    }

    public StoreEntity findStoreEntity(Short id)
    {
        EntityManager em = getEntityManager();
        try
        {
            return em.find(StoreEntity.class, id);
        }
        finally
        {
            em.close();
        }
    }

    public int getStoreEntityCount()
    {
        EntityManager em = getEntityManager();
        try
        {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<StoreEntity> rt = cq.from(StoreEntity.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        }
        finally
        {
            em.close();
        }
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ar.edu.utnfc.argprog.data.daos;

import ar.edu.utnfc.argprog.data.daos.exceptions.NonexistentEntityException;
import ar.edu.utnfc.argprog.data.daos.exceptions.PreexistingEntityException;
import ar.edu.utnfc.argprog.data.entities.FilmTextEntity;
import java.io.Serializable;
import java.util.List;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Query;
import jakarta.persistence.EntityNotFoundException;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Root;

/**
 *
 * @author Felipe
 */
public class FilmTextDao implements Serializable
{

    public FilmTextDao(EntityManagerFactory emf)
    {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager()
    {
        return emf.createEntityManager();
    }

    public void create(FilmTextEntity filmTextEntity) throws PreexistingEntityException, Exception
    {
        EntityManager em = null;
        try
        {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(filmTextEntity);
            em.getTransaction().commit();
        }
        catch (Exception ex)
        {
            if (findFilmTextEntity(filmTextEntity.getFilmId()) != null)
            {
                throw new PreexistingEntityException("FilmTextEntity " + filmTextEntity + " already exists.", ex);
            }
            throw ex;
        }
        finally
        {
            if (em != null)
            {
                em.close();
            }
        }
    }

    public void edit(FilmTextEntity filmTextEntity) throws NonexistentEntityException, Exception
    {
        EntityManager em = null;
        try
        {
            em = getEntityManager();
            em.getTransaction().begin();
            filmTextEntity = em.merge(filmTextEntity);
            em.getTransaction().commit();
        }
        catch (Exception ex)
        {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0)
            {
                Short id = filmTextEntity.getFilmId();
                if (findFilmTextEntity(id) == null)
                {
                    throw new NonexistentEntityException("The filmTextEntity with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
        finally
        {
            if (em != null)
            {
                em.close();
            }
        }
    }

    public void destroy(Short id) throws NonexistentEntityException
    {
        EntityManager em = null;
        try
        {
            em = getEntityManager();
            em.getTransaction().begin();
            FilmTextEntity filmTextEntity;
            try
            {
                filmTextEntity = em.getReference(FilmTextEntity.class, id);
                filmTextEntity.getFilmId();
            }
            catch (EntityNotFoundException enfe)
            {
                throw new NonexistentEntityException("The filmTextEntity with id " + id + " no longer exists.", enfe);
            }
            em.remove(filmTextEntity);
            em.getTransaction().commit();
        }
        finally
        {
            if (em != null)
            {
                em.close();
            }
        }
    }

    public List<FilmTextEntity> findFilmTextEntityEntities()
    {
        return findFilmTextEntityEntities(true, -1, -1);
    }

    public List<FilmTextEntity> findFilmTextEntityEntities(int maxResults, int firstResult)
    {
        return findFilmTextEntityEntities(false, maxResults, firstResult);
    }

    private List<FilmTextEntity> findFilmTextEntityEntities(boolean all, int maxResults, int firstResult)
    {
        EntityManager em = getEntityManager();
        try
        {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(FilmTextEntity.class));
            Query q = em.createQuery(cq);
            if (!all)
            {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        }
        finally
        {
            em.close();
        }
    }

    public FilmTextEntity findFilmTextEntity(Short id)
    {
        EntityManager em = getEntityManager();
        try
        {
            return em.find(FilmTextEntity.class, id);
        }
        finally
        {
            em.close();
        }
    }

    public int getFilmTextEntityCount()
    {
        EntityManager em = getEntityManager();
        try
        {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<FilmTextEntity> rt = cq.from(FilmTextEntity.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        }
        finally
        {
            em.close();
        }
    }
    
}

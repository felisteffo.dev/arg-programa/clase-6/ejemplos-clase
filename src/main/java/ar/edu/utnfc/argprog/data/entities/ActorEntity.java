/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ar.edu.utnfc.argprog.data.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import jakarta.persistence.Basic;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;


/**
 *
 * @author Felipe
 */
@Entity
@Table(name = "actor")
@NamedQueries(
{
    @NamedQuery(name = "ActorEntity.findAll", query = "SELECT a FROM ActorEntity a"),
    @NamedQuery(name = "ActorEntity.findByActorId", query = "SELECT a FROM ActorEntity a WHERE a.actorId = :actorId"),
    @NamedQuery(name = "ActorEntity.findByFirstName", query = "SELECT a FROM ActorEntity a WHERE a.firstName = :firstName"),
    @NamedQuery(name = "ActorEntity.findByLastName", query = "SELECT a FROM ActorEntity a WHERE a.lastName = :lastName"),
    @NamedQuery(name = "ActorEntity.findByLastUpdate", query = "SELECT a FROM ActorEntity a WHERE a.lastUpdate = :lastUpdate")
})
public class ActorEntity implements Serializable
{
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "actor_id")
    private Short actorId;
    @Basic(optional = false)
    @Column(name = "first_name")
    private String firstName;
    @Basic(optional = false)
    @Column(name = "last_name")
    private String lastName;
    @Basic(optional = false)
    @Column(name = "last_update")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastUpdate;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "actorEntity")
    private Collection<FilmActorEntity> filmActorEntityCollection;

    public ActorEntity()
    {
    }

    public ActorEntity(Short actorId)
    {
        this.actorId = actorId;
    }

    public ActorEntity(Short actorId, String firstName, String lastName, Date lastUpdate)
    {
        this.actorId = actorId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.lastUpdate = lastUpdate;
    }

    public Short getActorId()
    {
        return actorId;
    }

    public void setActorId(Short actorId)
    {
        this.actorId = actorId;
    }

    public String getFirstName()
    {
        return firstName;
    }

    public void setFirstName(String firstName)
    {
        this.firstName = firstName;
    }

    public String getLastName()
    {
        return lastName;
    }

    public void setLastName(String lastName)
    {
        this.lastName = lastName;
    }

    public Date getLastUpdate()
    {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate)
    {
        this.lastUpdate = lastUpdate;
    }

    public Collection<FilmActorEntity> getFilmActorEntityCollection()
    {
        return filmActorEntityCollection;
    }

    public void setFilmActorEntityCollection(Collection<FilmActorEntity> filmActorEntityCollection)
    {
        this.filmActorEntityCollection = filmActorEntityCollection;
    }

    @Override
    public int hashCode()
    {
        int hash = 0;
        hash += (actorId != null ? actorId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object)
    {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ActorEntity))
        {
            return false;
        }
        ActorEntity other = (ActorEntity) object;
        if ((this.actorId == null && other.actorId != null) || (this.actorId != null && !this.actorId.equals(other.actorId)))
        {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return "ar.edu.utnfc.argprog.data.entities.ActorEntity[ actorId=" + actorId + " ]";
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ar.edu.utnfc.argprog.data.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import jakarta.persistence.Basic;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;


/**
 *
 * @author Felipe
 */
@Entity
@Table(name = "city")

@NamedQueries(
{
    @NamedQuery(name = "CityEntity.findAll", query = "SELECT c FROM CityEntity c"),
    @NamedQuery(name = "CityEntity.findByCityId", query = "SELECT c FROM CityEntity c WHERE c.cityId = :cityId"),
    @NamedQuery(name = "CityEntity.findByCity", query = "SELECT c FROM CityEntity c WHERE c.city = :city"),
    @NamedQuery(name = "CityEntity.findByLastUpdate", query = "SELECT c FROM CityEntity c WHERE c.lastUpdate = :lastUpdate")
})
public class CityEntity implements Serializable
{
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "city_id")
    private Short cityId;
    @Basic(optional = false)
    private String city;
    @Basic(optional = false)
    @Column(name = "last_update")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastUpdate;
    @JoinColumn(name = "country_id", referencedColumnName = "country_id")
    @ManyToOne(optional = false)
    private CountryEntity countryEntity;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cityEntity")
    private Collection<AddressEntity> addressEntityCollection;

    public CityEntity()
    {
    }

    public CityEntity(Short cityId)
    {
        this.cityId = cityId;
    }

    public CityEntity(Short cityId, String city, Date lastUpdate)
    {
        this.cityId = cityId;
        this.city = city;
        this.lastUpdate = lastUpdate;
    }

    public Short getCityId()
    {
        return cityId;
    }

    public void setCityId(Short cityId)
    {
        this.cityId = cityId;
    }

    public String getCity()
    {
        return city;
    }

    public void setCity(String city)
    {
        this.city = city;
    }

    public Date getLastUpdate()
    {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate)
    {
        this.lastUpdate = lastUpdate;
    }

    public CountryEntity getCountryEntity()
    {
        return countryEntity;
    }

    public void setCountryEntity(CountryEntity countryEntity)
    {
        this.countryEntity = countryEntity;
    }

    public Collection<AddressEntity> getAddressEntityCollection()
    {
        return addressEntityCollection;
    }

    public void setAddressEntityCollection(Collection<AddressEntity> addressEntityCollection)
    {
        this.addressEntityCollection = addressEntityCollection;
    }

    @Override
    public int hashCode()
    {
        int hash = 0;
        hash += (cityId != null ? cityId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object)
    {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CityEntity))
        {
            return false;
        }
        CityEntity other = (CityEntity) object;
        if ((this.cityId == null && other.cityId != null) || (this.cityId != null && !this.cityId.equals(other.cityId)))
        {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return "ar.edu.utnfc.argprog.data.entities.CityEntity[ cityId=" + cityId + " ]";
    }
    
}

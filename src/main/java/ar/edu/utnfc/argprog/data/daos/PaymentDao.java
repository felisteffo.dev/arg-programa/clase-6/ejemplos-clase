/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ar.edu.utnfc.argprog.data.daos;

import ar.edu.utnfc.argprog.data.daos.exceptions.NonexistentEntityException;
import java.io.Serializable;
import jakarta.persistence.Query;
import jakarta.persistence.EntityNotFoundException;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Root;
import ar.edu.utnfc.argprog.data.entities.StaffEntity;
import ar.edu.utnfc.argprog.data.entities.CustomerEntity;
import ar.edu.utnfc.argprog.data.entities.PaymentEntity;
import ar.edu.utnfc.argprog.data.entities.RentalEntity;
import java.util.List;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;

/**
 *
 * @author Felipe
 */
public class PaymentDao implements Serializable
{

    public PaymentDao(EntityManagerFactory emf)
    {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager()
    {
        return emf.createEntityManager();
    }

    public void create(PaymentEntity paymentEntity)
    {
        EntityManager em = null;
        try
        {
            em = getEntityManager();
            em.getTransaction().begin();
            StaffEntity staffEntity = paymentEntity.getStaffEntity();
            if (staffEntity != null)
            {
                staffEntity = em.getReference(staffEntity.getClass(), staffEntity.getStaffId());
                paymentEntity.setStaffEntity(staffEntity);
            }
            CustomerEntity customerEntity = paymentEntity.getCustomerEntity();
            if (customerEntity != null)
            {
                customerEntity = em.getReference(customerEntity.getClass(), customerEntity.getCustomerId());
                paymentEntity.setCustomerEntity(customerEntity);
            }
            RentalEntity rentalEntity = paymentEntity.getRentalEntity();
            if (rentalEntity != null)
            {
                rentalEntity = em.getReference(rentalEntity.getClass(), rentalEntity.getRentalId());
                paymentEntity.setRentalEntity(rentalEntity);
            }
            em.persist(paymentEntity);
            if (staffEntity != null)
            {
                staffEntity.getPaymentEntityCollection().add(paymentEntity);
                staffEntity = em.merge(staffEntity);
            }
            if (customerEntity != null)
            {
                customerEntity.getPaymentEntityCollection().add(paymentEntity);
                customerEntity = em.merge(customerEntity);
            }
            if (rentalEntity != null)
            {
                rentalEntity.getPaymentEntityCollection().add(paymentEntity);
                rentalEntity = em.merge(rentalEntity);
            }
            em.getTransaction().commit();
        }
        finally
        {
            if (em != null)
            {
                em.close();
            }
        }
    }

    public void edit(PaymentEntity paymentEntity) throws NonexistentEntityException, Exception
    {
        EntityManager em = null;
        try
        {
            em = getEntityManager();
            em.getTransaction().begin();
            PaymentEntity persistentPaymentEntity = em.find(PaymentEntity.class, paymentEntity.getPaymentId());
            StaffEntity staffEntityOld = persistentPaymentEntity.getStaffEntity();
            StaffEntity staffEntityNew = paymentEntity.getStaffEntity();
            CustomerEntity customerEntityOld = persistentPaymentEntity.getCustomerEntity();
            CustomerEntity customerEntityNew = paymentEntity.getCustomerEntity();
            RentalEntity rentalEntityOld = persistentPaymentEntity.getRentalEntity();
            RentalEntity rentalEntityNew = paymentEntity.getRentalEntity();
            if (staffEntityNew != null)
            {
                staffEntityNew = em.getReference(staffEntityNew.getClass(), staffEntityNew.getStaffId());
                paymentEntity.setStaffEntity(staffEntityNew);
            }
            if (customerEntityNew != null)
            {
                customerEntityNew = em.getReference(customerEntityNew.getClass(), customerEntityNew.getCustomerId());
                paymentEntity.setCustomerEntity(customerEntityNew);
            }
            if (rentalEntityNew != null)
            {
                rentalEntityNew = em.getReference(rentalEntityNew.getClass(), rentalEntityNew.getRentalId());
                paymentEntity.setRentalEntity(rentalEntityNew);
            }
            paymentEntity = em.merge(paymentEntity);
            if (staffEntityOld != null && !staffEntityOld.equals(staffEntityNew))
            {
                staffEntityOld.getPaymentEntityCollection().remove(paymentEntity);
                staffEntityOld = em.merge(staffEntityOld);
            }
            if (staffEntityNew != null && !staffEntityNew.equals(staffEntityOld))
            {
                staffEntityNew.getPaymentEntityCollection().add(paymentEntity);
                staffEntityNew = em.merge(staffEntityNew);
            }
            if (customerEntityOld != null && !customerEntityOld.equals(customerEntityNew))
            {
                customerEntityOld.getPaymentEntityCollection().remove(paymentEntity);
                customerEntityOld = em.merge(customerEntityOld);
            }
            if (customerEntityNew != null && !customerEntityNew.equals(customerEntityOld))
            {
                customerEntityNew.getPaymentEntityCollection().add(paymentEntity);
                customerEntityNew = em.merge(customerEntityNew);
            }
            if (rentalEntityOld != null && !rentalEntityOld.equals(rentalEntityNew))
            {
                rentalEntityOld.getPaymentEntityCollection().remove(paymentEntity);
                rentalEntityOld = em.merge(rentalEntityOld);
            }
            if (rentalEntityNew != null && !rentalEntityNew.equals(rentalEntityOld))
            {
                rentalEntityNew.getPaymentEntityCollection().add(paymentEntity);
                rentalEntityNew = em.merge(rentalEntityNew);
            }
            em.getTransaction().commit();
        }
        catch (Exception ex)
        {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0)
            {
                Short id = paymentEntity.getPaymentId();
                if (findPaymentEntity(id) == null)
                {
                    throw new NonexistentEntityException("The paymentEntity with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
        finally
        {
            if (em != null)
            {
                em.close();
            }
        }
    }

    public void destroy(Short id) throws NonexistentEntityException
    {
        EntityManager em = null;
        try
        {
            em = getEntityManager();
            em.getTransaction().begin();
            PaymentEntity paymentEntity;
            try
            {
                paymentEntity = em.getReference(PaymentEntity.class, id);
                paymentEntity.getPaymentId();
            }
            catch (EntityNotFoundException enfe)
            {
                throw new NonexistentEntityException("The paymentEntity with id " + id + " no longer exists.", enfe);
            }
            StaffEntity staffEntity = paymentEntity.getStaffEntity();
            if (staffEntity != null)
            {
                staffEntity.getPaymentEntityCollection().remove(paymentEntity);
                staffEntity = em.merge(staffEntity);
            }
            CustomerEntity customerEntity = paymentEntity.getCustomerEntity();
            if (customerEntity != null)
            {
                customerEntity.getPaymentEntityCollection().remove(paymentEntity);
                customerEntity = em.merge(customerEntity);
            }
            RentalEntity rentalEntity = paymentEntity.getRentalEntity();
            if (rentalEntity != null)
            {
                rentalEntity.getPaymentEntityCollection().remove(paymentEntity);
                rentalEntity = em.merge(rentalEntity);
            }
            em.remove(paymentEntity);
            em.getTransaction().commit();
        }
        finally
        {
            if (em != null)
            {
                em.close();
            }
        }
    }

    public List<PaymentEntity> findPaymentEntityEntities()
    {
        return findPaymentEntityEntities(true, -1, -1);
    }

    public List<PaymentEntity> findPaymentEntityEntities(int maxResults, int firstResult)
    {
        return findPaymentEntityEntities(false, maxResults, firstResult);
    }

    private List<PaymentEntity> findPaymentEntityEntities(boolean all, int maxResults, int firstResult)
    {
        EntityManager em = getEntityManager();
        try
        {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(PaymentEntity.class));
            Query q = em.createQuery(cq);
            if (!all)
            {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        }
        finally
        {
            em.close();
        }
    }

    public PaymentEntity findPaymentEntity(Short id)
    {
        EntityManager em = getEntityManager();
        try
        {
            return em.find(PaymentEntity.class, id);
        }
        finally
        {
            em.close();
        }
    }

    public int getPaymentEntityCount()
    {
        EntityManager em = getEntityManager();
        try
        {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<PaymentEntity> rt = cq.from(PaymentEntity.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        }
        finally
        {
            em.close();
        }
    }
    
}

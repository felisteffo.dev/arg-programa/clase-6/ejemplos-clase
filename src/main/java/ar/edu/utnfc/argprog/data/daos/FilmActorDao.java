/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ar.edu.utnfc.argprog.data.daos;

import ar.edu.utnfc.argprog.data.daos.exceptions.NonexistentEntityException;
import ar.edu.utnfc.argprog.data.daos.exceptions.PreexistingEntityException;
import java.io.Serializable;
import jakarta.persistence.Query;
import jakarta.persistence.EntityNotFoundException;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Root;
import ar.edu.utnfc.argprog.data.entities.FilmEntity;
import ar.edu.utnfc.argprog.data.entities.ActorEntity;
import ar.edu.utnfc.argprog.data.entities.FilmActorEntity;
import ar.edu.utnfc.argprog.data.entities.FilmActorEntityPK;
import java.util.List;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;

/**
 *
 * @author Felipe
 */
public class FilmActorDao implements Serializable
{

    public FilmActorDao(EntityManagerFactory emf)
    {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager()
    {
        return emf.createEntityManager();
    }

    public void create(FilmActorEntity filmActorEntity) throws PreexistingEntityException, Exception
    {
        if (filmActorEntity.getFilmActorEntityPK() == null)
        {
            filmActorEntity.setFilmActorEntityPK(new FilmActorEntityPK());
        }
        filmActorEntity.getFilmActorEntityPK().setFilmId(filmActorEntity.getFilmEntity().getFilmId());
        filmActorEntity.getFilmActorEntityPK().setActorId(filmActorEntity.getActorEntity().getActorId());
        EntityManager em = null;
        try
        {
            em = getEntityManager();
            em.getTransaction().begin();
            FilmEntity filmEntity = filmActorEntity.getFilmEntity();
            if (filmEntity != null)
            {
                filmEntity = em.getReference(filmEntity.getClass(), filmEntity.getFilmId());
                filmActorEntity.setFilmEntity(filmEntity);
            }
            ActorEntity actorEntity = filmActorEntity.getActorEntity();
            if (actorEntity != null)
            {
                actorEntity = em.getReference(actorEntity.getClass(), actorEntity.getActorId());
                filmActorEntity.setActorEntity(actorEntity);
            }
            em.persist(filmActorEntity);
            if (filmEntity != null)
            {
                filmEntity.getFilmActorEntityCollection().add(filmActorEntity);
                filmEntity = em.merge(filmEntity);
            }
            if (actorEntity != null)
            {
                actorEntity.getFilmActorEntityCollection().add(filmActorEntity);
                actorEntity = em.merge(actorEntity);
            }
            em.getTransaction().commit();
        }
        catch (Exception ex)
        {
            if (findFilmActorEntity(filmActorEntity.getFilmActorEntityPK()) != null)
            {
                throw new PreexistingEntityException("FilmActorEntity " + filmActorEntity + " already exists.", ex);
            }
            throw ex;
        }
        finally
        {
            if (em != null)
            {
                em.close();
            }
        }
    }

    public void edit(FilmActorEntity filmActorEntity) throws NonexistentEntityException, Exception
    {
        filmActorEntity.getFilmActorEntityPK().setFilmId(filmActorEntity.getFilmEntity().getFilmId());
        filmActorEntity.getFilmActorEntityPK().setActorId(filmActorEntity.getActorEntity().getActorId());
        EntityManager em = null;
        try
        {
            em = getEntityManager();
            em.getTransaction().begin();
            FilmActorEntity persistentFilmActorEntity = em.find(FilmActorEntity.class, filmActorEntity.getFilmActorEntityPK());
            FilmEntity filmEntityOld = persistentFilmActorEntity.getFilmEntity();
            FilmEntity filmEntityNew = filmActorEntity.getFilmEntity();
            ActorEntity actorEntityOld = persistentFilmActorEntity.getActorEntity();
            ActorEntity actorEntityNew = filmActorEntity.getActorEntity();
            if (filmEntityNew != null)
            {
                filmEntityNew = em.getReference(filmEntityNew.getClass(), filmEntityNew.getFilmId());
                filmActorEntity.setFilmEntity(filmEntityNew);
            }
            if (actorEntityNew != null)
            {
                actorEntityNew = em.getReference(actorEntityNew.getClass(), actorEntityNew.getActorId());
                filmActorEntity.setActorEntity(actorEntityNew);
            }
            filmActorEntity = em.merge(filmActorEntity);
            if (filmEntityOld != null && !filmEntityOld.equals(filmEntityNew))
            {
                filmEntityOld.getFilmActorEntityCollection().remove(filmActorEntity);
                filmEntityOld = em.merge(filmEntityOld);
            }
            if (filmEntityNew != null && !filmEntityNew.equals(filmEntityOld))
            {
                filmEntityNew.getFilmActorEntityCollection().add(filmActorEntity);
                filmEntityNew = em.merge(filmEntityNew);
            }
            if (actorEntityOld != null && !actorEntityOld.equals(actorEntityNew))
            {
                actorEntityOld.getFilmActorEntityCollection().remove(filmActorEntity);
                actorEntityOld = em.merge(actorEntityOld);
            }
            if (actorEntityNew != null && !actorEntityNew.equals(actorEntityOld))
            {
                actorEntityNew.getFilmActorEntityCollection().add(filmActorEntity);
                actorEntityNew = em.merge(actorEntityNew);
            }
            em.getTransaction().commit();
        }
        catch (Exception ex)
        {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0)
            {
                FilmActorEntityPK id = filmActorEntity.getFilmActorEntityPK();
                if (findFilmActorEntity(id) == null)
                {
                    throw new NonexistentEntityException("The filmActorEntity with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
        finally
        {
            if (em != null)
            {
                em.close();
            }
        }
    }

    public void destroy(FilmActorEntityPK id) throws NonexistentEntityException
    {
        EntityManager em = null;
        try
        {
            em = getEntityManager();
            em.getTransaction().begin();
            FilmActorEntity filmActorEntity;
            try
            {
                filmActorEntity = em.getReference(FilmActorEntity.class, id);
                filmActorEntity.getFilmActorEntityPK();
            }
            catch (EntityNotFoundException enfe)
            {
                throw new NonexistentEntityException("The filmActorEntity with id " + id + " no longer exists.", enfe);
            }
            FilmEntity filmEntity = filmActorEntity.getFilmEntity();
            if (filmEntity != null)
            {
                filmEntity.getFilmActorEntityCollection().remove(filmActorEntity);
                filmEntity = em.merge(filmEntity);
            }
            ActorEntity actorEntity = filmActorEntity.getActorEntity();
            if (actorEntity != null)
            {
                actorEntity.getFilmActorEntityCollection().remove(filmActorEntity);
                actorEntity = em.merge(actorEntity);
            }
            em.remove(filmActorEntity);
            em.getTransaction().commit();
        }
        finally
        {
            if (em != null)
            {
                em.close();
            }
        }
    }

    public List<FilmActorEntity> findFilmActorEntityEntities()
    {
        return findFilmActorEntityEntities(true, -1, -1);
    }

    public List<FilmActorEntity> findFilmActorEntityEntities(int maxResults, int firstResult)
    {
        return findFilmActorEntityEntities(false, maxResults, firstResult);
    }

    private List<FilmActorEntity> findFilmActorEntityEntities(boolean all, int maxResults, int firstResult)
    {
        EntityManager em = getEntityManager();
        try
        {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(FilmActorEntity.class));
            Query q = em.createQuery(cq);
            if (!all)
            {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        }
        finally
        {
            em.close();
        }
    }

    public FilmActorEntity findFilmActorEntity(FilmActorEntityPK id)
    {
        EntityManager em = getEntityManager();
        try
        {
            return em.find(FilmActorEntity.class, id);
        }
        finally
        {
            em.close();
        }
    }

    public int getFilmActorEntityCount()
    {
        EntityManager em = getEntityManager();
        try
        {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<FilmActorEntity> rt = cq.from(FilmActorEntity.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        }
        finally
        {
            em.close();
        }
    }
    
}

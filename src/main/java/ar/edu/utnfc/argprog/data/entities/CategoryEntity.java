/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ar.edu.utnfc.argprog.data.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import jakarta.persistence.Basic;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;

/**
 *
 * @author Felipe
 */
@Entity
@Table(name = "category")
@NamedQueries(
{
    @NamedQuery(name = "CategoryEntity.findAll", query = "SELECT c FROM CategoryEntity c"),
    @NamedQuery(name = "CategoryEntity.findByCategoryId", query = "SELECT c FROM CategoryEntity c WHERE c.categoryId = :categoryId"),
    @NamedQuery(name = "CategoryEntity.findByName", query = "SELECT c FROM CategoryEntity c WHERE c.name = :name"),
    @NamedQuery(name = "CategoryEntity.findByLastUpdate", query = "SELECT c FROM CategoryEntity c WHERE c.lastUpdate = :lastUpdate")
})
public class CategoryEntity implements Serializable
{
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "category_id")
    private Short categoryId;
    @Basic(optional = false)
    private String name;
    @Basic(optional = false)
    @Column(name = "last_update")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastUpdate;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "categoryEntity")
    private Collection<FilmCategoryEntity> filmCategoryEntityCollection;

    public CategoryEntity()
    {
    }

    public CategoryEntity(Short categoryId)
    {
        this.categoryId = categoryId;
    }

    public CategoryEntity(Short categoryId, String name, Date lastUpdate)
    {
        this.categoryId = categoryId;
        this.name = name;
        this.lastUpdate = lastUpdate;
    }

    public Short getCategoryId()
    {
        return categoryId;
    }

    public void setCategoryId(Short categoryId)
    {
        this.categoryId = categoryId;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public Date getLastUpdate()
    {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate)
    {
        this.lastUpdate = lastUpdate;
    }

    public Collection<FilmCategoryEntity> getFilmCategoryEntityCollection()
    {
        return filmCategoryEntityCollection;
    }

    public void setFilmCategoryEntityCollection(Collection<FilmCategoryEntity> filmCategoryEntityCollection)
    {
        this.filmCategoryEntityCollection = filmCategoryEntityCollection;
    }

    @Override
    public int hashCode()
    {
        int hash = 0;
        hash += (categoryId != null ? categoryId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object)
    {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CategoryEntity))
        {
            return false;
        }
        CategoryEntity other = (CategoryEntity) object;
        if ((this.categoryId == null && other.categoryId != null) || (this.categoryId != null && !this.categoryId.equals(other.categoryId)))
        {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return "ar.edu.utnfc.argprog.data.entities.CategoryEntity[ categoryId=" + categoryId + " ]";
    }
    
}

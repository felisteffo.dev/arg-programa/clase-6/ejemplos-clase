/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ar.edu.utnfc.argprog.data.daos;

import ar.edu.utnfc.argprog.data.daos.exceptions.IllegalOrphanException;
import ar.edu.utnfc.argprog.data.daos.exceptions.NonexistentEntityException;
import java.io.Serializable;
import jakarta.persistence.Query;
import jakarta.persistence.EntityNotFoundException;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Root;
import ar.edu.utnfc.argprog.data.entities.FilmEntity;
import ar.edu.utnfc.argprog.data.entities.InventoryEntity;
import ar.edu.utnfc.argprog.data.entities.StoreEntity;
import ar.edu.utnfc.argprog.data.entities.RentalEntity;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;

/**
 *
 * @author Felipe
 */
public class InventoryDao implements Serializable
{

    public InventoryDao(EntityManagerFactory emf)
    {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager()
    {
        return emf.createEntityManager();
    }

    public void create(InventoryEntity inventoryEntity)
    {
        if (inventoryEntity.getRentalEntityCollection() == null)
        {
            inventoryEntity.setRentalEntityCollection(new ArrayList<RentalEntity>());
        }
        EntityManager em = null;
        try
        {
            em = getEntityManager();
            em.getTransaction().begin();
            FilmEntity filmEntity = inventoryEntity.getFilmEntity();
            if (filmEntity != null)
            {
                filmEntity = em.getReference(filmEntity.getClass(), filmEntity.getFilmId());
                inventoryEntity.setFilmEntity(filmEntity);
            }
            StoreEntity storeEntity = inventoryEntity.getStoreEntity();
            if (storeEntity != null)
            {
                storeEntity = em.getReference(storeEntity.getClass(), storeEntity.getStoreId());
                inventoryEntity.setStoreEntity(storeEntity);
            }
            Collection<RentalEntity> attachedRentalEntityCollection = new ArrayList<RentalEntity>();
            for (RentalEntity rentalEntityCollectionRentalEntityToAttach : inventoryEntity.getRentalEntityCollection())
            {
                rentalEntityCollectionRentalEntityToAttach = em.getReference(rentalEntityCollectionRentalEntityToAttach.getClass(), rentalEntityCollectionRentalEntityToAttach.getRentalId());
                attachedRentalEntityCollection.add(rentalEntityCollectionRentalEntityToAttach);
            }
            inventoryEntity.setRentalEntityCollection(attachedRentalEntityCollection);
            em.persist(inventoryEntity);
            if (filmEntity != null)
            {
                filmEntity.getInventoryEntityCollection().add(inventoryEntity);
                filmEntity = em.merge(filmEntity);
            }
            if (storeEntity != null)
            {
                storeEntity.getInventoryEntityCollection().add(inventoryEntity);
                storeEntity = em.merge(storeEntity);
            }
            for (RentalEntity rentalEntityCollectionRentalEntity : inventoryEntity.getRentalEntityCollection())
            {
                InventoryEntity oldInventoryEntityOfRentalEntityCollectionRentalEntity = rentalEntityCollectionRentalEntity.getInventoryEntity();
                rentalEntityCollectionRentalEntity.setInventoryEntity(inventoryEntity);
                rentalEntityCollectionRentalEntity = em.merge(rentalEntityCollectionRentalEntity);
                if (oldInventoryEntityOfRentalEntityCollectionRentalEntity != null)
                {
                    oldInventoryEntityOfRentalEntityCollectionRentalEntity.getRentalEntityCollection().remove(rentalEntityCollectionRentalEntity);
                    oldInventoryEntityOfRentalEntityCollectionRentalEntity = em.merge(oldInventoryEntityOfRentalEntityCollectionRentalEntity);
                }
            }
            em.getTransaction().commit();
        }
        finally
        {
            if (em != null)
            {
                em.close();
            }
        }
    }

    public void edit(InventoryEntity inventoryEntity) throws IllegalOrphanException, NonexistentEntityException, Exception
    {
        EntityManager em = null;
        try
        {
            em = getEntityManager();
            em.getTransaction().begin();
            InventoryEntity persistentInventoryEntity = em.find(InventoryEntity.class, inventoryEntity.getInventoryId());
            FilmEntity filmEntityOld = persistentInventoryEntity.getFilmEntity();
            FilmEntity filmEntityNew = inventoryEntity.getFilmEntity();
            StoreEntity storeEntityOld = persistentInventoryEntity.getStoreEntity();
            StoreEntity storeEntityNew = inventoryEntity.getStoreEntity();
            Collection<RentalEntity> rentalEntityCollectionOld = persistentInventoryEntity.getRentalEntityCollection();
            Collection<RentalEntity> rentalEntityCollectionNew = inventoryEntity.getRentalEntityCollection();
            List<String> illegalOrphanMessages = null;
            for (RentalEntity rentalEntityCollectionOldRentalEntity : rentalEntityCollectionOld)
            {
                if (!rentalEntityCollectionNew.contains(rentalEntityCollectionOldRentalEntity))
                {
                    if (illegalOrphanMessages == null)
                    {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain RentalEntity " + rentalEntityCollectionOldRentalEntity + " since its inventoryEntity field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null)
            {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (filmEntityNew != null)
            {
                filmEntityNew = em.getReference(filmEntityNew.getClass(), filmEntityNew.getFilmId());
                inventoryEntity.setFilmEntity(filmEntityNew);
            }
            if (storeEntityNew != null)
            {
                storeEntityNew = em.getReference(storeEntityNew.getClass(), storeEntityNew.getStoreId());
                inventoryEntity.setStoreEntity(storeEntityNew);
            }
            Collection<RentalEntity> attachedRentalEntityCollectionNew = new ArrayList<RentalEntity>();
            for (RentalEntity rentalEntityCollectionNewRentalEntityToAttach : rentalEntityCollectionNew)
            {
                rentalEntityCollectionNewRentalEntityToAttach = em.getReference(rentalEntityCollectionNewRentalEntityToAttach.getClass(), rentalEntityCollectionNewRentalEntityToAttach.getRentalId());
                attachedRentalEntityCollectionNew.add(rentalEntityCollectionNewRentalEntityToAttach);
            }
            rentalEntityCollectionNew = attachedRentalEntityCollectionNew;
            inventoryEntity.setRentalEntityCollection(rentalEntityCollectionNew);
            inventoryEntity = em.merge(inventoryEntity);
            if (filmEntityOld != null && !filmEntityOld.equals(filmEntityNew))
            {
                filmEntityOld.getInventoryEntityCollection().remove(inventoryEntity);
                filmEntityOld = em.merge(filmEntityOld);
            }
            if (filmEntityNew != null && !filmEntityNew.equals(filmEntityOld))
            {
                filmEntityNew.getInventoryEntityCollection().add(inventoryEntity);
                filmEntityNew = em.merge(filmEntityNew);
            }
            if (storeEntityOld != null && !storeEntityOld.equals(storeEntityNew))
            {
                storeEntityOld.getInventoryEntityCollection().remove(inventoryEntity);
                storeEntityOld = em.merge(storeEntityOld);
            }
            if (storeEntityNew != null && !storeEntityNew.equals(storeEntityOld))
            {
                storeEntityNew.getInventoryEntityCollection().add(inventoryEntity);
                storeEntityNew = em.merge(storeEntityNew);
            }
            for (RentalEntity rentalEntityCollectionNewRentalEntity : rentalEntityCollectionNew)
            {
                if (!rentalEntityCollectionOld.contains(rentalEntityCollectionNewRentalEntity))
                {
                    InventoryEntity oldInventoryEntityOfRentalEntityCollectionNewRentalEntity = rentalEntityCollectionNewRentalEntity.getInventoryEntity();
                    rentalEntityCollectionNewRentalEntity.setInventoryEntity(inventoryEntity);
                    rentalEntityCollectionNewRentalEntity = em.merge(rentalEntityCollectionNewRentalEntity);
                    if (oldInventoryEntityOfRentalEntityCollectionNewRentalEntity != null && !oldInventoryEntityOfRentalEntityCollectionNewRentalEntity.equals(inventoryEntity))
                    {
                        oldInventoryEntityOfRentalEntityCollectionNewRentalEntity.getRentalEntityCollection().remove(rentalEntityCollectionNewRentalEntity);
                        oldInventoryEntityOfRentalEntityCollectionNewRentalEntity = em.merge(oldInventoryEntityOfRentalEntityCollectionNewRentalEntity);
                    }
                }
            }
            em.getTransaction().commit();
        }
        catch (Exception ex)
        {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0)
            {
                Integer id = inventoryEntity.getInventoryId();
                if (findInventoryEntity(id) == null)
                {
                    throw new NonexistentEntityException("The inventoryEntity with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
        finally
        {
            if (em != null)
            {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException
    {
        EntityManager em = null;
        try
        {
            em = getEntityManager();
            em.getTransaction().begin();
            InventoryEntity inventoryEntity;
            try
            {
                inventoryEntity = em.getReference(InventoryEntity.class, id);
                inventoryEntity.getInventoryId();
            }
            catch (EntityNotFoundException enfe)
            {
                throw new NonexistentEntityException("The inventoryEntity with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<RentalEntity> rentalEntityCollectionOrphanCheck = inventoryEntity.getRentalEntityCollection();
            for (RentalEntity rentalEntityCollectionOrphanCheckRentalEntity : rentalEntityCollectionOrphanCheck)
            {
                if (illegalOrphanMessages == null)
                {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This InventoryEntity (" + inventoryEntity + ") cannot be destroyed since the RentalEntity " + rentalEntityCollectionOrphanCheckRentalEntity + " in its rentalEntityCollection field has a non-nullable inventoryEntity field.");
            }
            if (illegalOrphanMessages != null)
            {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            FilmEntity filmEntity = inventoryEntity.getFilmEntity();
            if (filmEntity != null)
            {
                filmEntity.getInventoryEntityCollection().remove(inventoryEntity);
                filmEntity = em.merge(filmEntity);
            }
            StoreEntity storeEntity = inventoryEntity.getStoreEntity();
            if (storeEntity != null)
            {
                storeEntity.getInventoryEntityCollection().remove(inventoryEntity);
                storeEntity = em.merge(storeEntity);
            }
            em.remove(inventoryEntity);
            em.getTransaction().commit();
        }
        finally
        {
            if (em != null)
            {
                em.close();
            }
        }
    }

    public List<InventoryEntity> findInventoryEntityEntities()
    {
        return findInventoryEntityEntities(true, -1, -1);
    }

    public List<InventoryEntity> findInventoryEntityEntities(int maxResults, int firstResult)
    {
        return findInventoryEntityEntities(false, maxResults, firstResult);
    }

    private List<InventoryEntity> findInventoryEntityEntities(boolean all, int maxResults, int firstResult)
    {
        EntityManager em = getEntityManager();
        try
        {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(InventoryEntity.class));
            Query q = em.createQuery(cq);
            if (!all)
            {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        }
        finally
        {
            em.close();
        }
    }

    public InventoryEntity findInventoryEntity(Integer id)
    {
        EntityManager em = getEntityManager();
        try
        {
            return em.find(InventoryEntity.class, id);
        }
        finally
        {
            em.close();
        }
    }

    public int getInventoryEntityCount()
    {
        EntityManager em = getEntityManager();
        try
        {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<InventoryEntity> rt = cq.from(InventoryEntity.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        }
        finally
        {
            em.close();
        }
    }
    
}

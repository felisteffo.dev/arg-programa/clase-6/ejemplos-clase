/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ar.edu.utnfc.argprog.data.daos;

import ar.edu.utnfc.argprog.data.daos.exceptions.IllegalOrphanException;
import ar.edu.utnfc.argprog.data.daos.exceptions.NonexistentEntityException;
import ar.edu.utnfc.argprog.data.entities.AddressEntity;
import java.io.Serializable;
import jakarta.persistence.Query;
import jakarta.persistence.EntityNotFoundException;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Root;
import ar.edu.utnfc.argprog.data.entities.CityEntity;
import ar.edu.utnfc.argprog.data.entities.StoreEntity;
import java.util.ArrayList;
import java.util.Collection;
import ar.edu.utnfc.argprog.data.entities.CustomerEntity;
import ar.edu.utnfc.argprog.data.entities.StaffEntity;
import java.util.List;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;

/**
 *
 * @author Felipe
 */
public class AddressDao implements Serializable
{

    public AddressDao(EntityManagerFactory emf)
    {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager()
    {
        return emf.createEntityManager();
    }

    public void create(AddressEntity addressEntity)
    {
        if (addressEntity.getStoreEntityCollection() == null)
        {
            addressEntity.setStoreEntityCollection(new ArrayList<StoreEntity>());
        }
        if (addressEntity.getCustomerEntityCollection() == null)
        {
            addressEntity.setCustomerEntityCollection(new ArrayList<CustomerEntity>());
        }
        if (addressEntity.getStaffEntityCollection() == null)
        {
            addressEntity.setStaffEntityCollection(new ArrayList<StaffEntity>());
        }
        EntityManager em = null;
        try
        {
            em = getEntityManager();
            em.getTransaction().begin();
            CityEntity cityEntity = addressEntity.getCityEntity();
            if (cityEntity != null)
            {
                cityEntity = em.getReference(cityEntity.getClass(), cityEntity.getCityId());
                addressEntity.setCityEntity(cityEntity);
            }
            Collection<StoreEntity> attachedStoreEntityCollection = new ArrayList<StoreEntity>();
            for (StoreEntity storeEntityCollectionStoreEntityToAttach : addressEntity.getStoreEntityCollection())
            {
                storeEntityCollectionStoreEntityToAttach = em.getReference(storeEntityCollectionStoreEntityToAttach.getClass(), storeEntityCollectionStoreEntityToAttach.getStoreId());
                attachedStoreEntityCollection.add(storeEntityCollectionStoreEntityToAttach);
            }
            addressEntity.setStoreEntityCollection(attachedStoreEntityCollection);
            Collection<CustomerEntity> attachedCustomerEntityCollection = new ArrayList<CustomerEntity>();
            for (CustomerEntity customerEntityCollectionCustomerEntityToAttach : addressEntity.getCustomerEntityCollection())
            {
                customerEntityCollectionCustomerEntityToAttach = em.getReference(customerEntityCollectionCustomerEntityToAttach.getClass(), customerEntityCollectionCustomerEntityToAttach.getCustomerId());
                attachedCustomerEntityCollection.add(customerEntityCollectionCustomerEntityToAttach);
            }
            addressEntity.setCustomerEntityCollection(attachedCustomerEntityCollection);
            Collection<StaffEntity> attachedStaffEntityCollection = new ArrayList<StaffEntity>();
            for (StaffEntity staffEntityCollectionStaffEntityToAttach : addressEntity.getStaffEntityCollection())
            {
                staffEntityCollectionStaffEntityToAttach = em.getReference(staffEntityCollectionStaffEntityToAttach.getClass(), staffEntityCollectionStaffEntityToAttach.getStaffId());
                attachedStaffEntityCollection.add(staffEntityCollectionStaffEntityToAttach);
            }
            addressEntity.setStaffEntityCollection(attachedStaffEntityCollection);
            em.persist(addressEntity);
            if (cityEntity != null)
            {
                cityEntity.getAddressEntityCollection().add(addressEntity);
                cityEntity = em.merge(cityEntity);
            }
            for (StoreEntity storeEntityCollectionStoreEntity : addressEntity.getStoreEntityCollection())
            {
                AddressEntity oldAddressEntityOfStoreEntityCollectionStoreEntity = storeEntityCollectionStoreEntity.getAddressEntity();
                storeEntityCollectionStoreEntity.setAddressEntity(addressEntity);
                storeEntityCollectionStoreEntity = em.merge(storeEntityCollectionStoreEntity);
                if (oldAddressEntityOfStoreEntityCollectionStoreEntity != null)
                {
                    oldAddressEntityOfStoreEntityCollectionStoreEntity.getStoreEntityCollection().remove(storeEntityCollectionStoreEntity);
                    oldAddressEntityOfStoreEntityCollectionStoreEntity = em.merge(oldAddressEntityOfStoreEntityCollectionStoreEntity);
                }
            }
            for (CustomerEntity customerEntityCollectionCustomerEntity : addressEntity.getCustomerEntityCollection())
            {
                AddressEntity oldAddressEntityOfCustomerEntityCollectionCustomerEntity = customerEntityCollectionCustomerEntity.getAddressEntity();
                customerEntityCollectionCustomerEntity.setAddressEntity(addressEntity);
                customerEntityCollectionCustomerEntity = em.merge(customerEntityCollectionCustomerEntity);
                if (oldAddressEntityOfCustomerEntityCollectionCustomerEntity != null)
                {
                    oldAddressEntityOfCustomerEntityCollectionCustomerEntity.getCustomerEntityCollection().remove(customerEntityCollectionCustomerEntity);
                    oldAddressEntityOfCustomerEntityCollectionCustomerEntity = em.merge(oldAddressEntityOfCustomerEntityCollectionCustomerEntity);
                }
            }
            for (StaffEntity staffEntityCollectionStaffEntity : addressEntity.getStaffEntityCollection())
            {
                AddressEntity oldAddressEntityOfStaffEntityCollectionStaffEntity = staffEntityCollectionStaffEntity.getAddressEntity();
                staffEntityCollectionStaffEntity.setAddressEntity(addressEntity);
                staffEntityCollectionStaffEntity = em.merge(staffEntityCollectionStaffEntity);
                if (oldAddressEntityOfStaffEntityCollectionStaffEntity != null)
                {
                    oldAddressEntityOfStaffEntityCollectionStaffEntity.getStaffEntityCollection().remove(staffEntityCollectionStaffEntity);
                    oldAddressEntityOfStaffEntityCollectionStaffEntity = em.merge(oldAddressEntityOfStaffEntityCollectionStaffEntity);
                }
            }
            em.getTransaction().commit();
        }
        finally
        {
            if (em != null)
            {
                em.close();
            }
        }
    }

    public void edit(AddressEntity addressEntity) throws IllegalOrphanException, NonexistentEntityException, Exception
    {
        EntityManager em = null;
        try
        {
            em = getEntityManager();
            em.getTransaction().begin();
            AddressEntity persistentAddressEntity = em.find(AddressEntity.class, addressEntity.getAddressId());
            CityEntity cityEntityOld = persistentAddressEntity.getCityEntity();
            CityEntity cityEntityNew = addressEntity.getCityEntity();
            Collection<StoreEntity> storeEntityCollectionOld = persistentAddressEntity.getStoreEntityCollection();
            Collection<StoreEntity> storeEntityCollectionNew = addressEntity.getStoreEntityCollection();
            Collection<CustomerEntity> customerEntityCollectionOld = persistentAddressEntity.getCustomerEntityCollection();
            Collection<CustomerEntity> customerEntityCollectionNew = addressEntity.getCustomerEntityCollection();
            Collection<StaffEntity> staffEntityCollectionOld = persistentAddressEntity.getStaffEntityCollection();
            Collection<StaffEntity> staffEntityCollectionNew = addressEntity.getStaffEntityCollection();
            List<String> illegalOrphanMessages = null;
            for (StoreEntity storeEntityCollectionOldStoreEntity : storeEntityCollectionOld)
            {
                if (!storeEntityCollectionNew.contains(storeEntityCollectionOldStoreEntity))
                {
                    if (illegalOrphanMessages == null)
                    {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain StoreEntity " + storeEntityCollectionOldStoreEntity + " since its addressEntity field is not nullable.");
                }
            }
            for (CustomerEntity customerEntityCollectionOldCustomerEntity : customerEntityCollectionOld)
            {
                if (!customerEntityCollectionNew.contains(customerEntityCollectionOldCustomerEntity))
                {
                    if (illegalOrphanMessages == null)
                    {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain CustomerEntity " + customerEntityCollectionOldCustomerEntity + " since its addressEntity field is not nullable.");
                }
            }
            for (StaffEntity staffEntityCollectionOldStaffEntity : staffEntityCollectionOld)
            {
                if (!staffEntityCollectionNew.contains(staffEntityCollectionOldStaffEntity))
                {
                    if (illegalOrphanMessages == null)
                    {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain StaffEntity " + staffEntityCollectionOldStaffEntity + " since its addressEntity field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null)
            {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (cityEntityNew != null)
            {
                cityEntityNew = em.getReference(cityEntityNew.getClass(), cityEntityNew.getCityId());
                addressEntity.setCityEntity(cityEntityNew);
            }
            Collection<StoreEntity> attachedStoreEntityCollectionNew = new ArrayList<StoreEntity>();
            for (StoreEntity storeEntityCollectionNewStoreEntityToAttach : storeEntityCollectionNew)
            {
                storeEntityCollectionNewStoreEntityToAttach = em.getReference(storeEntityCollectionNewStoreEntityToAttach.getClass(), storeEntityCollectionNewStoreEntityToAttach.getStoreId());
                attachedStoreEntityCollectionNew.add(storeEntityCollectionNewStoreEntityToAttach);
            }
            storeEntityCollectionNew = attachedStoreEntityCollectionNew;
            addressEntity.setStoreEntityCollection(storeEntityCollectionNew);
            Collection<CustomerEntity> attachedCustomerEntityCollectionNew = new ArrayList<CustomerEntity>();
            for (CustomerEntity customerEntityCollectionNewCustomerEntityToAttach : customerEntityCollectionNew)
            {
                customerEntityCollectionNewCustomerEntityToAttach = em.getReference(customerEntityCollectionNewCustomerEntityToAttach.getClass(), customerEntityCollectionNewCustomerEntityToAttach.getCustomerId());
                attachedCustomerEntityCollectionNew.add(customerEntityCollectionNewCustomerEntityToAttach);
            }
            customerEntityCollectionNew = attachedCustomerEntityCollectionNew;
            addressEntity.setCustomerEntityCollection(customerEntityCollectionNew);
            Collection<StaffEntity> attachedStaffEntityCollectionNew = new ArrayList<StaffEntity>();
            for (StaffEntity staffEntityCollectionNewStaffEntityToAttach : staffEntityCollectionNew)
            {
                staffEntityCollectionNewStaffEntityToAttach = em.getReference(staffEntityCollectionNewStaffEntityToAttach.getClass(), staffEntityCollectionNewStaffEntityToAttach.getStaffId());
                attachedStaffEntityCollectionNew.add(staffEntityCollectionNewStaffEntityToAttach);
            }
            staffEntityCollectionNew = attachedStaffEntityCollectionNew;
            addressEntity.setStaffEntityCollection(staffEntityCollectionNew);
            addressEntity = em.merge(addressEntity);
            if (cityEntityOld != null && !cityEntityOld.equals(cityEntityNew))
            {
                cityEntityOld.getAddressEntityCollection().remove(addressEntity);
                cityEntityOld = em.merge(cityEntityOld);
            }
            if (cityEntityNew != null && !cityEntityNew.equals(cityEntityOld))
            {
                cityEntityNew.getAddressEntityCollection().add(addressEntity);
                cityEntityNew = em.merge(cityEntityNew);
            }
            for (StoreEntity storeEntityCollectionNewStoreEntity : storeEntityCollectionNew)
            {
                if (!storeEntityCollectionOld.contains(storeEntityCollectionNewStoreEntity))
                {
                    AddressEntity oldAddressEntityOfStoreEntityCollectionNewStoreEntity = storeEntityCollectionNewStoreEntity.getAddressEntity();
                    storeEntityCollectionNewStoreEntity.setAddressEntity(addressEntity);
                    storeEntityCollectionNewStoreEntity = em.merge(storeEntityCollectionNewStoreEntity);
                    if (oldAddressEntityOfStoreEntityCollectionNewStoreEntity != null && !oldAddressEntityOfStoreEntityCollectionNewStoreEntity.equals(addressEntity))
                    {
                        oldAddressEntityOfStoreEntityCollectionNewStoreEntity.getStoreEntityCollection().remove(storeEntityCollectionNewStoreEntity);
                        oldAddressEntityOfStoreEntityCollectionNewStoreEntity = em.merge(oldAddressEntityOfStoreEntityCollectionNewStoreEntity);
                    }
                }
            }
            for (CustomerEntity customerEntityCollectionNewCustomerEntity : customerEntityCollectionNew)
            {
                if (!customerEntityCollectionOld.contains(customerEntityCollectionNewCustomerEntity))
                {
                    AddressEntity oldAddressEntityOfCustomerEntityCollectionNewCustomerEntity = customerEntityCollectionNewCustomerEntity.getAddressEntity();
                    customerEntityCollectionNewCustomerEntity.setAddressEntity(addressEntity);
                    customerEntityCollectionNewCustomerEntity = em.merge(customerEntityCollectionNewCustomerEntity);
                    if (oldAddressEntityOfCustomerEntityCollectionNewCustomerEntity != null && !oldAddressEntityOfCustomerEntityCollectionNewCustomerEntity.equals(addressEntity))
                    {
                        oldAddressEntityOfCustomerEntityCollectionNewCustomerEntity.getCustomerEntityCollection().remove(customerEntityCollectionNewCustomerEntity);
                        oldAddressEntityOfCustomerEntityCollectionNewCustomerEntity = em.merge(oldAddressEntityOfCustomerEntityCollectionNewCustomerEntity);
                    }
                }
            }
            for (StaffEntity staffEntityCollectionNewStaffEntity : staffEntityCollectionNew)
            {
                if (!staffEntityCollectionOld.contains(staffEntityCollectionNewStaffEntity))
                {
                    AddressEntity oldAddressEntityOfStaffEntityCollectionNewStaffEntity = staffEntityCollectionNewStaffEntity.getAddressEntity();
                    staffEntityCollectionNewStaffEntity.setAddressEntity(addressEntity);
                    staffEntityCollectionNewStaffEntity = em.merge(staffEntityCollectionNewStaffEntity);
                    if (oldAddressEntityOfStaffEntityCollectionNewStaffEntity != null && !oldAddressEntityOfStaffEntityCollectionNewStaffEntity.equals(addressEntity))
                    {
                        oldAddressEntityOfStaffEntityCollectionNewStaffEntity.getStaffEntityCollection().remove(staffEntityCollectionNewStaffEntity);
                        oldAddressEntityOfStaffEntityCollectionNewStaffEntity = em.merge(oldAddressEntityOfStaffEntityCollectionNewStaffEntity);
                    }
                }
            }
            em.getTransaction().commit();
        }
        catch (Exception ex)
        {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0)
            {
                Short id = addressEntity.getAddressId();
                if (findAddressEntity(id) == null)
                {
                    throw new NonexistentEntityException("The addressEntity with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
        finally
        {
            if (em != null)
            {
                em.close();
            }
        }
    }

    public void destroy(Short id) throws IllegalOrphanException, NonexistentEntityException
    {
        EntityManager em = null;
        try
        {
            em = getEntityManager();
            em.getTransaction().begin();
            AddressEntity addressEntity;
            try
            {
                addressEntity = em.getReference(AddressEntity.class, id);
                addressEntity.getAddressId();
            }
            catch (EntityNotFoundException enfe)
            {
                throw new NonexistentEntityException("The addressEntity with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<StoreEntity> storeEntityCollectionOrphanCheck = addressEntity.getStoreEntityCollection();
            for (StoreEntity storeEntityCollectionOrphanCheckStoreEntity : storeEntityCollectionOrphanCheck)
            {
                if (illegalOrphanMessages == null)
                {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This AddressEntity (" + addressEntity + ") cannot be destroyed since the StoreEntity " + storeEntityCollectionOrphanCheckStoreEntity + " in its storeEntityCollection field has a non-nullable addressEntity field.");
            }
            Collection<CustomerEntity> customerEntityCollectionOrphanCheck = addressEntity.getCustomerEntityCollection();
            for (CustomerEntity customerEntityCollectionOrphanCheckCustomerEntity : customerEntityCollectionOrphanCheck)
            {
                if (illegalOrphanMessages == null)
                {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This AddressEntity (" + addressEntity + ") cannot be destroyed since the CustomerEntity " + customerEntityCollectionOrphanCheckCustomerEntity + " in its customerEntityCollection field has a non-nullable addressEntity field.");
            }
            Collection<StaffEntity> staffEntityCollectionOrphanCheck = addressEntity.getStaffEntityCollection();
            for (StaffEntity staffEntityCollectionOrphanCheckStaffEntity : staffEntityCollectionOrphanCheck)
            {
                if (illegalOrphanMessages == null)
                {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This AddressEntity (" + addressEntity + ") cannot be destroyed since the StaffEntity " + staffEntityCollectionOrphanCheckStaffEntity + " in its staffEntityCollection field has a non-nullable addressEntity field.");
            }
            if (illegalOrphanMessages != null)
            {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            CityEntity cityEntity = addressEntity.getCityEntity();
            if (cityEntity != null)
            {
                cityEntity.getAddressEntityCollection().remove(addressEntity);
                cityEntity = em.merge(cityEntity);
            }
            em.remove(addressEntity);
            em.getTransaction().commit();
        }
        finally
        {
            if (em != null)
            {
                em.close();
            }
        }
    }

    public List<AddressEntity> findAddressEntityEntities()
    {
        return findAddressEntityEntities(true, -1, -1);
    }

    public List<AddressEntity> findAddressEntityEntities(int maxResults, int firstResult)
    {
        return findAddressEntityEntities(false, maxResults, firstResult);
    }

    private List<AddressEntity> findAddressEntityEntities(boolean all, int maxResults, int firstResult)
    {
        EntityManager em = getEntityManager();
        try
        {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(AddressEntity.class));
            Query q = em.createQuery(cq);
            if (!all)
            {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        }
        finally
        {
            em.close();
        }
    }

    public AddressEntity findAddressEntity(Short id)
    {
        EntityManager em = getEntityManager();
        try
        {
            return em.find(AddressEntity.class, id);
        }
        finally
        {
            em.close();
        }
    }

    public int getAddressEntityCount()
    {
        EntityManager em = getEntityManager();
        try
        {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<AddressEntity> rt = cq.from(AddressEntity.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        }
        finally
        {
            em.close();
        }
    }
    
}

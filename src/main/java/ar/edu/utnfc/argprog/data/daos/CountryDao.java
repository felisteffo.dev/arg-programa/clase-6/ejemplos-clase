/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ar.edu.utnfc.argprog.data.daos;

import ar.edu.utnfc.argprog.data.daos.exceptions.IllegalOrphanException;
import ar.edu.utnfc.argprog.data.daos.exceptions.NonexistentEntityException;
import java.io.Serializable;
import jakarta.persistence.Query;
import jakarta.persistence.EntityNotFoundException;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Root;
import ar.edu.utnfc.argprog.data.entities.CityEntity;
import ar.edu.utnfc.argprog.data.entities.CountryEntity;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;

/**
 *
 * @author Felipe
 */
public class CountryDao implements Serializable
{

    public CountryDao(EntityManagerFactory emf)
    {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager()
    {
        return emf.createEntityManager();
    }

    public void create(CountryEntity countryEntity)
    {
        if (countryEntity.getCityEntityCollection() == null)
        {
            countryEntity.setCityEntityCollection(new ArrayList<CityEntity>());
        }
        EntityManager em = null;
        try
        {
            em = getEntityManager();
            em.getTransaction().begin();
            Collection<CityEntity> attachedCityEntityCollection = new ArrayList<CityEntity>();
            for (CityEntity cityEntityCollectionCityEntityToAttach : countryEntity.getCityEntityCollection())
            {
                cityEntityCollectionCityEntityToAttach = em.getReference(cityEntityCollectionCityEntityToAttach.getClass(), cityEntityCollectionCityEntityToAttach.getCityId());
                attachedCityEntityCollection.add(cityEntityCollectionCityEntityToAttach);
            }
            countryEntity.setCityEntityCollection(attachedCityEntityCollection);
            em.persist(countryEntity);
            for (CityEntity cityEntityCollectionCityEntity : countryEntity.getCityEntityCollection())
            {
                CountryEntity oldCountryEntityOfCityEntityCollectionCityEntity = cityEntityCollectionCityEntity.getCountryEntity();
                cityEntityCollectionCityEntity.setCountryEntity(countryEntity);
                cityEntityCollectionCityEntity = em.merge(cityEntityCollectionCityEntity);
                if (oldCountryEntityOfCityEntityCollectionCityEntity != null)
                {
                    oldCountryEntityOfCityEntityCollectionCityEntity.getCityEntityCollection().remove(cityEntityCollectionCityEntity);
                    oldCountryEntityOfCityEntityCollectionCityEntity = em.merge(oldCountryEntityOfCityEntityCollectionCityEntity);
                }
            }
            em.getTransaction().commit();
        }
        finally
        {
            if (em != null)
            {
                em.close();
            }
        }
    }

    public void edit(CountryEntity countryEntity) throws IllegalOrphanException, NonexistentEntityException, Exception
    {
        EntityManager em = null;
        try
        {
            em = getEntityManager();
            em.getTransaction().begin();
            CountryEntity persistentCountryEntity = em.find(CountryEntity.class, countryEntity.getCountryId());
            Collection<CityEntity> cityEntityCollectionOld = persistentCountryEntity.getCityEntityCollection();
            Collection<CityEntity> cityEntityCollectionNew = countryEntity.getCityEntityCollection();
            List<String> illegalOrphanMessages = null;
            for (CityEntity cityEntityCollectionOldCityEntity : cityEntityCollectionOld)
            {
                if (!cityEntityCollectionNew.contains(cityEntityCollectionOldCityEntity))
                {
                    if (illegalOrphanMessages == null)
                    {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain CityEntity " + cityEntityCollectionOldCityEntity + " since its countryEntity field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null)
            {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Collection<CityEntity> attachedCityEntityCollectionNew = new ArrayList<CityEntity>();
            for (CityEntity cityEntityCollectionNewCityEntityToAttach : cityEntityCollectionNew)
            {
                cityEntityCollectionNewCityEntityToAttach = em.getReference(cityEntityCollectionNewCityEntityToAttach.getClass(), cityEntityCollectionNewCityEntityToAttach.getCityId());
                attachedCityEntityCollectionNew.add(cityEntityCollectionNewCityEntityToAttach);
            }
            cityEntityCollectionNew = attachedCityEntityCollectionNew;
            countryEntity.setCityEntityCollection(cityEntityCollectionNew);
            countryEntity = em.merge(countryEntity);
            for (CityEntity cityEntityCollectionNewCityEntity : cityEntityCollectionNew)
            {
                if (!cityEntityCollectionOld.contains(cityEntityCollectionNewCityEntity))
                {
                    CountryEntity oldCountryEntityOfCityEntityCollectionNewCityEntity = cityEntityCollectionNewCityEntity.getCountryEntity();
                    cityEntityCollectionNewCityEntity.setCountryEntity(countryEntity);
                    cityEntityCollectionNewCityEntity = em.merge(cityEntityCollectionNewCityEntity);
                    if (oldCountryEntityOfCityEntityCollectionNewCityEntity != null && !oldCountryEntityOfCityEntityCollectionNewCityEntity.equals(countryEntity))
                    {
                        oldCountryEntityOfCityEntityCollectionNewCityEntity.getCityEntityCollection().remove(cityEntityCollectionNewCityEntity);
                        oldCountryEntityOfCityEntityCollectionNewCityEntity = em.merge(oldCountryEntityOfCityEntityCollectionNewCityEntity);
                    }
                }
            }
            em.getTransaction().commit();
        }
        catch (Exception ex)
        {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0)
            {
                Short id = countryEntity.getCountryId();
                if (findCountryEntity(id) == null)
                {
                    throw new NonexistentEntityException("The countryEntity with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
        finally
        {
            if (em != null)
            {
                em.close();
            }
        }
    }

    public void destroy(Short id) throws IllegalOrphanException, NonexistentEntityException
    {
        EntityManager em = null;
        try
        {
            em = getEntityManager();
            em.getTransaction().begin();
            CountryEntity countryEntity;
            try
            {
                countryEntity = em.getReference(CountryEntity.class, id);
                countryEntity.getCountryId();
            }
            catch (EntityNotFoundException enfe)
            {
                throw new NonexistentEntityException("The countryEntity with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<CityEntity> cityEntityCollectionOrphanCheck = countryEntity.getCityEntityCollection();
            for (CityEntity cityEntityCollectionOrphanCheckCityEntity : cityEntityCollectionOrphanCheck)
            {
                if (illegalOrphanMessages == null)
                {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This CountryEntity (" + countryEntity + ") cannot be destroyed since the CityEntity " + cityEntityCollectionOrphanCheckCityEntity + " in its cityEntityCollection field has a non-nullable countryEntity field.");
            }
            if (illegalOrphanMessages != null)
            {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(countryEntity);
            em.getTransaction().commit();
        }
        finally
        {
            if (em != null)
            {
                em.close();
            }
        }
    }

    public List<CountryEntity> findCountryEntityEntities()
    {
        return findCountryEntityEntities(true, -1, -1);
    }

    public List<CountryEntity> findCountryEntityEntities(int maxResults, int firstResult)
    {
        return findCountryEntityEntities(false, maxResults, firstResult);
    }

    private List<CountryEntity> findCountryEntityEntities(boolean all, int maxResults, int firstResult)
    {
        EntityManager em = getEntityManager();
        try
        {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(CountryEntity.class));
            Query q = em.createQuery(cq);
            if (!all)
            {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        }
        finally
        {
            em.close();
        }
    }

    public CountryEntity findCountryEntity(Short id)
    {
        EntityManager em = getEntityManager();
        try
        {
            return em.find(CountryEntity.class, id);
        }
        finally
        {
            em.close();
        }
    }

    public int getCountryEntityCount()
    {
        EntityManager em = getEntityManager();
        try
        {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<CountryEntity> rt = cq.from(CountryEntity.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        }
        finally
        {
            em.close();
        }
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ar.edu.utnfc.argprog.data.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import jakarta.persistence.Basic;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;

/**
 *
 * @author Felipe
 */
@Entity
@Table(name = "address")
@NamedQueries(
{
    @NamedQuery(name = "AddressEntity.findAll", query = "SELECT a FROM AddressEntity a"),
    @NamedQuery(name = "AddressEntity.findByAddressId", query = "SELECT a FROM AddressEntity a WHERE a.addressId = :addressId"),
    @NamedQuery(name = "AddressEntity.findByAddress", query = "SELECT a FROM AddressEntity a WHERE a.address = :address"),
    @NamedQuery(name = "AddressEntity.findByAddress2", query = "SELECT a FROM AddressEntity a WHERE a.address2 = :address2"),
    @NamedQuery(name = "AddressEntity.findByDistrict", query = "SELECT a FROM AddressEntity a WHERE a.district = :district"),
    @NamedQuery(name = "AddressEntity.findByPostalCode", query = "SELECT a FROM AddressEntity a WHERE a.postalCode = :postalCode"),
    @NamedQuery(name = "AddressEntity.findByPhone", query = "SELECT a FROM AddressEntity a WHERE a.phone = :phone"),
    @NamedQuery(name = "AddressEntity.findByLastUpdate", query = "SELECT a FROM AddressEntity a WHERE a.lastUpdate = :lastUpdate")
})
public class AddressEntity implements Serializable
{
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "address_id")
    private Short addressId;
    @Basic(optional = false)
    private String address;
    private String address2;
    @Basic(optional = false)
    private String district;
    @Column(name = "postal_code")
    private String postalCode;
    @Basic(optional = false)
    private String phone;
    @Basic(optional = false)
    @Column(name = "last_update")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastUpdate;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "addressEntity")
    private Collection<StoreEntity> storeEntityCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "addressEntity")
    private Collection<CustomerEntity> customerEntityCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "addressEntity")
    private Collection<StaffEntity> staffEntityCollection;
    @JoinColumn(name = "city_id", referencedColumnName = "city_id")
    @ManyToOne(optional = false)
    private CityEntity cityEntity;

    public AddressEntity()
    {
    }

    public AddressEntity(Short addressId)
    {
        this.addressId = addressId;
    }

    public AddressEntity(Short addressId, String address, String district, String phone, Date lastUpdate)
    {
        this.addressId = addressId;
        this.address = address;
        this.district = district;
        this.phone = phone;
        this.lastUpdate = lastUpdate;
    }

    public Short getAddressId()
    {
        return addressId;
    }

    public void setAddressId(Short addressId)
    {
        this.addressId = addressId;
    }

    public String getAddress()
    {
        return address;
    }

    public void setAddress(String address)
    {
        this.address = address;
    }

    public String getAddress2()
    {
        return address2;
    }

    public void setAddress2(String address2)
    {
        this.address2 = address2;
    }

    public String getDistrict()
    {
        return district;
    }

    public void setDistrict(String district)
    {
        this.district = district;
    }

    public String getPostalCode()
    {
        return postalCode;
    }

    public void setPostalCode(String postalCode)
    {
        this.postalCode = postalCode;
    }

    public String getPhone()
    {
        return phone;
    }

    public void setPhone(String phone)
    {
        this.phone = phone;
    }

    public Date getLastUpdate()
    {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate)
    {
        this.lastUpdate = lastUpdate;
    }

    public Collection<StoreEntity> getStoreEntityCollection()
    {
        return storeEntityCollection;
    }

    public void setStoreEntityCollection(Collection<StoreEntity> storeEntityCollection)
    {
        this.storeEntityCollection = storeEntityCollection;
    }

    public Collection<CustomerEntity> getCustomerEntityCollection()
    {
        return customerEntityCollection;
    }

    public void setCustomerEntityCollection(Collection<CustomerEntity> customerEntityCollection)
    {
        this.customerEntityCollection = customerEntityCollection;
    }

    public Collection<StaffEntity> getStaffEntityCollection()
    {
        return staffEntityCollection;
    }

    public void setStaffEntityCollection(Collection<StaffEntity> staffEntityCollection)
    {
        this.staffEntityCollection = staffEntityCollection;
    }

    public CityEntity getCityEntity()
    {
        return cityEntity;
    }

    public void setCityEntity(CityEntity cityEntity)
    {
        this.cityEntity = cityEntity;
    }

    @Override
    public int hashCode()
    {
        int hash = 0;
        hash += (addressId != null ? addressId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object)
    {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AddressEntity))
        {
            return false;
        }
        AddressEntity other = (AddressEntity) object;
        if ((this.addressId == null && other.addressId != null) || (this.addressId != null && !this.addressId.equals(other.addressId)))
        {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return "ar.edu.utnfc.argprog.data.entities.AddressEntity[ addressId=" + addressId + " ]";
    }
    
}

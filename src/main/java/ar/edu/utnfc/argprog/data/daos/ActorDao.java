/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ar.edu.utnfc.argprog.data.daos;

import ar.edu.utnfc.argprog.data.daos.exceptions.IllegalOrphanException;
import ar.edu.utnfc.argprog.data.daos.exceptions.NonexistentEntityException;
import ar.edu.utnfc.argprog.data.entities.ActorEntity;
import ar.edu.utnfc.argprog.data.entities.FilmActorEntity;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.EntityNotFoundException;
import jakarta.persistence.Query;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Root;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Felipe
 */
public class ActorDao implements Serializable
{

    public ActorDao(EntityManagerFactory emf)
    {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager()
    {
        return emf.createEntityManager();
    }

    public void create(ActorEntity actorEntity)
    {
        if (actorEntity.getFilmActorEntityCollection() == null)
        {
            actorEntity.setFilmActorEntityCollection(new ArrayList<FilmActorEntity>());
        }
        EntityManager em = null;
        try
        {
            em = getEntityManager();
            em.getTransaction().begin();
            Collection<FilmActorEntity> attachedFilmActorEntityCollection = new ArrayList<FilmActorEntity>();
            for (FilmActorEntity filmActorEntityCollectionFilmActorEntityToAttach : actorEntity.getFilmActorEntityCollection())
            {
                filmActorEntityCollectionFilmActorEntityToAttach = em.getReference(filmActorEntityCollectionFilmActorEntityToAttach.getClass(), filmActorEntityCollectionFilmActorEntityToAttach.getFilmActorEntityPK());
                attachedFilmActorEntityCollection.add(filmActorEntityCollectionFilmActorEntityToAttach);
            }
            actorEntity.setFilmActorEntityCollection(attachedFilmActorEntityCollection);
            em.persist(actorEntity);
            for (FilmActorEntity filmActorEntityCollectionFilmActorEntity : actorEntity.getFilmActorEntityCollection())
            {
                ActorEntity oldActorEntityOfFilmActorEntityCollectionFilmActorEntity = filmActorEntityCollectionFilmActorEntity.getActorEntity();
                filmActorEntityCollectionFilmActorEntity.setActorEntity(actorEntity);
                filmActorEntityCollectionFilmActorEntity = em.merge(filmActorEntityCollectionFilmActorEntity);
                if (oldActorEntityOfFilmActorEntityCollectionFilmActorEntity != null)
                {
                    oldActorEntityOfFilmActorEntityCollectionFilmActorEntity.getFilmActorEntityCollection().remove(filmActorEntityCollectionFilmActorEntity);
                    oldActorEntityOfFilmActorEntityCollectionFilmActorEntity = em.merge(oldActorEntityOfFilmActorEntityCollectionFilmActorEntity);
                }
            }
            em.getTransaction().commit();
        }
        finally
        {
            if (em != null)
            {
                em.close();
            }
        }
    }

    public void edit(ActorEntity actorEntity) throws IllegalOrphanException, NonexistentEntityException, Exception
    {
        EntityManager em = null;
        try
        {
            em = getEntityManager();
            em.getTransaction().begin();
            ActorEntity persistentActorEntity = em.find(ActorEntity.class, actorEntity.getActorId());
            Collection<FilmActorEntity> filmActorEntityCollectionOld = persistentActorEntity.getFilmActorEntityCollection();
            Collection<FilmActorEntity> filmActorEntityCollectionNew = actorEntity.getFilmActorEntityCollection();
            List<String> illegalOrphanMessages = null;
            for (FilmActorEntity filmActorEntityCollectionOldFilmActorEntity : filmActorEntityCollectionOld)
            {
                if (!filmActorEntityCollectionNew.contains(filmActorEntityCollectionOldFilmActorEntity))
                {
                    if (illegalOrphanMessages == null)
                    {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain FilmActorEntity " + filmActorEntityCollectionOldFilmActorEntity + " since its actorEntity field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null)
            {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Collection<FilmActorEntity> attachedFilmActorEntityCollectionNew = new ArrayList<FilmActorEntity>();
            for (FilmActorEntity filmActorEntityCollectionNewFilmActorEntityToAttach : filmActorEntityCollectionNew)
            {
                filmActorEntityCollectionNewFilmActorEntityToAttach = em.getReference(filmActorEntityCollectionNewFilmActorEntityToAttach.getClass(), filmActorEntityCollectionNewFilmActorEntityToAttach.getFilmActorEntityPK());
                attachedFilmActorEntityCollectionNew.add(filmActorEntityCollectionNewFilmActorEntityToAttach);
            }
            filmActorEntityCollectionNew = attachedFilmActorEntityCollectionNew;
            actorEntity.setFilmActorEntityCollection(filmActorEntityCollectionNew);
            actorEntity = em.merge(actorEntity);
            for (FilmActorEntity filmActorEntityCollectionNewFilmActorEntity : filmActorEntityCollectionNew)
            {
                if (!filmActorEntityCollectionOld.contains(filmActorEntityCollectionNewFilmActorEntity))
                {
                    ActorEntity oldActorEntityOfFilmActorEntityCollectionNewFilmActorEntity = filmActorEntityCollectionNewFilmActorEntity.getActorEntity();
                    filmActorEntityCollectionNewFilmActorEntity.setActorEntity(actorEntity);
                    filmActorEntityCollectionNewFilmActorEntity = em.merge(filmActorEntityCollectionNewFilmActorEntity);
                    if (oldActorEntityOfFilmActorEntityCollectionNewFilmActorEntity != null && !oldActorEntityOfFilmActorEntityCollectionNewFilmActorEntity.equals(actorEntity))
                    {
                        oldActorEntityOfFilmActorEntityCollectionNewFilmActorEntity.getFilmActorEntityCollection().remove(filmActorEntityCollectionNewFilmActorEntity);
                        oldActorEntityOfFilmActorEntityCollectionNewFilmActorEntity = em.merge(oldActorEntityOfFilmActorEntityCollectionNewFilmActorEntity);
                    }
                }
            }
            em.getTransaction().commit();
        }
        catch (Exception ex)
        {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0)
            {
                Short id = actorEntity.getActorId();
                if (findActorEntity(id) == null)
                {
                    throw new NonexistentEntityException("The actorEntity with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
        finally
        {
            if (em != null)
            {
                em.close();
            }
        }
    }

    public void destroy(Short id) throws IllegalOrphanException, NonexistentEntityException
    {
        EntityManager em = null;
        try
        {
            em = getEntityManager();
            em.getTransaction().begin();
            ActorEntity actorEntity;
            try
            {
                actorEntity = em.getReference(ActorEntity.class, id);
                actorEntity.getActorId();
            }
            catch (EntityNotFoundException enfe)
            {
                throw new NonexistentEntityException("The actorEntity with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<FilmActorEntity> filmActorEntityCollectionOrphanCheck = actorEntity.getFilmActorEntityCollection();
            for (FilmActorEntity filmActorEntityCollectionOrphanCheckFilmActorEntity : filmActorEntityCollectionOrphanCheck)
            {
                if (illegalOrphanMessages == null)
                {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This ActorEntity (" + actorEntity + ") cannot be destroyed since the FilmActorEntity " + filmActorEntityCollectionOrphanCheckFilmActorEntity + " in its filmActorEntityCollection field has a non-nullable actorEntity field.");
            }
            if (illegalOrphanMessages != null)
            {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(actorEntity);
            em.getTransaction().commit();
        }
        finally
        {
            if (em != null)
            {
                em.close();
            }
        }
    }

    public List<ActorEntity> findActorEntityEntities()
    {
        return findActorEntityEntities(true, -1, -1);
    }

    public List<ActorEntity> findActorEntityEntities(int maxResults, int firstResult)
    {
        return findActorEntityEntities(false, maxResults, firstResult);
    }

    private List<ActorEntity> findActorEntityEntities(boolean all, int maxResults, int firstResult)
    {
        EntityManager em = getEntityManager();
        try
        {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(ActorEntity.class));
            Query q = em.createQuery(cq);
            if (!all)
            {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        }
        finally
        {
            em.close();
        }
    }

    public ActorEntity findActorEntity(Short id)
    {
        EntityManager em = getEntityManager();
        try
        {
            return em.find(ActorEntity.class, id);
        }
        finally
        {
            em.close();
        }
    }

    public int getActorEntityCount()
    {
        EntityManager em = getEntityManager();
        try
        {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<ActorEntity> rt = cq.from(ActorEntity.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        }
        finally
        {
            em.close();
        }
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ar.edu.utnfc.argprog.data.daos;

import ar.edu.utnfc.argprog.data.daos.exceptions.NonexistentEntityException;
import java.io.Serializable;
import jakarta.persistence.Query;
import jakarta.persistence.EntityNotFoundException;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Root;
import ar.edu.utnfc.argprog.data.entities.CustomerEntity;
import ar.edu.utnfc.argprog.data.entities.InventoryEntity;
import ar.edu.utnfc.argprog.data.entities.StaffEntity;
import ar.edu.utnfc.argprog.data.entities.PaymentEntity;
import ar.edu.utnfc.argprog.data.entities.RentalEntity;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;

/**
 *
 * @author Felipe
 */
public class RentalDao implements Serializable
{

    public RentalDao(EntityManagerFactory emf)
    {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager()
    {
        return emf.createEntityManager();
    }

    public void create(RentalEntity rentalEntity)
    {
        if (rentalEntity.getPaymentEntityCollection() == null)
        {
            rentalEntity.setPaymentEntityCollection(new ArrayList<PaymentEntity>());
        }
        EntityManager em = null;
        try
        {
            em = getEntityManager();
            em.getTransaction().begin();
            CustomerEntity customerEntity = rentalEntity.getCustomerEntity();
            if (customerEntity != null)
            {
                customerEntity = em.getReference(customerEntity.getClass(), customerEntity.getCustomerId());
                rentalEntity.setCustomerEntity(customerEntity);
            }
            InventoryEntity inventoryEntity = rentalEntity.getInventoryEntity();
            if (inventoryEntity != null)
            {
                inventoryEntity = em.getReference(inventoryEntity.getClass(), inventoryEntity.getInventoryId());
                rentalEntity.setInventoryEntity(inventoryEntity);
            }
            StaffEntity staffEntity = rentalEntity.getStaffEntity();
            if (staffEntity != null)
            {
                staffEntity = em.getReference(staffEntity.getClass(), staffEntity.getStaffId());
                rentalEntity.setStaffEntity(staffEntity);
            }
            Collection<PaymentEntity> attachedPaymentEntityCollection = new ArrayList<PaymentEntity>();
            for (PaymentEntity paymentEntityCollectionPaymentEntityToAttach : rentalEntity.getPaymentEntityCollection())
            {
                paymentEntityCollectionPaymentEntityToAttach = em.getReference(paymentEntityCollectionPaymentEntityToAttach.getClass(), paymentEntityCollectionPaymentEntityToAttach.getPaymentId());
                attachedPaymentEntityCollection.add(paymentEntityCollectionPaymentEntityToAttach);
            }
            rentalEntity.setPaymentEntityCollection(attachedPaymentEntityCollection);
            em.persist(rentalEntity);
            if (customerEntity != null)
            {
                customerEntity.getRentalEntityCollection().add(rentalEntity);
                customerEntity = em.merge(customerEntity);
            }
            if (inventoryEntity != null)
            {
                inventoryEntity.getRentalEntityCollection().add(rentalEntity);
                inventoryEntity = em.merge(inventoryEntity);
            }
            if (staffEntity != null)
            {
                staffEntity.getRentalEntityCollection().add(rentalEntity);
                staffEntity = em.merge(staffEntity);
            }
            for (PaymentEntity paymentEntityCollectionPaymentEntity : rentalEntity.getPaymentEntityCollection())
            {
                RentalEntity oldRentalEntityOfPaymentEntityCollectionPaymentEntity = paymentEntityCollectionPaymentEntity.getRentalEntity();
                paymentEntityCollectionPaymentEntity.setRentalEntity(rentalEntity);
                paymentEntityCollectionPaymentEntity = em.merge(paymentEntityCollectionPaymentEntity);
                if (oldRentalEntityOfPaymentEntityCollectionPaymentEntity != null)
                {
                    oldRentalEntityOfPaymentEntityCollectionPaymentEntity.getPaymentEntityCollection().remove(paymentEntityCollectionPaymentEntity);
                    oldRentalEntityOfPaymentEntityCollectionPaymentEntity = em.merge(oldRentalEntityOfPaymentEntityCollectionPaymentEntity);
                }
            }
            em.getTransaction().commit();
        }
        finally
        {
            if (em != null)
            {
                em.close();
            }
        }
    }

    public void edit(RentalEntity rentalEntity) throws NonexistentEntityException, Exception
    {
        EntityManager em = null;
        try
        {
            em = getEntityManager();
            em.getTransaction().begin();
            RentalEntity persistentRentalEntity = em.find(RentalEntity.class, rentalEntity.getRentalId());
            CustomerEntity customerEntityOld = persistentRentalEntity.getCustomerEntity();
            CustomerEntity customerEntityNew = rentalEntity.getCustomerEntity();
            InventoryEntity inventoryEntityOld = persistentRentalEntity.getInventoryEntity();
            InventoryEntity inventoryEntityNew = rentalEntity.getInventoryEntity();
            StaffEntity staffEntityOld = persistentRentalEntity.getStaffEntity();
            StaffEntity staffEntityNew = rentalEntity.getStaffEntity();
            Collection<PaymentEntity> paymentEntityCollectionOld = persistentRentalEntity.getPaymentEntityCollection();
            Collection<PaymentEntity> paymentEntityCollectionNew = rentalEntity.getPaymentEntityCollection();
            if (customerEntityNew != null)
            {
                customerEntityNew = em.getReference(customerEntityNew.getClass(), customerEntityNew.getCustomerId());
                rentalEntity.setCustomerEntity(customerEntityNew);
            }
            if (inventoryEntityNew != null)
            {
                inventoryEntityNew = em.getReference(inventoryEntityNew.getClass(), inventoryEntityNew.getInventoryId());
                rentalEntity.setInventoryEntity(inventoryEntityNew);
            }
            if (staffEntityNew != null)
            {
                staffEntityNew = em.getReference(staffEntityNew.getClass(), staffEntityNew.getStaffId());
                rentalEntity.setStaffEntity(staffEntityNew);
            }
            Collection<PaymentEntity> attachedPaymentEntityCollectionNew = new ArrayList<PaymentEntity>();
            for (PaymentEntity paymentEntityCollectionNewPaymentEntityToAttach : paymentEntityCollectionNew)
            {
                paymentEntityCollectionNewPaymentEntityToAttach = em.getReference(paymentEntityCollectionNewPaymentEntityToAttach.getClass(), paymentEntityCollectionNewPaymentEntityToAttach.getPaymentId());
                attachedPaymentEntityCollectionNew.add(paymentEntityCollectionNewPaymentEntityToAttach);
            }
            paymentEntityCollectionNew = attachedPaymentEntityCollectionNew;
            rentalEntity.setPaymentEntityCollection(paymentEntityCollectionNew);
            rentalEntity = em.merge(rentalEntity);
            if (customerEntityOld != null && !customerEntityOld.equals(customerEntityNew))
            {
                customerEntityOld.getRentalEntityCollection().remove(rentalEntity);
                customerEntityOld = em.merge(customerEntityOld);
            }
            if (customerEntityNew != null && !customerEntityNew.equals(customerEntityOld))
            {
                customerEntityNew.getRentalEntityCollection().add(rentalEntity);
                customerEntityNew = em.merge(customerEntityNew);
            }
            if (inventoryEntityOld != null && !inventoryEntityOld.equals(inventoryEntityNew))
            {
                inventoryEntityOld.getRentalEntityCollection().remove(rentalEntity);
                inventoryEntityOld = em.merge(inventoryEntityOld);
            }
            if (inventoryEntityNew != null && !inventoryEntityNew.equals(inventoryEntityOld))
            {
                inventoryEntityNew.getRentalEntityCollection().add(rentalEntity);
                inventoryEntityNew = em.merge(inventoryEntityNew);
            }
            if (staffEntityOld != null && !staffEntityOld.equals(staffEntityNew))
            {
                staffEntityOld.getRentalEntityCollection().remove(rentalEntity);
                staffEntityOld = em.merge(staffEntityOld);
            }
            if (staffEntityNew != null && !staffEntityNew.equals(staffEntityOld))
            {
                staffEntityNew.getRentalEntityCollection().add(rentalEntity);
                staffEntityNew = em.merge(staffEntityNew);
            }
            for (PaymentEntity paymentEntityCollectionOldPaymentEntity : paymentEntityCollectionOld)
            {
                if (!paymentEntityCollectionNew.contains(paymentEntityCollectionOldPaymentEntity))
                {
                    paymentEntityCollectionOldPaymentEntity.setRentalEntity(null);
                    paymentEntityCollectionOldPaymentEntity = em.merge(paymentEntityCollectionOldPaymentEntity);
                }
            }
            for (PaymentEntity paymentEntityCollectionNewPaymentEntity : paymentEntityCollectionNew)
            {
                if (!paymentEntityCollectionOld.contains(paymentEntityCollectionNewPaymentEntity))
                {
                    RentalEntity oldRentalEntityOfPaymentEntityCollectionNewPaymentEntity = paymentEntityCollectionNewPaymentEntity.getRentalEntity();
                    paymentEntityCollectionNewPaymentEntity.setRentalEntity(rentalEntity);
                    paymentEntityCollectionNewPaymentEntity = em.merge(paymentEntityCollectionNewPaymentEntity);
                    if (oldRentalEntityOfPaymentEntityCollectionNewPaymentEntity != null && !oldRentalEntityOfPaymentEntityCollectionNewPaymentEntity.equals(rentalEntity))
                    {
                        oldRentalEntityOfPaymentEntityCollectionNewPaymentEntity.getPaymentEntityCollection().remove(paymentEntityCollectionNewPaymentEntity);
                        oldRentalEntityOfPaymentEntityCollectionNewPaymentEntity = em.merge(oldRentalEntityOfPaymentEntityCollectionNewPaymentEntity);
                    }
                }
            }
            em.getTransaction().commit();
        }
        catch (Exception ex)
        {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0)
            {
                Integer id = rentalEntity.getRentalId();
                if (findRentalEntity(id) == null)
                {
                    throw new NonexistentEntityException("The rentalEntity with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
        finally
        {
            if (em != null)
            {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException
    {
        EntityManager em = null;
        try
        {
            em = getEntityManager();
            em.getTransaction().begin();
            RentalEntity rentalEntity;
            try
            {
                rentalEntity = em.getReference(RentalEntity.class, id);
                rentalEntity.getRentalId();
            }
            catch (EntityNotFoundException enfe)
            {
                throw new NonexistentEntityException("The rentalEntity with id " + id + " no longer exists.", enfe);
            }
            CustomerEntity customerEntity = rentalEntity.getCustomerEntity();
            if (customerEntity != null)
            {
                customerEntity.getRentalEntityCollection().remove(rentalEntity);
                customerEntity = em.merge(customerEntity);
            }
            InventoryEntity inventoryEntity = rentalEntity.getInventoryEntity();
            if (inventoryEntity != null)
            {
                inventoryEntity.getRentalEntityCollection().remove(rentalEntity);
                inventoryEntity = em.merge(inventoryEntity);
            }
            StaffEntity staffEntity = rentalEntity.getStaffEntity();
            if (staffEntity != null)
            {
                staffEntity.getRentalEntityCollection().remove(rentalEntity);
                staffEntity = em.merge(staffEntity);
            }
            Collection<PaymentEntity> paymentEntityCollection = rentalEntity.getPaymentEntityCollection();
            for (PaymentEntity paymentEntityCollectionPaymentEntity : paymentEntityCollection)
            {
                paymentEntityCollectionPaymentEntity.setRentalEntity(null);
                paymentEntityCollectionPaymentEntity = em.merge(paymentEntityCollectionPaymentEntity);
            }
            em.remove(rentalEntity);
            em.getTransaction().commit();
        }
        finally
        {
            if (em != null)
            {
                em.close();
            }
        }
    }

    public List<RentalEntity> findRentalEntityEntities()
    {
        return findRentalEntityEntities(true, -1, -1);
    }

    public List<RentalEntity> findRentalEntityEntities(int maxResults, int firstResult)
    {
        return findRentalEntityEntities(false, maxResults, firstResult);
    }

    private List<RentalEntity> findRentalEntityEntities(boolean all, int maxResults, int firstResult)
    {
        EntityManager em = getEntityManager();
        try
        {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(RentalEntity.class));
            Query q = em.createQuery(cq);
            if (!all)
            {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        }
        finally
        {
            em.close();
        }
    }

    public RentalEntity findRentalEntity(Integer id)
    {
        EntityManager em = getEntityManager();
        try
        {
            return em.find(RentalEntity.class, id);
        }
        finally
        {
            em.close();
        }
    }

    public int getRentalEntityCount()
    {
        EntityManager em = getEntityManager();
        try
        {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<RentalEntity> rt = cq.from(RentalEntity.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        }
        finally
        {
            em.close();
        }
    }
    
}

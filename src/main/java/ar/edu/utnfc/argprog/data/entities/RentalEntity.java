/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ar.edu.utnfc.argprog.data.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import jakarta.persistence.Basic;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;

/**
 *
 * @author Felipe
 */
@Entity
@Table(name = "rental")
@NamedQueries(
{
    @NamedQuery(name = "RentalEntity.findAll", query = "SELECT r FROM RentalEntity r"),
    @NamedQuery(name = "RentalEntity.findByRentalId", query = "SELECT r FROM RentalEntity r WHERE r.rentalId = :rentalId"),
    @NamedQuery(name = "RentalEntity.findByRentalDate", query = "SELECT r FROM RentalEntity r WHERE r.rentalDate = :rentalDate"),
    @NamedQuery(name = "RentalEntity.findByReturnDate", query = "SELECT r FROM RentalEntity r WHERE r.returnDate = :returnDate"),
    @NamedQuery(name = "RentalEntity.findByLastUpdate", query = "SELECT r FROM RentalEntity r WHERE r.lastUpdate = :lastUpdate")
})
public class RentalEntity implements Serializable
{
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "rental_id")
    private Integer rentalId;
    @Basic(optional = false)
    @Column(name = "rental_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date rentalDate;
    @Column(name = "return_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date returnDate;
    @Basic(optional = false)
    @Column(name = "last_update")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastUpdate;
    @OneToMany(mappedBy = "rentalEntity")
    private Collection<PaymentEntity> paymentEntityCollection;
    @JoinColumn(name = "staff_id", referencedColumnName = "staff_id")
    @ManyToOne(optional = false)
    private StaffEntity staffEntity;
    @JoinColumn(name = "inventory_id", referencedColumnName = "inventory_id")
    @ManyToOne(optional = false)
    private InventoryEntity inventoryEntity;
    @JoinColumn(name = "customer_id", referencedColumnName = "customer_id")
    @ManyToOne(optional = false)
    private CustomerEntity customerEntity;

    public RentalEntity()
    {
    }

    public RentalEntity(Integer rentalId)
    {
        this.rentalId = rentalId;
    }

    public RentalEntity(Integer rentalId, Date rentalDate, Date lastUpdate)
    {
        this.rentalId = rentalId;
        this.rentalDate = rentalDate;
        this.lastUpdate = lastUpdate;
    }

    public Integer getRentalId()
    {
        return rentalId;
    }

    public void setRentalId(Integer rentalId)
    {
        this.rentalId = rentalId;
    }

    public Date getRentalDate()
    {
        return rentalDate;
    }

    public void setRentalDate(Date rentalDate)
    {
        this.rentalDate = rentalDate;
    }

    public Date getReturnDate()
    {
        return returnDate;
    }

    public void setReturnDate(Date returnDate)
    {
        this.returnDate = returnDate;
    }

    public Date getLastUpdate()
    {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate)
    {
        this.lastUpdate = lastUpdate;
    }

    public Collection<PaymentEntity> getPaymentEntityCollection()
    {
        return paymentEntityCollection;
    }

    public void setPaymentEntityCollection(Collection<PaymentEntity> paymentEntityCollection)
    {
        this.paymentEntityCollection = paymentEntityCollection;
    }

    public StaffEntity getStaffEntity()
    {
        return staffEntity;
    }

    public void setStaffEntity(StaffEntity staffEntity)
    {
        this.staffEntity = staffEntity;
    }

    public InventoryEntity getInventoryEntity()
    {
        return inventoryEntity;
    }

    public void setInventoryEntity(InventoryEntity inventoryEntity)
    {
        this.inventoryEntity = inventoryEntity;
    }

    public CustomerEntity getCustomerEntity()
    {
        return customerEntity;
    }

    public void setCustomerEntity(CustomerEntity customerEntity)
    {
        this.customerEntity = customerEntity;
    }

    @Override
    public int hashCode()
    {
        int hash = 0;
        hash += (rentalId != null ? rentalId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object)
    {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RentalEntity))
        {
            return false;
        }
        RentalEntity other = (RentalEntity) object;
        if ((this.rentalId == null && other.rentalId != null) || (this.rentalId != null && !this.rentalId.equals(other.rentalId)))
        {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return "ar.edu.utnfc.argprog.data.entities.RentalEntity[ rentalId=" + rentalId + " ]";
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ar.edu.utnfc.argprog.data.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import jakarta.persistence.Basic;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.Lob;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;

/**
 *
 * @author Felipe
 */
@Entity
@Table(name = "staff")
@NamedQueries(
{
    @NamedQuery(name = "StaffEntity.findAll", query = "SELECT s FROM StaffEntity s"),
    @NamedQuery(name = "StaffEntity.findByStaffId", query = "SELECT s FROM StaffEntity s WHERE s.staffId = :staffId"),
    @NamedQuery(name = "StaffEntity.findByFirstName", query = "SELECT s FROM StaffEntity s WHERE s.firstName = :firstName"),
    @NamedQuery(name = "StaffEntity.findByLastName", query = "SELECT s FROM StaffEntity s WHERE s.lastName = :lastName"),
    @NamedQuery(name = "StaffEntity.findByEmail", query = "SELECT s FROM StaffEntity s WHERE s.email = :email"),
    @NamedQuery(name = "StaffEntity.findByActive", query = "SELECT s FROM StaffEntity s WHERE s.active = :active"),
    @NamedQuery(name = "StaffEntity.findByUsername", query = "SELECT s FROM StaffEntity s WHERE s.username = :username"),
    @NamedQuery(name = "StaffEntity.findByPassword", query = "SELECT s FROM StaffEntity s WHERE s.password = :password"),
    @NamedQuery(name = "StaffEntity.findByLastUpdate", query = "SELECT s FROM StaffEntity s WHERE s.lastUpdate = :lastUpdate")
})
public class StaffEntity implements Serializable
{
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "staff_id")
    private Short staffId;
    @Basic(optional = false)
    @Column(name = "first_name")
    private String firstName;
    @Basic(optional = false)
    @Column(name = "last_name")
    private String lastName;
    @Lob
    private byte[] picture;
    private String email;
    @Basic(optional = false)
    private boolean active;
    @Basic(optional = false)
    private String username;
    private String password;
    @Basic(optional = false)
    @Column(name = "last_update")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastUpdate;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "staffEntity")
    private StoreEntity storeEntity;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "staffEntity")
    private Collection<PaymentEntity> paymentEntityCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "staffEntity")
    private Collection<RentalEntity> rentalEntityCollection;
    @JoinColumn(name = "store_id", referencedColumnName = "store_id")
    @ManyToOne(optional = false)
    private StoreEntity storeEntity1;
    @JoinColumn(name = "address_id", referencedColumnName = "address_id")
    @ManyToOne(optional = false)
    private AddressEntity addressEntity;

    public StaffEntity()
    {
    }

    public StaffEntity(Short staffId)
    {
        this.staffId = staffId;
    }

    public StaffEntity(Short staffId, String firstName, String lastName, boolean active, String username, Date lastUpdate)
    {
        this.staffId = staffId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.active = active;
        this.username = username;
        this.lastUpdate = lastUpdate;
    }

    public Short getStaffId()
    {
        return staffId;
    }

    public void setStaffId(Short staffId)
    {
        this.staffId = staffId;
    }

    public String getFirstName()
    {
        return firstName;
    }

    public void setFirstName(String firstName)
    {
        this.firstName = firstName;
    }

    public String getLastName()
    {
        return lastName;
    }

    public void setLastName(String lastName)
    {
        this.lastName = lastName;
    }

    public byte[] getPicture()
    {
        return picture;
    }

    public void setPicture(byte[] picture)
    {
        this.picture = picture;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public boolean getActive()
    {
        return active;
    }

    public void setActive(boolean active)
    {
        this.active = active;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public Date getLastUpdate()
    {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate)
    {
        this.lastUpdate = lastUpdate;
    }

    public StoreEntity getStoreEntity()
    {
        return storeEntity;
    }

    public void setStoreEntity(StoreEntity storeEntity)
    {
        this.storeEntity = storeEntity;
    }

    public Collection<PaymentEntity> getPaymentEntityCollection()
    {
        return paymentEntityCollection;
    }

    public void setPaymentEntityCollection(Collection<PaymentEntity> paymentEntityCollection)
    {
        this.paymentEntityCollection = paymentEntityCollection;
    }

    public Collection<RentalEntity> getRentalEntityCollection()
    {
        return rentalEntityCollection;
    }

    public void setRentalEntityCollection(Collection<RentalEntity> rentalEntityCollection)
    {
        this.rentalEntityCollection = rentalEntityCollection;
    }

    public StoreEntity getStoreEntity1()
    {
        return storeEntity1;
    }

    public void setStoreEntity1(StoreEntity storeEntity1)
    {
        this.storeEntity1 = storeEntity1;
    }

    public AddressEntity getAddressEntity()
    {
        return addressEntity;
    }

    public void setAddressEntity(AddressEntity addressEntity)
    {
        this.addressEntity = addressEntity;
    }

    @Override
    public int hashCode()
    {
        int hash = 0;
        hash += (staffId != null ? staffId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object)
    {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof StaffEntity))
        {
            return false;
        }
        StaffEntity other = (StaffEntity) object;
        if ((this.staffId == null && other.staffId != null) || (this.staffId != null && !this.staffId.equals(other.staffId)))
        {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return "ar.edu.utnfc.argprog.data.entities.StaffEntity[ staffId=" + staffId + " ]";
    }
    
}

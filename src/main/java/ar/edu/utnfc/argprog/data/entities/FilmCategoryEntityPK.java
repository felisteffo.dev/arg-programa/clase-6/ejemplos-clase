/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ar.edu.utnfc.argprog.data.entities;

import java.io.Serializable;
import jakarta.persistence.Basic;
import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;

/**
 *
 * @author Felipe
 */
@Embeddable
public class FilmCategoryEntityPK implements Serializable
{
    @Basic(optional = false)
    @Column(name = "film_id")
    private short filmId;
    @Basic(optional = false)
    @Column(name = "category_id")
    private short categoryId;

    public FilmCategoryEntityPK()
    {
    }

    public FilmCategoryEntityPK(short filmId, short categoryId)
    {
        this.filmId = filmId;
        this.categoryId = categoryId;
    }

    public short getFilmId()
    {
        return filmId;
    }

    public void setFilmId(short filmId)
    {
        this.filmId = filmId;
    }

    public short getCategoryId()
    {
        return categoryId;
    }

    public void setCategoryId(short categoryId)
    {
        this.categoryId = categoryId;
    }

    @Override
    public int hashCode()
    {
        int hash = 0;
        hash += (int) filmId;
        hash += (int) categoryId;
        return hash;
    }

    @Override
    public boolean equals(Object object)
    {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FilmCategoryEntityPK))
        {
            return false;
        }
        FilmCategoryEntityPK other = (FilmCategoryEntityPK) object;
        if (this.filmId != other.filmId)
        {
            return false;
        }
        if (this.categoryId != other.categoryId)
        {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return "ar.edu.utnfc.argprog.data.entities.FilmCategoryEntityPK[ filmId=" + filmId + ", categoryId=" + categoryId + " ]";
    }
    
}

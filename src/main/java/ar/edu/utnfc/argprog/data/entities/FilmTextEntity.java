/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ar.edu.utnfc.argprog.data.entities;

import java.io.Serializable;
import jakarta.persistence.Basic;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Lob;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.Table;

/**
 *
 * @author Felipe
 */
@Entity
@Table(name = "film_text")
@NamedQueries(
{
    @NamedQuery(name = "FilmTextEntity.findAll", query = "SELECT f FROM FilmTextEntity f"),
    @NamedQuery(name = "FilmTextEntity.findByFilmId", query = "SELECT f FROM FilmTextEntity f WHERE f.filmId = :filmId"),
    @NamedQuery(name = "FilmTextEntity.findByTitle", query = "SELECT f FROM FilmTextEntity f WHERE f.title = :title")
})
public class FilmTextEntity implements Serializable
{
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "film_id")
    private Short filmId;
    @Basic(optional = false)
    private String title;
    @Lob
    private String description;

    public FilmTextEntity()
    {
    }

    public FilmTextEntity(Short filmId)
    {
        this.filmId = filmId;
    }

    public FilmTextEntity(Short filmId, String title)
    {
        this.filmId = filmId;
        this.title = title;
    }

    public Short getFilmId()
    {
        return filmId;
    }

    public void setFilmId(Short filmId)
    {
        this.filmId = filmId;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    @Override
    public int hashCode()
    {
        int hash = 0;
        hash += (filmId != null ? filmId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object)
    {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FilmTextEntity))
        {
            return false;
        }
        FilmTextEntity other = (FilmTextEntity) object;
        if ((this.filmId == null && other.filmId != null) || (this.filmId != null && !this.filmId.equals(other.filmId)))
        {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return "ar.edu.utnfc.argprog.data.entities.FilmTextEntity[ filmId=" + filmId + " ]";
    }
    
}

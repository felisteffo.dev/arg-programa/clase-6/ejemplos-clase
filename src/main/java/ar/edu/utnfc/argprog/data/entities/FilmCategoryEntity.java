/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ar.edu.utnfc.argprog.data.entities;

import java.io.Serializable;
import java.util.Date;
import jakarta.persistence.Basic;
import jakarta.persistence.Column;
import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;

/**
 *
 * @author Felipe
 */
@Entity
@Table(name = "film_category")
@NamedQueries(
{
    @NamedQuery(name = "FilmCategoryEntity.findAll", query = "SELECT f FROM FilmCategoryEntity f"),
    @NamedQuery(name = "FilmCategoryEntity.findByFilmId", query = "SELECT f FROM FilmCategoryEntity f WHERE f.filmCategoryEntityPK.filmId = :filmId"),
    @NamedQuery(name = "FilmCategoryEntity.findByCategoryId", query = "SELECT f FROM FilmCategoryEntity f WHERE f.filmCategoryEntityPK.categoryId = :categoryId"),
    @NamedQuery(name = "FilmCategoryEntity.findByLastUpdate", query = "SELECT f FROM FilmCategoryEntity f WHERE f.lastUpdate = :lastUpdate")
})
public class FilmCategoryEntity implements Serializable
{
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected FilmCategoryEntityPK filmCategoryEntityPK;
    @Basic(optional = false)
    @Column(name = "last_update")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastUpdate;
    @JoinColumn(name = "film_id", referencedColumnName = "film_id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private FilmEntity filmEntity;
    @JoinColumn(name = "category_id", referencedColumnName = "category_id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private CategoryEntity categoryEntity;

    public FilmCategoryEntity()
    {
    }

    public FilmCategoryEntity(FilmCategoryEntityPK filmCategoryEntityPK)
    {
        this.filmCategoryEntityPK = filmCategoryEntityPK;
    }

    public FilmCategoryEntity(FilmCategoryEntityPK filmCategoryEntityPK, Date lastUpdate)
    {
        this.filmCategoryEntityPK = filmCategoryEntityPK;
        this.lastUpdate = lastUpdate;
    }

    public FilmCategoryEntity(short filmId, short categoryId)
    {
        this.filmCategoryEntityPK = new FilmCategoryEntityPK(filmId, categoryId);
    }

    public FilmCategoryEntityPK getFilmCategoryEntityPK()
    {
        return filmCategoryEntityPK;
    }

    public void setFilmCategoryEntityPK(FilmCategoryEntityPK filmCategoryEntityPK)
    {
        this.filmCategoryEntityPK = filmCategoryEntityPK;
    }

    public Date getLastUpdate()
    {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate)
    {
        this.lastUpdate = lastUpdate;
    }

    public FilmEntity getFilmEntity()
    {
        return filmEntity;
    }

    public void setFilmEntity(FilmEntity filmEntity)
    {
        this.filmEntity = filmEntity;
    }

    public CategoryEntity getCategoryEntity()
    {
        return categoryEntity;
    }

    public void setCategoryEntity(CategoryEntity categoryEntity)
    {
        this.categoryEntity = categoryEntity;
    }

    @Override
    public int hashCode()
    {
        int hash = 0;
        hash += (filmCategoryEntityPK != null ? filmCategoryEntityPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object)
    {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FilmCategoryEntity))
        {
            return false;
        }
        FilmCategoryEntity other = (FilmCategoryEntity) object;
        if ((this.filmCategoryEntityPK == null && other.filmCategoryEntityPK != null) || (this.filmCategoryEntityPK != null && !this.filmCategoryEntityPK.equals(other.filmCategoryEntityPK)))
        {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return "ar.edu.utnfc.argprog.data.entities.FilmCategoryEntity[ filmCategoryEntityPK=" + filmCategoryEntityPK + " ]";
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ar.edu.utnfc.argprog;

import ar.edu.utnfc.argprog.data.entities.CustomerEntity;
import ar.edu.utnfc.argprog.data.entities.FilmEntity;
import ar.edu.utnfc.argprog.data.entities.LanguageEntity;

import java.util.List;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;

/**
 * @author Felipe
 */
public class PruebaJpql {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //Para crear obtener el EntityManager en un entorno no administrado
        //usamos el EntityManagerFactory que podemos obtener de la clase
        //Persistence
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("DemoJPAPU");

        //Dicha clase me permite fabricar el EntityManager
        EntityManager em = emf.createEntityManager();

        // A partir del EntityManager puedo obtener y administrar entidades
        // Obtener colecciones de instancias
        System.out.println("Lista de Clientes con una consulta JPQL construida inline");
        em.createQuery("Select c From CustomerEntity c Where c.addressEntity.cityEntity.countryEntity.country Like :pais")
                .setParameter("pais", "Argentina")
                .getResultList()
                .stream()
                .forEach(System.out::println);


        // Puedo ejecutar en base a una consulta SQL Nativa
        System.out.println("\nCon una consulta SQL Navtiva");
        em.createNativeQuery("Select * From language", LanguageEntity.class)
                .getResultList()
                .stream()
                .forEach(System.out::println);


        // Puedo ejecutar en base a una consulta JPQL configurada en la entidad
        System.out.println("\nCon una consulta nombrada con parámetros específicos");
        em.createNamedQuery("CustomerEntity.findByCountryNameNative")
                .setParameter(1, "Argentina")
                .getResultList()
                .stream()
                .forEach(System.out::println);


        em.close();

    }

}

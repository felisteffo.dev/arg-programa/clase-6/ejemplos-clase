/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ar.edu.utnfc.argprog.data.entities;

import java.io.Serializable;
import java.util.Date;
import jakarta.persistence.Basic;
import jakarta.persistence.Column;
import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;

/**
 *
 * @author Felipe
 */
@Entity
@Table(name = "film_actor")
@NamedQueries(
{
    @NamedQuery(name = "FilmActorEntity.findAll", query = "SELECT f FROM FilmActorEntity f"),
    @NamedQuery(name = "FilmActorEntity.findByActorId", query = "SELECT f FROM FilmActorEntity f WHERE f.filmActorEntityPK.actorId = :actorId"),
    @NamedQuery(name = "FilmActorEntity.findByFilmId", query = "SELECT f FROM FilmActorEntity f WHERE f.filmActorEntityPK.filmId = :filmId"),
    @NamedQuery(name = "FilmActorEntity.findByLastUpdate", query = "SELECT f FROM FilmActorEntity f WHERE f.lastUpdate = :lastUpdate")
})
public class FilmActorEntity implements Serializable
{
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected FilmActorEntityPK filmActorEntityPK;
    @Basic(optional = false)
    @Column(name = "last_update")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastUpdate;
    @JoinColumn(name = "film_id", referencedColumnName = "film_id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private FilmEntity filmEntity;
    @JoinColumn(name = "actor_id", referencedColumnName = "actor_id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private ActorEntity actorEntity;

    public FilmActorEntity()
    {
    }

    public FilmActorEntity(FilmActorEntityPK filmActorEntityPK)
    {
        this.filmActorEntityPK = filmActorEntityPK;
    }

    public FilmActorEntity(FilmActorEntityPK filmActorEntityPK, Date lastUpdate)
    {
        this.filmActorEntityPK = filmActorEntityPK;
        this.lastUpdate = lastUpdate;
    }

    public FilmActorEntity(short actorId, short filmId)
    {
        this.filmActorEntityPK = new FilmActorEntityPK(actorId, filmId);
    }

    public FilmActorEntityPK getFilmActorEntityPK()
    {
        return filmActorEntityPK;
    }

    public void setFilmActorEntityPK(FilmActorEntityPK filmActorEntityPK)
    {
        this.filmActorEntityPK = filmActorEntityPK;
    }

    public Date getLastUpdate()
    {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate)
    {
        this.lastUpdate = lastUpdate;
    }

    public FilmEntity getFilmEntity()
    {
        return filmEntity;
    }

    public void setFilmEntity(FilmEntity filmEntity)
    {
        this.filmEntity = filmEntity;
    }

    public ActorEntity getActorEntity()
    {
        return actorEntity;
    }

    public void setActorEntity(ActorEntity actorEntity)
    {
        this.actorEntity = actorEntity;
    }

    @Override
    public int hashCode()
    {
        int hash = 0;
        hash += (filmActorEntityPK != null ? filmActorEntityPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object)
    {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FilmActorEntity))
        {
            return false;
        }
        FilmActorEntity other = (FilmActorEntity) object;
        if ((this.filmActorEntityPK == null && other.filmActorEntityPK != null) || (this.filmActorEntityPK != null && !this.filmActorEntityPK.equals(other.filmActorEntityPK)))
        {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return "ar.edu.utnfc.argprog.data.entities.FilmActorEntity[ filmActorEntityPK=" + filmActorEntityPK + " ]";
    }
    
}

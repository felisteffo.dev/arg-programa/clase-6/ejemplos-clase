/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ar.edu.utnfc.argprog.data.daos;

import ar.edu.utnfc.argprog.data.daos.exceptions.IllegalOrphanException;
import ar.edu.utnfc.argprog.data.daos.exceptions.NonexistentEntityException;
import java.io.Serializable;
import jakarta.persistence.Query;
import jakarta.persistence.EntityNotFoundException;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Root;
import ar.edu.utnfc.argprog.data.entities.CountryEntity;
import ar.edu.utnfc.argprog.data.entities.AddressEntity;
import ar.edu.utnfc.argprog.data.entities.CityEntity;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;

/**
 *
 * @author Felipe
 */
public class CityDao implements Serializable
{

    public CityDao(EntityManagerFactory emf)
    {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager()
    {
        return emf.createEntityManager();
    }

    public void create(CityEntity cityEntity)
    {
        if (cityEntity.getAddressEntityCollection() == null)
        {
            cityEntity.setAddressEntityCollection(new ArrayList<AddressEntity>());
        }
        EntityManager em = null;
        try
        {
            em = getEntityManager();
            em.getTransaction().begin();
            CountryEntity countryEntity = cityEntity.getCountryEntity();
            if (countryEntity != null)
            {
                countryEntity = em.getReference(countryEntity.getClass(), countryEntity.getCountryId());
                cityEntity.setCountryEntity(countryEntity);
            }
            Collection<AddressEntity> attachedAddressEntityCollection = new ArrayList<AddressEntity>();
            for (AddressEntity addressEntityCollectionAddressEntityToAttach : cityEntity.getAddressEntityCollection())
            {
                addressEntityCollectionAddressEntityToAttach = em.getReference(addressEntityCollectionAddressEntityToAttach.getClass(), addressEntityCollectionAddressEntityToAttach.getAddressId());
                attachedAddressEntityCollection.add(addressEntityCollectionAddressEntityToAttach);
            }
            cityEntity.setAddressEntityCollection(attachedAddressEntityCollection);
            em.persist(cityEntity);
            if (countryEntity != null)
            {
                countryEntity.getCityEntityCollection().add(cityEntity);
                countryEntity = em.merge(countryEntity);
            }
            for (AddressEntity addressEntityCollectionAddressEntity : cityEntity.getAddressEntityCollection())
            {
                CityEntity oldCityEntityOfAddressEntityCollectionAddressEntity = addressEntityCollectionAddressEntity.getCityEntity();
                addressEntityCollectionAddressEntity.setCityEntity(cityEntity);
                addressEntityCollectionAddressEntity = em.merge(addressEntityCollectionAddressEntity);
                if (oldCityEntityOfAddressEntityCollectionAddressEntity != null)
                {
                    oldCityEntityOfAddressEntityCollectionAddressEntity.getAddressEntityCollection().remove(addressEntityCollectionAddressEntity);
                    oldCityEntityOfAddressEntityCollectionAddressEntity = em.merge(oldCityEntityOfAddressEntityCollectionAddressEntity);
                }
            }
            em.getTransaction().commit();
        }
        finally
        {
            if (em != null)
            {
                em.close();
            }
        }
    }

    public void edit(CityEntity cityEntity) throws IllegalOrphanException, NonexistentEntityException, Exception
    {
        EntityManager em = null;
        try
        {
            em = getEntityManager();
            em.getTransaction().begin();
            CityEntity persistentCityEntity = em.find(CityEntity.class, cityEntity.getCityId());
            CountryEntity countryEntityOld = persistentCityEntity.getCountryEntity();
            CountryEntity countryEntityNew = cityEntity.getCountryEntity();
            Collection<AddressEntity> addressEntityCollectionOld = persistentCityEntity.getAddressEntityCollection();
            Collection<AddressEntity> addressEntityCollectionNew = cityEntity.getAddressEntityCollection();
            List<String> illegalOrphanMessages = null;
            for (AddressEntity addressEntityCollectionOldAddressEntity : addressEntityCollectionOld)
            {
                if (!addressEntityCollectionNew.contains(addressEntityCollectionOldAddressEntity))
                {
                    if (illegalOrphanMessages == null)
                    {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain AddressEntity " + addressEntityCollectionOldAddressEntity + " since its cityEntity field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null)
            {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (countryEntityNew != null)
            {
                countryEntityNew = em.getReference(countryEntityNew.getClass(), countryEntityNew.getCountryId());
                cityEntity.setCountryEntity(countryEntityNew);
            }
            Collection<AddressEntity> attachedAddressEntityCollectionNew = new ArrayList<AddressEntity>();
            for (AddressEntity addressEntityCollectionNewAddressEntityToAttach : addressEntityCollectionNew)
            {
                addressEntityCollectionNewAddressEntityToAttach = em.getReference(addressEntityCollectionNewAddressEntityToAttach.getClass(), addressEntityCollectionNewAddressEntityToAttach.getAddressId());
                attachedAddressEntityCollectionNew.add(addressEntityCollectionNewAddressEntityToAttach);
            }
            addressEntityCollectionNew = attachedAddressEntityCollectionNew;
            cityEntity.setAddressEntityCollection(addressEntityCollectionNew);
            cityEntity = em.merge(cityEntity);
            if (countryEntityOld != null && !countryEntityOld.equals(countryEntityNew))
            {
                countryEntityOld.getCityEntityCollection().remove(cityEntity);
                countryEntityOld = em.merge(countryEntityOld);
            }
            if (countryEntityNew != null && !countryEntityNew.equals(countryEntityOld))
            {
                countryEntityNew.getCityEntityCollection().add(cityEntity);
                countryEntityNew = em.merge(countryEntityNew);
            }
            for (AddressEntity addressEntityCollectionNewAddressEntity : addressEntityCollectionNew)
            {
                if (!addressEntityCollectionOld.contains(addressEntityCollectionNewAddressEntity))
                {
                    CityEntity oldCityEntityOfAddressEntityCollectionNewAddressEntity = addressEntityCollectionNewAddressEntity.getCityEntity();
                    addressEntityCollectionNewAddressEntity.setCityEntity(cityEntity);
                    addressEntityCollectionNewAddressEntity = em.merge(addressEntityCollectionNewAddressEntity);
                    if (oldCityEntityOfAddressEntityCollectionNewAddressEntity != null && !oldCityEntityOfAddressEntityCollectionNewAddressEntity.equals(cityEntity))
                    {
                        oldCityEntityOfAddressEntityCollectionNewAddressEntity.getAddressEntityCollection().remove(addressEntityCollectionNewAddressEntity);
                        oldCityEntityOfAddressEntityCollectionNewAddressEntity = em.merge(oldCityEntityOfAddressEntityCollectionNewAddressEntity);
                    }
                }
            }
            em.getTransaction().commit();
        }
        catch (Exception ex)
        {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0)
            {
                Short id = cityEntity.getCityId();
                if (findCityEntity(id) == null)
                {
                    throw new NonexistentEntityException("The cityEntity with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
        finally
        {
            if (em != null)
            {
                em.close();
            }
        }
    }

    public void destroy(Short id) throws IllegalOrphanException, NonexistentEntityException
    {
        EntityManager em = null;
        try
        {
            em = getEntityManager();
            em.getTransaction().begin();
            CityEntity cityEntity;
            try
            {
                cityEntity = em.getReference(CityEntity.class, id);
                cityEntity.getCityId();
            }
            catch (EntityNotFoundException enfe)
            {
                throw new NonexistentEntityException("The cityEntity with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<AddressEntity> addressEntityCollectionOrphanCheck = cityEntity.getAddressEntityCollection();
            for (AddressEntity addressEntityCollectionOrphanCheckAddressEntity : addressEntityCollectionOrphanCheck)
            {
                if (illegalOrphanMessages == null)
                {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This CityEntity (" + cityEntity + ") cannot be destroyed since the AddressEntity " + addressEntityCollectionOrphanCheckAddressEntity + " in its addressEntityCollection field has a non-nullable cityEntity field.");
            }
            if (illegalOrphanMessages != null)
            {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            CountryEntity countryEntity = cityEntity.getCountryEntity();
            if (countryEntity != null)
            {
                countryEntity.getCityEntityCollection().remove(cityEntity);
                countryEntity = em.merge(countryEntity);
            }
            em.remove(cityEntity);
            em.getTransaction().commit();
        }
        finally
        {
            if (em != null)
            {
                em.close();
            }
        }
    }

    public List<CityEntity> findCityEntityEntities()
    {
        return findCityEntityEntities(true, -1, -1);
    }

    public List<CityEntity> findCityEntityEntities(int maxResults, int firstResult)
    {
        return findCityEntityEntities(false, maxResults, firstResult);
    }

    private List<CityEntity> findCityEntityEntities(boolean all, int maxResults, int firstResult)
    {
        EntityManager em = getEntityManager();
        try
        {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(CityEntity.class));
            Query q = em.createQuery(cq);
            if (!all)
            {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        }
        finally
        {
            em.close();
        }
    }

    public CityEntity findCityEntity(Short id)
    {
        EntityManager em = getEntityManager();
        try
        {
            return em.find(CityEntity.class, id);
        }
        finally
        {
            em.close();
        }
    }

    public int getCityEntityCount()
    {
        EntityManager em = getEntityManager();
        try
        {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<CityEntity> rt = cq.from(CityEntity.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        }
        finally
        {
            em.close();
        }
    }
    
}

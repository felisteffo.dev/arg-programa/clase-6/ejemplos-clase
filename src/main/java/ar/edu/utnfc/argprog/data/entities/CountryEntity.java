/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ar.edu.utnfc.argprog.data.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import jakarta.persistence.Basic;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;

/**
 *
 * @author Felipe
 */
@Entity
@Table(name = "country")
@NamedQueries(
{
    @NamedQuery(name = "CountryEntity.findAll", query = "SELECT c FROM CountryEntity c"),
    @NamedQuery(name = "CountryEntity.findByCountryId", query = "SELECT c FROM CountryEntity c WHERE c.countryId = :countryId"),
    @NamedQuery(name = "CountryEntity.findByCountry", query = "SELECT c FROM CountryEntity c WHERE c.country = :country"),
    @NamedQuery(name = "CountryEntity.findByLastUpdate", query = "SELECT c FROM CountryEntity c WHERE c.lastUpdate = :lastUpdate")
})
public class CountryEntity implements Serializable
{
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "country_id")
    private Short countryId;
    @Basic(optional = false)
    private String country;
    @Basic(optional = false)
    @Column(name = "last_update")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastUpdate;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "countryEntity")
    private Collection<CityEntity> cityEntityCollection;

    public CountryEntity()
    {
    }

    public CountryEntity(Short countryId)
    {
        this.countryId = countryId;
    }

    public CountryEntity(Short countryId, String country, Date lastUpdate)
    {
        this.countryId = countryId;
        this.country = country;
        this.lastUpdate = lastUpdate;
    }

    public Short getCountryId()
    {
        return countryId;
    }

    public void setCountryId(Short countryId)
    {
        this.countryId = countryId;
    }

    public String getCountry()
    {
        return country;
    }

    public void setCountry(String country)
    {
        this.country = country;
    }

    public Date getLastUpdate()
    {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate)
    {
        this.lastUpdate = lastUpdate;
    }

    public Collection<CityEntity> getCityEntityCollection()
    {
        return cityEntityCollection;
    }

    public void setCityEntityCollection(Collection<CityEntity> cityEntityCollection)
    {
        this.cityEntityCollection = cityEntityCollection;
    }

    @Override
    public int hashCode()
    {
        int hash = 0;
        hash += (countryId != null ? countryId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object)
    {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CountryEntity))
        {
            return false;
        }
        CountryEntity other = (CountryEntity) object;
        if ((this.countryId == null && other.countryId != null) || (this.countryId != null && !this.countryId.equals(other.countryId)))
        {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return "ar.edu.utnfc.argprog.data.entities.CountryEntity[ countryId=" + countryId + " ]";
    }
    
}

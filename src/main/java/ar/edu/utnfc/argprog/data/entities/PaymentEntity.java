/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ar.edu.utnfc.argprog.data.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import jakarta.persistence.Basic;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;

/**
 *
 * @author Felipe
 */
@Entity
@Table(name = "payment")
@NamedQueries(
{
    @NamedQuery(name = "PaymentEntity.findAll", query = "SELECT p FROM PaymentEntity p"),
    @NamedQuery(name = "PaymentEntity.findByPaymentId", query = "SELECT p FROM PaymentEntity p WHERE p.paymentId = :paymentId"),
    @NamedQuery(name = "PaymentEntity.findByAmount", query = "SELECT p FROM PaymentEntity p WHERE p.amount = :amount"),
    @NamedQuery(name = "PaymentEntity.findByPaymentDate", query = "SELECT p FROM PaymentEntity p WHERE p.paymentDate = :paymentDate"),
    @NamedQuery(name = "PaymentEntity.findByLastUpdate", query = "SELECT p FROM PaymentEntity p WHERE p.lastUpdate = :lastUpdate")
})
public class PaymentEntity implements Serializable
{
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "payment_id")
    private Short paymentId;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    private BigDecimal amount;
    @Basic(optional = false)
    @Column(name = "payment_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date paymentDate;
    @Basic(optional = false)
    @Column(name = "last_update")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastUpdate;
    @JoinColumn(name = "staff_id", referencedColumnName = "staff_id")
    @ManyToOne(optional = false)
    private StaffEntity staffEntity;
    @JoinColumn(name = "rental_id", referencedColumnName = "rental_id")
    @ManyToOne
    private RentalEntity rentalEntity;
    @JoinColumn(name = "customer_id", referencedColumnName = "customer_id")
    @ManyToOne(optional = false)
    private CustomerEntity customerEntity;

    public PaymentEntity()
    {
    }

    public PaymentEntity(Short paymentId)
    {
        this.paymentId = paymentId;
    }

    public PaymentEntity(Short paymentId, BigDecimal amount, Date paymentDate, Date lastUpdate)
    {
        this.paymentId = paymentId;
        this.amount = amount;
        this.paymentDate = paymentDate;
        this.lastUpdate = lastUpdate;
    }

    public Short getPaymentId()
    {
        return paymentId;
    }

    public void setPaymentId(Short paymentId)
    {
        this.paymentId = paymentId;
    }

    public BigDecimal getAmount()
    {
        return amount;
    }

    public void setAmount(BigDecimal amount)
    {
        this.amount = amount;
    }

    public Date getPaymentDate()
    {
        return paymentDate;
    }

    public void setPaymentDate(Date paymentDate)
    {
        this.paymentDate = paymentDate;
    }

    public Date getLastUpdate()
    {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate)
    {
        this.lastUpdate = lastUpdate;
    }

    public StaffEntity getStaffEntity()
    {
        return staffEntity;
    }

    public void setStaffEntity(StaffEntity staffEntity)
    {
        this.staffEntity = staffEntity;
    }

    public RentalEntity getRentalEntity()
    {
        return rentalEntity;
    }

    public void setRentalEntity(RentalEntity rentalEntity)
    {
        this.rentalEntity = rentalEntity;
    }

    public CustomerEntity getCustomerEntity()
    {
        return customerEntity;
    }

    public void setCustomerEntity(CustomerEntity customerEntity)
    {
        this.customerEntity = customerEntity;
    }

    @Override
    public int hashCode()
    {
        int hash = 0;
        hash += (paymentId != null ? paymentId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object)
    {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PaymentEntity))
        {
            return false;
        }
        PaymentEntity other = (PaymentEntity) object;
        if ((this.paymentId == null && other.paymentId != null) || (this.paymentId != null && !this.paymentId.equals(other.paymentId)))
        {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return "ar.edu.utnfc.argprog.data.entities.PaymentEntity[ paymentId=" + paymentId + " ]";
    }
    
}

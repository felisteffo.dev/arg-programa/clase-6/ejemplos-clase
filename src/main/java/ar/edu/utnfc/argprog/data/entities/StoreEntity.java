/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ar.edu.utnfc.argprog.data.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import jakarta.persistence.Basic;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;

/**
 *
 * @author Felipe
 */
@Entity
@Table(name = "store")
@NamedQueries(
{
    @NamedQuery(name = "StoreEntity.findAll", query = "SELECT s FROM StoreEntity s"),
    @NamedQuery(name = "StoreEntity.findByStoreId", query = "SELECT s FROM StoreEntity s WHERE s.storeId = :storeId"),
    @NamedQuery(name = "StoreEntity.findByLastUpdate", query = "SELECT s FROM StoreEntity s WHERE s.lastUpdate = :lastUpdate")
})
public class StoreEntity implements Serializable
{
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "store_id")
    private Short storeId;
    @Basic(optional = false)
    @Column(name = "last_update")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastUpdate;
    @JoinColumn(name = "manager_staff_id", referencedColumnName = "staff_id")
    @OneToOne(optional = false)
    private StaffEntity staffEntity;
    @JoinColumn(name = "address_id", referencedColumnName = "address_id")
    @ManyToOne(optional = false)
    private AddressEntity addressEntity;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "storeEntity")
    private Collection<CustomerEntity> customerEntityCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "storeEntity1")
    private Collection<StaffEntity> staffEntityCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "storeEntity")
    private Collection<InventoryEntity> inventoryEntityCollection;

    public StoreEntity()
    {
    }

    public StoreEntity(Short storeId)
    {
        this.storeId = storeId;
    }

    public StoreEntity(Short storeId, Date lastUpdate)
    {
        this.storeId = storeId;
        this.lastUpdate = lastUpdate;
    }

    public Short getStoreId()
    {
        return storeId;
    }

    public void setStoreId(Short storeId)
    {
        this.storeId = storeId;
    }

    public Date getLastUpdate()
    {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate)
    {
        this.lastUpdate = lastUpdate;
    }

    public StaffEntity getStaffEntity()
    {
        return staffEntity;
    }

    public void setStaffEntity(StaffEntity staffEntity)
    {
        this.staffEntity = staffEntity;
    }

    public AddressEntity getAddressEntity()
    {
        return addressEntity;
    }

    public void setAddressEntity(AddressEntity addressEntity)
    {
        this.addressEntity = addressEntity;
    }

    public Collection<CustomerEntity> getCustomerEntityCollection()
    {
        return customerEntityCollection;
    }

    public void setCustomerEntityCollection(Collection<CustomerEntity> customerEntityCollection)
    {
        this.customerEntityCollection = customerEntityCollection;
    }

    public Collection<StaffEntity> getStaffEntityCollection()
    {
        return staffEntityCollection;
    }

    public void setStaffEntityCollection(Collection<StaffEntity> staffEntityCollection)
    {
        this.staffEntityCollection = staffEntityCollection;
    }

    public Collection<InventoryEntity> getInventoryEntityCollection()
    {
        return inventoryEntityCollection;
    }

    public void setInventoryEntityCollection(Collection<InventoryEntity> inventoryEntityCollection)
    {
        this.inventoryEntityCollection = inventoryEntityCollection;
    }

    @Override
    public int hashCode()
    {
        int hash = 0;
        hash += (storeId != null ? storeId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object)
    {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof StoreEntity))
        {
            return false;
        }
        StoreEntity other = (StoreEntity) object;
        if ((this.storeId == null && other.storeId != null) || (this.storeId != null && !this.storeId.equals(other.storeId)))
        {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return "ar.edu.utnfc.argprog.data.entities.StoreEntity[ storeId=" + storeId + " ]";
    }
    
}

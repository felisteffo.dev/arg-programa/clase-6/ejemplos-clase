/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ar.edu.utnfc.argprog.data.daos;

import ar.edu.utnfc.argprog.data.daos.exceptions.IllegalOrphanException;
import ar.edu.utnfc.argprog.data.daos.exceptions.NonexistentEntityException;
import java.io.Serializable;
import jakarta.persistence.Query;
import jakarta.persistence.EntityNotFoundException;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Root;
import ar.edu.utnfc.argprog.data.entities.StoreEntity;
import ar.edu.utnfc.argprog.data.entities.AddressEntity;
import ar.edu.utnfc.argprog.data.entities.PaymentEntity;
import java.util.ArrayList;
import java.util.Collection;
import ar.edu.utnfc.argprog.data.entities.RentalEntity;
import ar.edu.utnfc.argprog.data.entities.StaffEntity;
import java.util.List;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;

/**
 *
 * @author Felipe
 */
public class StaffDao implements Serializable
{

    public StaffDao(EntityManagerFactory emf)
    {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager()
    {
        return emf.createEntityManager();
    }

    public void create(StaffEntity staffEntity) throws IllegalOrphanException
    {
        if (staffEntity.getPaymentEntityCollection() == null)
        {
            staffEntity.setPaymentEntityCollection(new ArrayList<PaymentEntity>());
        }
        if (staffEntity.getRentalEntityCollection() == null)
        {
            staffEntity.setRentalEntityCollection(new ArrayList<RentalEntity>());
        }
        List<String> illegalOrphanMessages = null;
        StoreEntity storeEntity1OrphanCheck = staffEntity.getStoreEntity1();
        if (storeEntity1OrphanCheck != null)
        {
            StaffEntity oldStaffEntityOfStoreEntity1 = storeEntity1OrphanCheck.getStaffEntity();
            if (oldStaffEntityOfStoreEntity1 != null)
            {
                if (illegalOrphanMessages == null)
                {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("The StoreEntity " + storeEntity1OrphanCheck + " already has an item of type StaffEntity whose storeEntity1 column cannot be null. Please make another selection for the storeEntity1 field.");
            }
        }
        if (illegalOrphanMessages != null)
        {
            throw new IllegalOrphanException(illegalOrphanMessages);
        }
        EntityManager em = null;
        try
        {
            em = getEntityManager();
            em.getTransaction().begin();
            StoreEntity storeEntity = staffEntity.getStoreEntity();
            if (storeEntity != null)
            {
                storeEntity = em.getReference(storeEntity.getClass(), storeEntity.getStoreId());
                staffEntity.setStoreEntity(storeEntity);
            }
            AddressEntity addressEntity = staffEntity.getAddressEntity();
            if (addressEntity != null)
            {
                addressEntity = em.getReference(addressEntity.getClass(), addressEntity.getAddressId());
                staffEntity.setAddressEntity(addressEntity);
            }
            StoreEntity storeEntity1 = staffEntity.getStoreEntity1();
            if (storeEntity1 != null)
            {
                storeEntity1 = em.getReference(storeEntity1.getClass(), storeEntity1.getStoreId());
                staffEntity.setStoreEntity1(storeEntity1);
            }
            Collection<PaymentEntity> attachedPaymentEntityCollection = new ArrayList<PaymentEntity>();
            for (PaymentEntity paymentEntityCollectionPaymentEntityToAttach : staffEntity.getPaymentEntityCollection())
            {
                paymentEntityCollectionPaymentEntityToAttach = em.getReference(paymentEntityCollectionPaymentEntityToAttach.getClass(), paymentEntityCollectionPaymentEntityToAttach.getPaymentId());
                attachedPaymentEntityCollection.add(paymentEntityCollectionPaymentEntityToAttach);
            }
            staffEntity.setPaymentEntityCollection(attachedPaymentEntityCollection);
            Collection<RentalEntity> attachedRentalEntityCollection = new ArrayList<RentalEntity>();
            for (RentalEntity rentalEntityCollectionRentalEntityToAttach : staffEntity.getRentalEntityCollection())
            {
                rentalEntityCollectionRentalEntityToAttach = em.getReference(rentalEntityCollectionRentalEntityToAttach.getClass(), rentalEntityCollectionRentalEntityToAttach.getRentalId());
                attachedRentalEntityCollection.add(rentalEntityCollectionRentalEntityToAttach);
            }
            staffEntity.setRentalEntityCollection(attachedRentalEntityCollection);
            em.persist(staffEntity);
            if (storeEntity != null)
            {
                StaffEntity oldStaffEntityOfStoreEntity = storeEntity.getStaffEntity();
                if (oldStaffEntityOfStoreEntity != null)
                {
                    oldStaffEntityOfStoreEntity.setStoreEntity(null);
                    oldStaffEntityOfStoreEntity = em.merge(oldStaffEntityOfStoreEntity);
                }
                storeEntity.setStaffEntity(staffEntity);
                storeEntity = em.merge(storeEntity);
            }
            if (addressEntity != null)
            {
                addressEntity.getStaffEntityCollection().add(staffEntity);
                addressEntity = em.merge(addressEntity);
            }
            if (storeEntity1 != null)
            {
                storeEntity1.setStaffEntity(staffEntity);
                storeEntity1 = em.merge(storeEntity1);
            }
            for (PaymentEntity paymentEntityCollectionPaymentEntity : staffEntity.getPaymentEntityCollection())
            {
                StaffEntity oldStaffEntityOfPaymentEntityCollectionPaymentEntity = paymentEntityCollectionPaymentEntity.getStaffEntity();
                paymentEntityCollectionPaymentEntity.setStaffEntity(staffEntity);
                paymentEntityCollectionPaymentEntity = em.merge(paymentEntityCollectionPaymentEntity);
                if (oldStaffEntityOfPaymentEntityCollectionPaymentEntity != null)
                {
                    oldStaffEntityOfPaymentEntityCollectionPaymentEntity.getPaymentEntityCollection().remove(paymentEntityCollectionPaymentEntity);
                    oldStaffEntityOfPaymentEntityCollectionPaymentEntity = em.merge(oldStaffEntityOfPaymentEntityCollectionPaymentEntity);
                }
            }
            for (RentalEntity rentalEntityCollectionRentalEntity : staffEntity.getRentalEntityCollection())
            {
                StaffEntity oldStaffEntityOfRentalEntityCollectionRentalEntity = rentalEntityCollectionRentalEntity.getStaffEntity();
                rentalEntityCollectionRentalEntity.setStaffEntity(staffEntity);
                rentalEntityCollectionRentalEntity = em.merge(rentalEntityCollectionRentalEntity);
                if (oldStaffEntityOfRentalEntityCollectionRentalEntity != null)
                {
                    oldStaffEntityOfRentalEntityCollectionRentalEntity.getRentalEntityCollection().remove(rentalEntityCollectionRentalEntity);
                    oldStaffEntityOfRentalEntityCollectionRentalEntity = em.merge(oldStaffEntityOfRentalEntityCollectionRentalEntity);
                }
            }
            em.getTransaction().commit();
        }
        finally
        {
            if (em != null)
            {
                em.close();
            }
        }
    }

    public void edit(StaffEntity staffEntity) throws IllegalOrphanException, NonexistentEntityException, Exception
    {
        EntityManager em = null;
        try
        {
            em = getEntityManager();
            em.getTransaction().begin();
            StaffEntity persistentStaffEntity = em.find(StaffEntity.class, staffEntity.getStaffId());
            StoreEntity storeEntityOld = persistentStaffEntity.getStoreEntity();
            StoreEntity storeEntityNew = staffEntity.getStoreEntity();
            AddressEntity addressEntityOld = persistentStaffEntity.getAddressEntity();
            AddressEntity addressEntityNew = staffEntity.getAddressEntity();
            StoreEntity storeEntity1Old = persistentStaffEntity.getStoreEntity1();
            StoreEntity storeEntity1New = staffEntity.getStoreEntity1();
            Collection<PaymentEntity> paymentEntityCollectionOld = persistentStaffEntity.getPaymentEntityCollection();
            Collection<PaymentEntity> paymentEntityCollectionNew = staffEntity.getPaymentEntityCollection();
            Collection<RentalEntity> rentalEntityCollectionOld = persistentStaffEntity.getRentalEntityCollection();
            Collection<RentalEntity> rentalEntityCollectionNew = staffEntity.getRentalEntityCollection();
            List<String> illegalOrphanMessages = null;
            if (storeEntityOld != null && !storeEntityOld.equals(storeEntityNew))
            {
                if (illegalOrphanMessages == null)
                {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("You must retain StoreEntity " + storeEntityOld + " since its staffEntity field is not nullable.");
            }
            if (storeEntity1Old != null && !storeEntity1Old.equals(storeEntity1New))
            {
                if (illegalOrphanMessages == null)
                {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("You must retain StoreEntity " + storeEntity1Old + " since its staffEntity field is not nullable.");
            }
            if (storeEntity1New != null && !storeEntity1New.equals(storeEntity1Old))
            {
                StaffEntity oldStaffEntityOfStoreEntity1 = storeEntity1New.getStaffEntity();
                if (oldStaffEntityOfStoreEntity1 != null)
                {
                    if (illegalOrphanMessages == null)
                    {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("The StoreEntity " + storeEntity1New + " already has an item of type StaffEntity whose storeEntity1 column cannot be null. Please make another selection for the storeEntity1 field.");
                }
            }
            for (PaymentEntity paymentEntityCollectionOldPaymentEntity : paymentEntityCollectionOld)
            {
                if (!paymentEntityCollectionNew.contains(paymentEntityCollectionOldPaymentEntity))
                {
                    if (illegalOrphanMessages == null)
                    {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain PaymentEntity " + paymentEntityCollectionOldPaymentEntity + " since its staffEntity field is not nullable.");
                }
            }
            for (RentalEntity rentalEntityCollectionOldRentalEntity : rentalEntityCollectionOld)
            {
                if (!rentalEntityCollectionNew.contains(rentalEntityCollectionOldRentalEntity))
                {
                    if (illegalOrphanMessages == null)
                    {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain RentalEntity " + rentalEntityCollectionOldRentalEntity + " since its staffEntity field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null)
            {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (storeEntityNew != null)
            {
                storeEntityNew = em.getReference(storeEntityNew.getClass(), storeEntityNew.getStoreId());
                staffEntity.setStoreEntity(storeEntityNew);
            }
            if (addressEntityNew != null)
            {
                addressEntityNew = em.getReference(addressEntityNew.getClass(), addressEntityNew.getAddressId());
                staffEntity.setAddressEntity(addressEntityNew);
            }
            if (storeEntity1New != null)
            {
                storeEntity1New = em.getReference(storeEntity1New.getClass(), storeEntity1New.getStoreId());
                staffEntity.setStoreEntity1(storeEntity1New);
            }
            Collection<PaymentEntity> attachedPaymentEntityCollectionNew = new ArrayList<PaymentEntity>();
            for (PaymentEntity paymentEntityCollectionNewPaymentEntityToAttach : paymentEntityCollectionNew)
            {
                paymentEntityCollectionNewPaymentEntityToAttach = em.getReference(paymentEntityCollectionNewPaymentEntityToAttach.getClass(), paymentEntityCollectionNewPaymentEntityToAttach.getPaymentId());
                attachedPaymentEntityCollectionNew.add(paymentEntityCollectionNewPaymentEntityToAttach);
            }
            paymentEntityCollectionNew = attachedPaymentEntityCollectionNew;
            staffEntity.setPaymentEntityCollection(paymentEntityCollectionNew);
            Collection<RentalEntity> attachedRentalEntityCollectionNew = new ArrayList<RentalEntity>();
            for (RentalEntity rentalEntityCollectionNewRentalEntityToAttach : rentalEntityCollectionNew)
            {
                rentalEntityCollectionNewRentalEntityToAttach = em.getReference(rentalEntityCollectionNewRentalEntityToAttach.getClass(), rentalEntityCollectionNewRentalEntityToAttach.getRentalId());
                attachedRentalEntityCollectionNew.add(rentalEntityCollectionNewRentalEntityToAttach);
            }
            rentalEntityCollectionNew = attachedRentalEntityCollectionNew;
            staffEntity.setRentalEntityCollection(rentalEntityCollectionNew);
            staffEntity = em.merge(staffEntity);
            if (storeEntityNew != null && !storeEntityNew.equals(storeEntityOld))
            {
                StaffEntity oldStaffEntityOfStoreEntity = storeEntityNew.getStaffEntity();
                if (oldStaffEntityOfStoreEntity != null)
                {
                    oldStaffEntityOfStoreEntity.setStoreEntity(null);
                    oldStaffEntityOfStoreEntity = em.merge(oldStaffEntityOfStoreEntity);
                }
                storeEntityNew.setStaffEntity(staffEntity);
                storeEntityNew = em.merge(storeEntityNew);
            }
            if (addressEntityOld != null && !addressEntityOld.equals(addressEntityNew))
            {
                addressEntityOld.getStaffEntityCollection().remove(staffEntity);
                addressEntityOld = em.merge(addressEntityOld);
            }
            if (addressEntityNew != null && !addressEntityNew.equals(addressEntityOld))
            {
                addressEntityNew.getStaffEntityCollection().add(staffEntity);
                addressEntityNew = em.merge(addressEntityNew);
            }
            if (storeEntity1New != null && !storeEntity1New.equals(storeEntity1Old))
            {
                storeEntity1New.setStaffEntity(staffEntity);
                storeEntity1New = em.merge(storeEntity1New);
            }
            for (PaymentEntity paymentEntityCollectionNewPaymentEntity : paymentEntityCollectionNew)
            {
                if (!paymentEntityCollectionOld.contains(paymentEntityCollectionNewPaymentEntity))
                {
                    StaffEntity oldStaffEntityOfPaymentEntityCollectionNewPaymentEntity = paymentEntityCollectionNewPaymentEntity.getStaffEntity();
                    paymentEntityCollectionNewPaymentEntity.setStaffEntity(staffEntity);
                    paymentEntityCollectionNewPaymentEntity = em.merge(paymentEntityCollectionNewPaymentEntity);
                    if (oldStaffEntityOfPaymentEntityCollectionNewPaymentEntity != null && !oldStaffEntityOfPaymentEntityCollectionNewPaymentEntity.equals(staffEntity))
                    {
                        oldStaffEntityOfPaymentEntityCollectionNewPaymentEntity.getPaymentEntityCollection().remove(paymentEntityCollectionNewPaymentEntity);
                        oldStaffEntityOfPaymentEntityCollectionNewPaymentEntity = em.merge(oldStaffEntityOfPaymentEntityCollectionNewPaymentEntity);
                    }
                }
            }
            for (RentalEntity rentalEntityCollectionNewRentalEntity : rentalEntityCollectionNew)
            {
                if (!rentalEntityCollectionOld.contains(rentalEntityCollectionNewRentalEntity))
                {
                    StaffEntity oldStaffEntityOfRentalEntityCollectionNewRentalEntity = rentalEntityCollectionNewRentalEntity.getStaffEntity();
                    rentalEntityCollectionNewRentalEntity.setStaffEntity(staffEntity);
                    rentalEntityCollectionNewRentalEntity = em.merge(rentalEntityCollectionNewRentalEntity);
                    if (oldStaffEntityOfRentalEntityCollectionNewRentalEntity != null && !oldStaffEntityOfRentalEntityCollectionNewRentalEntity.equals(staffEntity))
                    {
                        oldStaffEntityOfRentalEntityCollectionNewRentalEntity.getRentalEntityCollection().remove(rentalEntityCollectionNewRentalEntity);
                        oldStaffEntityOfRentalEntityCollectionNewRentalEntity = em.merge(oldStaffEntityOfRentalEntityCollectionNewRentalEntity);
                    }
                }
            }
            em.getTransaction().commit();
        }
        catch (Exception ex)
        {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0)
            {
                Short id = staffEntity.getStaffId();
                if (findStaffEntity(id) == null)
                {
                    throw new NonexistentEntityException("The staffEntity with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
        finally
        {
            if (em != null)
            {
                em.close();
            }
        }
    }

    public void destroy(Short id) throws IllegalOrphanException, NonexistentEntityException
    {
        EntityManager em = null;
        try
        {
            em = getEntityManager();
            em.getTransaction().begin();
            StaffEntity staffEntity;
            try
            {
                staffEntity = em.getReference(StaffEntity.class, id);
                staffEntity.getStaffId();
            }
            catch (EntityNotFoundException enfe)
            {
                throw new NonexistentEntityException("The staffEntity with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            StoreEntity storeEntityOrphanCheck = staffEntity.getStoreEntity();
            if (storeEntityOrphanCheck != null)
            {
                if (illegalOrphanMessages == null)
                {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This StaffEntity (" + staffEntity + ") cannot be destroyed since the StoreEntity " + storeEntityOrphanCheck + " in its storeEntity field has a non-nullable staffEntity field.");
            }
            StoreEntity storeEntity1OrphanCheck = staffEntity.getStoreEntity1();
            if (storeEntity1OrphanCheck != null)
            {
                if (illegalOrphanMessages == null)
                {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This StaffEntity (" + staffEntity + ") cannot be destroyed since the StoreEntity " + storeEntity1OrphanCheck + " in its storeEntity1 field has a non-nullable staffEntity field.");
            }
            Collection<PaymentEntity> paymentEntityCollectionOrphanCheck = staffEntity.getPaymentEntityCollection();
            for (PaymentEntity paymentEntityCollectionOrphanCheckPaymentEntity : paymentEntityCollectionOrphanCheck)
            {
                if (illegalOrphanMessages == null)
                {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This StaffEntity (" + staffEntity + ") cannot be destroyed since the PaymentEntity " + paymentEntityCollectionOrphanCheckPaymentEntity + " in its paymentEntityCollection field has a non-nullable staffEntity field.");
            }
            Collection<RentalEntity> rentalEntityCollectionOrphanCheck = staffEntity.getRentalEntityCollection();
            for (RentalEntity rentalEntityCollectionOrphanCheckRentalEntity : rentalEntityCollectionOrphanCheck)
            {
                if (illegalOrphanMessages == null)
                {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This StaffEntity (" + staffEntity + ") cannot be destroyed since the RentalEntity " + rentalEntityCollectionOrphanCheckRentalEntity + " in its rentalEntityCollection field has a non-nullable staffEntity field.");
            }
            if (illegalOrphanMessages != null)
            {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            AddressEntity addressEntity = staffEntity.getAddressEntity();
            if (addressEntity != null)
            {
                addressEntity.getStaffEntityCollection().remove(staffEntity);
                addressEntity = em.merge(addressEntity);
            }
            em.remove(staffEntity);
            em.getTransaction().commit();
        }
        finally
        {
            if (em != null)
            {
                em.close();
            }
        }
    }

    public List<StaffEntity> findStaffEntityEntities()
    {
        return findStaffEntityEntities(true, -1, -1);
    }

    public List<StaffEntity> findStaffEntityEntities(int maxResults, int firstResult)
    {
        return findStaffEntityEntities(false, maxResults, firstResult);
    }

    private List<StaffEntity> findStaffEntityEntities(boolean all, int maxResults, int firstResult)
    {
        EntityManager em = getEntityManager();
        try
        {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(StaffEntity.class));
            Query q = em.createQuery(cq);
            if (!all)
            {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        }
        finally
        {
            em.close();
        }
    }

    public StaffEntity findStaffEntity(Short id)
    {
        EntityManager em = getEntityManager();
        try
        {
            return em.find(StaffEntity.class, id);
        }
        finally
        {
            em.close();
        }
    }

    public int getStaffEntityCount()
    {
        EntityManager em = getEntityManager();
        try
        {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<StaffEntity> rt = cq.from(StaffEntity.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        }
        finally
        {
            em.close();
        }
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ar.edu.utnfc.argprog.data.daos;

import ar.edu.utnfc.argprog.data.daos.exceptions.NonexistentEntityException;
import ar.edu.utnfc.argprog.data.daos.exceptions.PreexistingEntityException;
import java.io.Serializable;
import jakarta.persistence.Query;
import jakarta.persistence.EntityNotFoundException;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Root;
import ar.edu.utnfc.argprog.data.entities.CategoryEntity;
import ar.edu.utnfc.argprog.data.entities.FilmCategoryEntity;
import ar.edu.utnfc.argprog.data.entities.FilmCategoryEntityPK;
import ar.edu.utnfc.argprog.data.entities.FilmEntity;
import java.util.List;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;

/**
 *
 * @author Felipe
 */
public class FilmCategoryDao implements Serializable
{

    public FilmCategoryDao(EntityManagerFactory emf)
    {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager()
    {
        return emf.createEntityManager();
    }

    public void create(FilmCategoryEntity filmCategoryEntity) throws PreexistingEntityException, Exception
    {
        if (filmCategoryEntity.getFilmCategoryEntityPK() == null)
        {
            filmCategoryEntity.setFilmCategoryEntityPK(new FilmCategoryEntityPK());
        }
        filmCategoryEntity.getFilmCategoryEntityPK().setCategoryId(filmCategoryEntity.getCategoryEntity().getCategoryId());
        filmCategoryEntity.getFilmCategoryEntityPK().setFilmId(filmCategoryEntity.getFilmEntity().getFilmId());
        EntityManager em = null;
        try
        {
            em = getEntityManager();
            em.getTransaction().begin();
            CategoryEntity categoryEntity = filmCategoryEntity.getCategoryEntity();
            if (categoryEntity != null)
            {
                categoryEntity = em.getReference(categoryEntity.getClass(), categoryEntity.getCategoryId());
                filmCategoryEntity.setCategoryEntity(categoryEntity);
            }
            FilmEntity filmEntity = filmCategoryEntity.getFilmEntity();
            if (filmEntity != null)
            {
                filmEntity = em.getReference(filmEntity.getClass(), filmEntity.getFilmId());
                filmCategoryEntity.setFilmEntity(filmEntity);
            }
            em.persist(filmCategoryEntity);
            if (categoryEntity != null)
            {
                categoryEntity.getFilmCategoryEntityCollection().add(filmCategoryEntity);
                categoryEntity = em.merge(categoryEntity);
            }
            if (filmEntity != null)
            {
                filmEntity.getFilmCategoryEntityCollection().add(filmCategoryEntity);
                filmEntity = em.merge(filmEntity);
            }
            em.getTransaction().commit();
        }
        catch (Exception ex)
        {
            if (findFilmCategoryEntity(filmCategoryEntity.getFilmCategoryEntityPK()) != null)
            {
                throw new PreexistingEntityException("FilmCategoryEntity " + filmCategoryEntity + " already exists.", ex);
            }
            throw ex;
        }
        finally
        {
            if (em != null)
            {
                em.close();
            }
        }
    }

    public void edit(FilmCategoryEntity filmCategoryEntity) throws NonexistentEntityException, Exception
    {
        filmCategoryEntity.getFilmCategoryEntityPK().setCategoryId(filmCategoryEntity.getCategoryEntity().getCategoryId());
        filmCategoryEntity.getFilmCategoryEntityPK().setFilmId(filmCategoryEntity.getFilmEntity().getFilmId());
        EntityManager em = null;
        try
        {
            em = getEntityManager();
            em.getTransaction().begin();
            FilmCategoryEntity persistentFilmCategoryEntity = em.find(FilmCategoryEntity.class, filmCategoryEntity.getFilmCategoryEntityPK());
            CategoryEntity categoryEntityOld = persistentFilmCategoryEntity.getCategoryEntity();
            CategoryEntity categoryEntityNew = filmCategoryEntity.getCategoryEntity();
            FilmEntity filmEntityOld = persistentFilmCategoryEntity.getFilmEntity();
            FilmEntity filmEntityNew = filmCategoryEntity.getFilmEntity();
            if (categoryEntityNew != null)
            {
                categoryEntityNew = em.getReference(categoryEntityNew.getClass(), categoryEntityNew.getCategoryId());
                filmCategoryEntity.setCategoryEntity(categoryEntityNew);
            }
            if (filmEntityNew != null)
            {
                filmEntityNew = em.getReference(filmEntityNew.getClass(), filmEntityNew.getFilmId());
                filmCategoryEntity.setFilmEntity(filmEntityNew);
            }
            filmCategoryEntity = em.merge(filmCategoryEntity);
            if (categoryEntityOld != null && !categoryEntityOld.equals(categoryEntityNew))
            {
                categoryEntityOld.getFilmCategoryEntityCollection().remove(filmCategoryEntity);
                categoryEntityOld = em.merge(categoryEntityOld);
            }
            if (categoryEntityNew != null && !categoryEntityNew.equals(categoryEntityOld))
            {
                categoryEntityNew.getFilmCategoryEntityCollection().add(filmCategoryEntity);
                categoryEntityNew = em.merge(categoryEntityNew);
            }
            if (filmEntityOld != null && !filmEntityOld.equals(filmEntityNew))
            {
                filmEntityOld.getFilmCategoryEntityCollection().remove(filmCategoryEntity);
                filmEntityOld = em.merge(filmEntityOld);
            }
            if (filmEntityNew != null && !filmEntityNew.equals(filmEntityOld))
            {
                filmEntityNew.getFilmCategoryEntityCollection().add(filmCategoryEntity);
                filmEntityNew = em.merge(filmEntityNew);
            }
            em.getTransaction().commit();
        }
        catch (Exception ex)
        {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0)
            {
                FilmCategoryEntityPK id = filmCategoryEntity.getFilmCategoryEntityPK();
                if (findFilmCategoryEntity(id) == null)
                {
                    throw new NonexistentEntityException("The filmCategoryEntity with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
        finally
        {
            if (em != null)
            {
                em.close();
            }
        }
    }

    public void destroy(FilmCategoryEntityPK id) throws NonexistentEntityException
    {
        EntityManager em = null;
        try
        {
            em = getEntityManager();
            em.getTransaction().begin();
            FilmCategoryEntity filmCategoryEntity;
            try
            {
                filmCategoryEntity = em.getReference(FilmCategoryEntity.class, id);
                filmCategoryEntity.getFilmCategoryEntityPK();
            }
            catch (EntityNotFoundException enfe)
            {
                throw new NonexistentEntityException("The filmCategoryEntity with id " + id + " no longer exists.", enfe);
            }
            CategoryEntity categoryEntity = filmCategoryEntity.getCategoryEntity();
            if (categoryEntity != null)
            {
                categoryEntity.getFilmCategoryEntityCollection().remove(filmCategoryEntity);
                categoryEntity = em.merge(categoryEntity);
            }
            FilmEntity filmEntity = filmCategoryEntity.getFilmEntity();
            if (filmEntity != null)
            {
                filmEntity.getFilmCategoryEntityCollection().remove(filmCategoryEntity);
                filmEntity = em.merge(filmEntity);
            }
            em.remove(filmCategoryEntity);
            em.getTransaction().commit();
        }
        finally
        {
            if (em != null)
            {
                em.close();
            }
        }
    }

    public List<FilmCategoryEntity> findFilmCategoryEntityEntities()
    {
        return findFilmCategoryEntityEntities(true, -1, -1);
    }

    public List<FilmCategoryEntity> findFilmCategoryEntityEntities(int maxResults, int firstResult)
    {
        return findFilmCategoryEntityEntities(false, maxResults, firstResult);
    }

    private List<FilmCategoryEntity> findFilmCategoryEntityEntities(boolean all, int maxResults, int firstResult)
    {
        EntityManager em = getEntityManager();
        try
        {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(FilmCategoryEntity.class));
            Query q = em.createQuery(cq);
            if (!all)
            {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        }
        finally
        {
            em.close();
        }
    }

    public FilmCategoryEntity findFilmCategoryEntity(FilmCategoryEntityPK id)
    {
        EntityManager em = getEntityManager();
        try
        {
            return em.find(FilmCategoryEntity.class, id);
        }
        finally
        {
            em.close();
        }
    }

    public int getFilmCategoryEntityCount()
    {
        EntityManager em = getEntityManager();
        try
        {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<FilmCategoryEntity> rt = cq.from(FilmCategoryEntity.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        }
        finally
        {
            em.close();
        }
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ar.edu.utnfc.argprog.data.daos;

import ar.edu.utnfc.argprog.data.daos.exceptions.IllegalOrphanException;
import ar.edu.utnfc.argprog.data.daos.exceptions.NonexistentEntityException;
import java.io.Serializable;
import jakarta.persistence.Query;
import jakarta.persistence.EntityNotFoundException;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Root;
import ar.edu.utnfc.argprog.data.entities.StoreEntity;
import ar.edu.utnfc.argprog.data.entities.AddressEntity;
import ar.edu.utnfc.argprog.data.entities.CustomerEntity;
import ar.edu.utnfc.argprog.data.entities.PaymentEntity;
import java.util.ArrayList;
import java.util.Collection;
import ar.edu.utnfc.argprog.data.entities.RentalEntity;
import java.util.List;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;

/**
 *
 * @author Felipe
 */
public class CustomerDao implements Serializable
{

    public CustomerDao(EntityManagerFactory emf)
    {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager()
    {
        return emf.createEntityManager();
    }

    public void create(CustomerEntity customerEntity)
    {
        if (customerEntity.getPaymentEntityCollection() == null)
        {
            customerEntity.setPaymentEntityCollection(new ArrayList<PaymentEntity>());
        }
        if (customerEntity.getRentalEntityCollection() == null)
        {
            customerEntity.setRentalEntityCollection(new ArrayList<RentalEntity>());
        }
        EntityManager em = null;
        try
        {
            em = getEntityManager();
            em.getTransaction().begin();
            StoreEntity storeEntity = customerEntity.getStoreEntity();
            if (storeEntity != null)
            {
                storeEntity = em.getReference(storeEntity.getClass(), storeEntity.getStoreId());
                customerEntity.setStoreEntity(storeEntity);
            }
            AddressEntity addressEntity = customerEntity.getAddressEntity();
            if (addressEntity != null)
            {
                addressEntity = em.getReference(addressEntity.getClass(), addressEntity.getAddressId());
                customerEntity.setAddressEntity(addressEntity);
            }
            Collection<PaymentEntity> attachedPaymentEntityCollection = new ArrayList<PaymentEntity>();
            for (PaymentEntity paymentEntityCollectionPaymentEntityToAttach : customerEntity.getPaymentEntityCollection())
            {
                paymentEntityCollectionPaymentEntityToAttach = em.getReference(paymentEntityCollectionPaymentEntityToAttach.getClass(), paymentEntityCollectionPaymentEntityToAttach.getPaymentId());
                attachedPaymentEntityCollection.add(paymentEntityCollectionPaymentEntityToAttach);
            }
            customerEntity.setPaymentEntityCollection(attachedPaymentEntityCollection);
            Collection<RentalEntity> attachedRentalEntityCollection = new ArrayList<RentalEntity>();
            for (RentalEntity rentalEntityCollectionRentalEntityToAttach : customerEntity.getRentalEntityCollection())
            {
                rentalEntityCollectionRentalEntityToAttach = em.getReference(rentalEntityCollectionRentalEntityToAttach.getClass(), rentalEntityCollectionRentalEntityToAttach.getRentalId());
                attachedRentalEntityCollection.add(rentalEntityCollectionRentalEntityToAttach);
            }
            customerEntity.setRentalEntityCollection(attachedRentalEntityCollection);
            em.persist(customerEntity);
            if (storeEntity != null)
            {
                storeEntity.getCustomerEntityCollection().add(customerEntity);
                storeEntity = em.merge(storeEntity);
            }
            if (addressEntity != null)
            {
                addressEntity.getCustomerEntityCollection().add(customerEntity);
                addressEntity = em.merge(addressEntity);
            }
            for (PaymentEntity paymentEntityCollectionPaymentEntity : customerEntity.getPaymentEntityCollection())
            {
                CustomerEntity oldCustomerEntityOfPaymentEntityCollectionPaymentEntity = paymentEntityCollectionPaymentEntity.getCustomerEntity();
                paymentEntityCollectionPaymentEntity.setCustomerEntity(customerEntity);
                paymentEntityCollectionPaymentEntity = em.merge(paymentEntityCollectionPaymentEntity);
                if (oldCustomerEntityOfPaymentEntityCollectionPaymentEntity != null)
                {
                    oldCustomerEntityOfPaymentEntityCollectionPaymentEntity.getPaymentEntityCollection().remove(paymentEntityCollectionPaymentEntity);
                    oldCustomerEntityOfPaymentEntityCollectionPaymentEntity = em.merge(oldCustomerEntityOfPaymentEntityCollectionPaymentEntity);
                }
            }
            for (RentalEntity rentalEntityCollectionRentalEntity : customerEntity.getRentalEntityCollection())
            {
                CustomerEntity oldCustomerEntityOfRentalEntityCollectionRentalEntity = rentalEntityCollectionRentalEntity.getCustomerEntity();
                rentalEntityCollectionRentalEntity.setCustomerEntity(customerEntity);
                rentalEntityCollectionRentalEntity = em.merge(rentalEntityCollectionRentalEntity);
                if (oldCustomerEntityOfRentalEntityCollectionRentalEntity != null)
                {
                    oldCustomerEntityOfRentalEntityCollectionRentalEntity.getRentalEntityCollection().remove(rentalEntityCollectionRentalEntity);
                    oldCustomerEntityOfRentalEntityCollectionRentalEntity = em.merge(oldCustomerEntityOfRentalEntityCollectionRentalEntity);
                }
            }
            em.getTransaction().commit();
        }
        finally
        {
            if (em != null)
            {
                em.close();
            }
        }
    }

    public void edit(CustomerEntity customerEntity) throws IllegalOrphanException, NonexistentEntityException, Exception
    {
        EntityManager em = null;
        try
        {
            em = getEntityManager();
            em.getTransaction().begin();
            CustomerEntity persistentCustomerEntity = em.find(CustomerEntity.class, customerEntity.getCustomerId());
            StoreEntity storeEntityOld = persistentCustomerEntity.getStoreEntity();
            StoreEntity storeEntityNew = customerEntity.getStoreEntity();
            AddressEntity addressEntityOld = persistentCustomerEntity.getAddressEntity();
            AddressEntity addressEntityNew = customerEntity.getAddressEntity();
            Collection<PaymentEntity> paymentEntityCollectionOld = persistentCustomerEntity.getPaymentEntityCollection();
            Collection<PaymentEntity> paymentEntityCollectionNew = customerEntity.getPaymentEntityCollection();
            Collection<RentalEntity> rentalEntityCollectionOld = persistentCustomerEntity.getRentalEntityCollection();
            Collection<RentalEntity> rentalEntityCollectionNew = customerEntity.getRentalEntityCollection();
            List<String> illegalOrphanMessages = null;
            for (PaymentEntity paymentEntityCollectionOldPaymentEntity : paymentEntityCollectionOld)
            {
                if (!paymentEntityCollectionNew.contains(paymentEntityCollectionOldPaymentEntity))
                {
                    if (illegalOrphanMessages == null)
                    {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain PaymentEntity " + paymentEntityCollectionOldPaymentEntity + " since its customerEntity field is not nullable.");
                }
            }
            for (RentalEntity rentalEntityCollectionOldRentalEntity : rentalEntityCollectionOld)
            {
                if (!rentalEntityCollectionNew.contains(rentalEntityCollectionOldRentalEntity))
                {
                    if (illegalOrphanMessages == null)
                    {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain RentalEntity " + rentalEntityCollectionOldRentalEntity + " since its customerEntity field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null)
            {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (storeEntityNew != null)
            {
                storeEntityNew = em.getReference(storeEntityNew.getClass(), storeEntityNew.getStoreId());
                customerEntity.setStoreEntity(storeEntityNew);
            }
            if (addressEntityNew != null)
            {
                addressEntityNew = em.getReference(addressEntityNew.getClass(), addressEntityNew.getAddressId());
                customerEntity.setAddressEntity(addressEntityNew);
            }
            Collection<PaymentEntity> attachedPaymentEntityCollectionNew = new ArrayList<PaymentEntity>();
            for (PaymentEntity paymentEntityCollectionNewPaymentEntityToAttach : paymentEntityCollectionNew)
            {
                paymentEntityCollectionNewPaymentEntityToAttach = em.getReference(paymentEntityCollectionNewPaymentEntityToAttach.getClass(), paymentEntityCollectionNewPaymentEntityToAttach.getPaymentId());
                attachedPaymentEntityCollectionNew.add(paymentEntityCollectionNewPaymentEntityToAttach);
            }
            paymentEntityCollectionNew = attachedPaymentEntityCollectionNew;
            customerEntity.setPaymentEntityCollection(paymentEntityCollectionNew);
            Collection<RentalEntity> attachedRentalEntityCollectionNew = new ArrayList<RentalEntity>();
            for (RentalEntity rentalEntityCollectionNewRentalEntityToAttach : rentalEntityCollectionNew)
            {
                rentalEntityCollectionNewRentalEntityToAttach = em.getReference(rentalEntityCollectionNewRentalEntityToAttach.getClass(), rentalEntityCollectionNewRentalEntityToAttach.getRentalId());
                attachedRentalEntityCollectionNew.add(rentalEntityCollectionNewRentalEntityToAttach);
            }
            rentalEntityCollectionNew = attachedRentalEntityCollectionNew;
            customerEntity.setRentalEntityCollection(rentalEntityCollectionNew);
            customerEntity = em.merge(customerEntity);
            if (storeEntityOld != null && !storeEntityOld.equals(storeEntityNew))
            {
                storeEntityOld.getCustomerEntityCollection().remove(customerEntity);
                storeEntityOld = em.merge(storeEntityOld);
            }
            if (storeEntityNew != null && !storeEntityNew.equals(storeEntityOld))
            {
                storeEntityNew.getCustomerEntityCollection().add(customerEntity);
                storeEntityNew = em.merge(storeEntityNew);
            }
            if (addressEntityOld != null && !addressEntityOld.equals(addressEntityNew))
            {
                addressEntityOld.getCustomerEntityCollection().remove(customerEntity);
                addressEntityOld = em.merge(addressEntityOld);
            }
            if (addressEntityNew != null && !addressEntityNew.equals(addressEntityOld))
            {
                addressEntityNew.getCustomerEntityCollection().add(customerEntity);
                addressEntityNew = em.merge(addressEntityNew);
            }
            for (PaymentEntity paymentEntityCollectionNewPaymentEntity : paymentEntityCollectionNew)
            {
                if (!paymentEntityCollectionOld.contains(paymentEntityCollectionNewPaymentEntity))
                {
                    CustomerEntity oldCustomerEntityOfPaymentEntityCollectionNewPaymentEntity = paymentEntityCollectionNewPaymentEntity.getCustomerEntity();
                    paymentEntityCollectionNewPaymentEntity.setCustomerEntity(customerEntity);
                    paymentEntityCollectionNewPaymentEntity = em.merge(paymentEntityCollectionNewPaymentEntity);
                    if (oldCustomerEntityOfPaymentEntityCollectionNewPaymentEntity != null && !oldCustomerEntityOfPaymentEntityCollectionNewPaymentEntity.equals(customerEntity))
                    {
                        oldCustomerEntityOfPaymentEntityCollectionNewPaymentEntity.getPaymentEntityCollection().remove(paymentEntityCollectionNewPaymentEntity);
                        oldCustomerEntityOfPaymentEntityCollectionNewPaymentEntity = em.merge(oldCustomerEntityOfPaymentEntityCollectionNewPaymentEntity);
                    }
                }
            }
            for (RentalEntity rentalEntityCollectionNewRentalEntity : rentalEntityCollectionNew)
            {
                if (!rentalEntityCollectionOld.contains(rentalEntityCollectionNewRentalEntity))
                {
                    CustomerEntity oldCustomerEntityOfRentalEntityCollectionNewRentalEntity = rentalEntityCollectionNewRentalEntity.getCustomerEntity();
                    rentalEntityCollectionNewRentalEntity.setCustomerEntity(customerEntity);
                    rentalEntityCollectionNewRentalEntity = em.merge(rentalEntityCollectionNewRentalEntity);
                    if (oldCustomerEntityOfRentalEntityCollectionNewRentalEntity != null && !oldCustomerEntityOfRentalEntityCollectionNewRentalEntity.equals(customerEntity))
                    {
                        oldCustomerEntityOfRentalEntityCollectionNewRentalEntity.getRentalEntityCollection().remove(rentalEntityCollectionNewRentalEntity);
                        oldCustomerEntityOfRentalEntityCollectionNewRentalEntity = em.merge(oldCustomerEntityOfRentalEntityCollectionNewRentalEntity);
                    }
                }
            }
            em.getTransaction().commit();
        }
        catch (Exception ex)
        {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0)
            {
                Short id = customerEntity.getCustomerId();
                if (findCustomerEntity(id) == null)
                {
                    throw new NonexistentEntityException("The customerEntity with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
        finally
        {
            if (em != null)
            {
                em.close();
            }
        }
    }

    public void destroy(Short id) throws IllegalOrphanException, NonexistentEntityException
    {
        EntityManager em = null;
        try
        {
            em = getEntityManager();
            em.getTransaction().begin();
            CustomerEntity customerEntity;
            try
            {
                customerEntity = em.getReference(CustomerEntity.class, id);
                customerEntity.getCustomerId();
            }
            catch (EntityNotFoundException enfe)
            {
                throw new NonexistentEntityException("The customerEntity with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<PaymentEntity> paymentEntityCollectionOrphanCheck = customerEntity.getPaymentEntityCollection();
            for (PaymentEntity paymentEntityCollectionOrphanCheckPaymentEntity : paymentEntityCollectionOrphanCheck)
            {
                if (illegalOrphanMessages == null)
                {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This CustomerEntity (" + customerEntity + ") cannot be destroyed since the PaymentEntity " + paymentEntityCollectionOrphanCheckPaymentEntity + " in its paymentEntityCollection field has a non-nullable customerEntity field.");
            }
            Collection<RentalEntity> rentalEntityCollectionOrphanCheck = customerEntity.getRentalEntityCollection();
            for (RentalEntity rentalEntityCollectionOrphanCheckRentalEntity : rentalEntityCollectionOrphanCheck)
            {
                if (illegalOrphanMessages == null)
                {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This CustomerEntity (" + customerEntity + ") cannot be destroyed since the RentalEntity " + rentalEntityCollectionOrphanCheckRentalEntity + " in its rentalEntityCollection field has a non-nullable customerEntity field.");
            }
            if (illegalOrphanMessages != null)
            {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            StoreEntity storeEntity = customerEntity.getStoreEntity();
            if (storeEntity != null)
            {
                storeEntity.getCustomerEntityCollection().remove(customerEntity);
                storeEntity = em.merge(storeEntity);
            }
            AddressEntity addressEntity = customerEntity.getAddressEntity();
            if (addressEntity != null)
            {
                addressEntity.getCustomerEntityCollection().remove(customerEntity);
                addressEntity = em.merge(addressEntity);
            }
            em.remove(customerEntity);
            em.getTransaction().commit();
        }
        finally
        {
            if (em != null)
            {
                em.close();
            }
        }
    }

    public List<CustomerEntity> findCustomerEntityEntities()
    {
        return findCustomerEntityEntities(true, -1, -1);
    }

    public List<CustomerEntity> findCustomerEntityEntities(int maxResults, int firstResult)
    {
        return findCustomerEntityEntities(false, maxResults, firstResult);
    }

    private List<CustomerEntity> findCustomerEntityEntities(boolean all, int maxResults, int firstResult)
    {
        EntityManager em = getEntityManager();
        try
        {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(CustomerEntity.class));
            Query q = em.createQuery(cq);
            if (!all)
            {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        }
        finally
        {
            em.close();
        }
    }

    public CustomerEntity findCustomerEntity(Short id)
    {
        EntityManager em = getEntityManager();
        try
        {
            return em.find(CustomerEntity.class, id);
        }
        finally
        {
            em.close();
        }
    }

    public int getCustomerEntityCount()
    {
        EntityManager em = getEntityManager();
        try
        {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<CustomerEntity> rt = cq.from(CustomerEntity.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        }
        finally
        {
            em.close();
        }
    }
    
}

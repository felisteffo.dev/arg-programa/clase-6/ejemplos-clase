/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ar.edu.utnfc.argprog.data.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import jakarta.persistence.Basic;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;

/**
 *
 * @author Felipe
 */
@Entity
@Table(name = "inventory")
@NamedQueries(
{
    @NamedQuery(name = "InventoryEntity.findAll", query = "SELECT i FROM InventoryEntity i"),
    @NamedQuery(name = "InventoryEntity.findByInventoryId", query = "SELECT i FROM InventoryEntity i WHERE i.inventoryId = :inventoryId"),
    @NamedQuery(name = "InventoryEntity.findByLastUpdate", query = "SELECT i FROM InventoryEntity i WHERE i.lastUpdate = :lastUpdate")
})
public class InventoryEntity implements Serializable
{
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "inventory_id")
    private Integer inventoryId;
    @Basic(optional = false)
    @Column(name = "last_update")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastUpdate;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "inventoryEntity")
    private Collection<RentalEntity> rentalEntityCollection;
    @JoinColumn(name = "store_id", referencedColumnName = "store_id")
    @ManyToOne(optional = false)
    private StoreEntity storeEntity;
    @JoinColumn(name = "film_id", referencedColumnName = "film_id")
    @ManyToOne(optional = false)
    private FilmEntity filmEntity;

    public InventoryEntity()
    {
    }

    public InventoryEntity(Integer inventoryId)
    {
        this.inventoryId = inventoryId;
    }

    public InventoryEntity(Integer inventoryId, Date lastUpdate)
    {
        this.inventoryId = inventoryId;
        this.lastUpdate = lastUpdate;
    }

    public Integer getInventoryId()
    {
        return inventoryId;
    }

    public void setInventoryId(Integer inventoryId)
    {
        this.inventoryId = inventoryId;
    }

    public Date getLastUpdate()
    {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate)
    {
        this.lastUpdate = lastUpdate;
    }

    public Collection<RentalEntity> getRentalEntityCollection()
    {
        return rentalEntityCollection;
    }

    public void setRentalEntityCollection(Collection<RentalEntity> rentalEntityCollection)
    {
        this.rentalEntityCollection = rentalEntityCollection;
    }

    public StoreEntity getStoreEntity()
    {
        return storeEntity;
    }

    public void setStoreEntity(StoreEntity storeEntity)
    {
        this.storeEntity = storeEntity;
    }

    public FilmEntity getFilmEntity()
    {
        return filmEntity;
    }

    public void setFilmEntity(FilmEntity filmEntity)
    {
        this.filmEntity = filmEntity;
    }

    @Override
    public int hashCode()
    {
        int hash = 0;
        hash += (inventoryId != null ? inventoryId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object)
    {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof InventoryEntity))
        {
            return false;
        }
        InventoryEntity other = (InventoryEntity) object;
        if ((this.inventoryId == null && other.inventoryId != null) || (this.inventoryId != null && !this.inventoryId.equals(other.inventoryId)))
        {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return "ar.edu.utnfc.argprog.data.entities.InventoryEntity[ inventoryId=" + inventoryId + " ]";
    }
    
}

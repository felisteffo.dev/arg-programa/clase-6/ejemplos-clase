/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ar.edu.utnfc.argprog.data.daos;

import ar.edu.utnfc.argprog.data.daos.exceptions.IllegalOrphanException;
import ar.edu.utnfc.argprog.data.daos.exceptions.NonexistentEntityException;
import java.io.Serializable;
import jakarta.persistence.Query;
import jakarta.persistence.EntityNotFoundException;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Root;
import ar.edu.utnfc.argprog.data.entities.FilmEntity;
import ar.edu.utnfc.argprog.data.entities.LanguageEntity;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;

/**
 *
 * @author Felipe
 */
public class LanguageDao implements Serializable
{

    public LanguageDao(EntityManagerFactory emf)
    {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager()
    {
        return emf.createEntityManager();
    }

    public void create(LanguageEntity languageEntity)
    {
        if (languageEntity.getFilmEntityCollection() == null)
        {
            languageEntity.setFilmEntityCollection(new ArrayList<FilmEntity>());
        }
        if (languageEntity.getFilmEntityCollection1() == null)
        {
            languageEntity.setFilmEntityCollection1(new ArrayList<FilmEntity>());
        }
        EntityManager em = null;
        try
        {
            em = getEntityManager();
            em.getTransaction().begin();
            Collection<FilmEntity> attachedFilmEntityCollection = new ArrayList<FilmEntity>();
            for (FilmEntity filmEntityCollectionFilmEntityToAttach : languageEntity.getFilmEntityCollection())
            {
                filmEntityCollectionFilmEntityToAttach = em.getReference(filmEntityCollectionFilmEntityToAttach.getClass(), filmEntityCollectionFilmEntityToAttach.getFilmId());
                attachedFilmEntityCollection.add(filmEntityCollectionFilmEntityToAttach);
            }
            languageEntity.setFilmEntityCollection(attachedFilmEntityCollection);
            Collection<FilmEntity> attachedFilmEntityCollection1 = new ArrayList<FilmEntity>();
            for (FilmEntity filmEntityCollection1FilmEntityToAttach : languageEntity.getFilmEntityCollection1())
            {
                filmEntityCollection1FilmEntityToAttach = em.getReference(filmEntityCollection1FilmEntityToAttach.getClass(), filmEntityCollection1FilmEntityToAttach.getFilmId());
                attachedFilmEntityCollection1.add(filmEntityCollection1FilmEntityToAttach);
            }
            languageEntity.setFilmEntityCollection1(attachedFilmEntityCollection1);
            em.persist(languageEntity);
            for (FilmEntity filmEntityCollectionFilmEntity : languageEntity.getFilmEntityCollection())
            {
                LanguageEntity oldLanguageEntityOfFilmEntityCollectionFilmEntity = filmEntityCollectionFilmEntity.getLanguageEntity();
                filmEntityCollectionFilmEntity.setLanguageEntity(languageEntity);
                filmEntityCollectionFilmEntity = em.merge(filmEntityCollectionFilmEntity);
                if (oldLanguageEntityOfFilmEntityCollectionFilmEntity != null)
                {
                    oldLanguageEntityOfFilmEntityCollectionFilmEntity.getFilmEntityCollection().remove(filmEntityCollectionFilmEntity);
                    oldLanguageEntityOfFilmEntityCollectionFilmEntity = em.merge(oldLanguageEntityOfFilmEntityCollectionFilmEntity);
                }
            }
            for (FilmEntity filmEntityCollection1FilmEntity : languageEntity.getFilmEntityCollection1())
            {
                LanguageEntity oldLanguageEntity1OfFilmEntityCollection1FilmEntity = filmEntityCollection1FilmEntity.getLanguageEntity1();
                filmEntityCollection1FilmEntity.setLanguageEntity1(languageEntity);
                filmEntityCollection1FilmEntity = em.merge(filmEntityCollection1FilmEntity);
                if (oldLanguageEntity1OfFilmEntityCollection1FilmEntity != null)
                {
                    oldLanguageEntity1OfFilmEntityCollection1FilmEntity.getFilmEntityCollection1().remove(filmEntityCollection1FilmEntity);
                    oldLanguageEntity1OfFilmEntityCollection1FilmEntity = em.merge(oldLanguageEntity1OfFilmEntityCollection1FilmEntity);
                }
            }
            em.getTransaction().commit();
        }
        finally
        {
            if (em != null)
            {
                em.close();
            }
        }
    }

    public void edit(LanguageEntity languageEntity) throws IllegalOrphanException, NonexistentEntityException, Exception
    {
        EntityManager em = null;
        try
        {
            em = getEntityManager();
            em.getTransaction().begin();
            LanguageEntity persistentLanguageEntity = em.find(LanguageEntity.class, languageEntity.getLanguageId());
            Collection<FilmEntity> filmEntityCollectionOld = persistentLanguageEntity.getFilmEntityCollection();
            Collection<FilmEntity> filmEntityCollectionNew = languageEntity.getFilmEntityCollection();
            Collection<FilmEntity> filmEntityCollection1Old = persistentLanguageEntity.getFilmEntityCollection1();
            Collection<FilmEntity> filmEntityCollection1New = languageEntity.getFilmEntityCollection1();
            List<String> illegalOrphanMessages = null;
            for (FilmEntity filmEntityCollection1OldFilmEntity : filmEntityCollection1Old)
            {
                if (!filmEntityCollection1New.contains(filmEntityCollection1OldFilmEntity))
                {
                    if (illegalOrphanMessages == null)
                    {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain FilmEntity " + filmEntityCollection1OldFilmEntity + " since its languageEntity1 field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null)
            {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Collection<FilmEntity> attachedFilmEntityCollectionNew = new ArrayList<FilmEntity>();
            for (FilmEntity filmEntityCollectionNewFilmEntityToAttach : filmEntityCollectionNew)
            {
                filmEntityCollectionNewFilmEntityToAttach = em.getReference(filmEntityCollectionNewFilmEntityToAttach.getClass(), filmEntityCollectionNewFilmEntityToAttach.getFilmId());
                attachedFilmEntityCollectionNew.add(filmEntityCollectionNewFilmEntityToAttach);
            }
            filmEntityCollectionNew = attachedFilmEntityCollectionNew;
            languageEntity.setFilmEntityCollection(filmEntityCollectionNew);
            Collection<FilmEntity> attachedFilmEntityCollection1New = new ArrayList<FilmEntity>();
            for (FilmEntity filmEntityCollection1NewFilmEntityToAttach : filmEntityCollection1New)
            {
                filmEntityCollection1NewFilmEntityToAttach = em.getReference(filmEntityCollection1NewFilmEntityToAttach.getClass(), filmEntityCollection1NewFilmEntityToAttach.getFilmId());
                attachedFilmEntityCollection1New.add(filmEntityCollection1NewFilmEntityToAttach);
            }
            filmEntityCollection1New = attachedFilmEntityCollection1New;
            languageEntity.setFilmEntityCollection1(filmEntityCollection1New);
            languageEntity = em.merge(languageEntity);
            for (FilmEntity filmEntityCollectionOldFilmEntity : filmEntityCollectionOld)
            {
                if (!filmEntityCollectionNew.contains(filmEntityCollectionOldFilmEntity))
                {
                    filmEntityCollectionOldFilmEntity.setLanguageEntity(null);
                    filmEntityCollectionOldFilmEntity = em.merge(filmEntityCollectionOldFilmEntity);
                }
            }
            for (FilmEntity filmEntityCollectionNewFilmEntity : filmEntityCollectionNew)
            {
                if (!filmEntityCollectionOld.contains(filmEntityCollectionNewFilmEntity))
                {
                    LanguageEntity oldLanguageEntityOfFilmEntityCollectionNewFilmEntity = filmEntityCollectionNewFilmEntity.getLanguageEntity();
                    filmEntityCollectionNewFilmEntity.setLanguageEntity(languageEntity);
                    filmEntityCollectionNewFilmEntity = em.merge(filmEntityCollectionNewFilmEntity);
                    if (oldLanguageEntityOfFilmEntityCollectionNewFilmEntity != null && !oldLanguageEntityOfFilmEntityCollectionNewFilmEntity.equals(languageEntity))
                    {
                        oldLanguageEntityOfFilmEntityCollectionNewFilmEntity.getFilmEntityCollection().remove(filmEntityCollectionNewFilmEntity);
                        oldLanguageEntityOfFilmEntityCollectionNewFilmEntity = em.merge(oldLanguageEntityOfFilmEntityCollectionNewFilmEntity);
                    }
                }
            }
            for (FilmEntity filmEntityCollection1NewFilmEntity : filmEntityCollection1New)
            {
                if (!filmEntityCollection1Old.contains(filmEntityCollection1NewFilmEntity))
                {
                    LanguageEntity oldLanguageEntity1OfFilmEntityCollection1NewFilmEntity = filmEntityCollection1NewFilmEntity.getLanguageEntity1();
                    filmEntityCollection1NewFilmEntity.setLanguageEntity1(languageEntity);
                    filmEntityCollection1NewFilmEntity = em.merge(filmEntityCollection1NewFilmEntity);
                    if (oldLanguageEntity1OfFilmEntityCollection1NewFilmEntity != null && !oldLanguageEntity1OfFilmEntityCollection1NewFilmEntity.equals(languageEntity))
                    {
                        oldLanguageEntity1OfFilmEntityCollection1NewFilmEntity.getFilmEntityCollection1().remove(filmEntityCollection1NewFilmEntity);
                        oldLanguageEntity1OfFilmEntityCollection1NewFilmEntity = em.merge(oldLanguageEntity1OfFilmEntityCollection1NewFilmEntity);
                    }
                }
            }
            em.getTransaction().commit();
        }
        catch (Exception ex)
        {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0)
            {
                Short id = languageEntity.getLanguageId();
                if (findLanguageEntity(id) == null)
                {
                    throw new NonexistentEntityException("The languageEntity with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
        finally
        {
            if (em != null)
            {
                em.close();
            }
        }
    }

    public void destroy(Short id) throws IllegalOrphanException, NonexistentEntityException
    {
        EntityManager em = null;
        try
        {
            em = getEntityManager();
            em.getTransaction().begin();
            LanguageEntity languageEntity;
            try
            {
                languageEntity = em.getReference(LanguageEntity.class, id);
                languageEntity.getLanguageId();
            }
            catch (EntityNotFoundException enfe)
            {
                throw new NonexistentEntityException("The languageEntity with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<FilmEntity> filmEntityCollection1OrphanCheck = languageEntity.getFilmEntityCollection1();
            for (FilmEntity filmEntityCollection1OrphanCheckFilmEntity : filmEntityCollection1OrphanCheck)
            {
                if (illegalOrphanMessages == null)
                {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This LanguageEntity (" + languageEntity + ") cannot be destroyed since the FilmEntity " + filmEntityCollection1OrphanCheckFilmEntity + " in its filmEntityCollection1 field has a non-nullable languageEntity1 field.");
            }
            if (illegalOrphanMessages != null)
            {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Collection<FilmEntity> filmEntityCollection = languageEntity.getFilmEntityCollection();
            for (FilmEntity filmEntityCollectionFilmEntity : filmEntityCollection)
            {
                filmEntityCollectionFilmEntity.setLanguageEntity(null);
                filmEntityCollectionFilmEntity = em.merge(filmEntityCollectionFilmEntity);
            }
            em.remove(languageEntity);
            em.getTransaction().commit();
        }
        finally
        {
            if (em != null)
            {
                em.close();
            }
        }
    }

    public List<LanguageEntity> findLanguageEntityEntities()
    {
        return findLanguageEntityEntities(true, -1, -1);
    }

    public List<LanguageEntity> findLanguageEntityEntities(int maxResults, int firstResult)
    {
        return findLanguageEntityEntities(false, maxResults, firstResult);
    }

    private List<LanguageEntity> findLanguageEntityEntities(boolean all, int maxResults, int firstResult)
    {
        EntityManager em = getEntityManager();
        try
        {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(LanguageEntity.class));
            Query q = em.createQuery(cq);
            if (!all)
            {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        }
        finally
        {
            em.close();
        }
    }

    public LanguageEntity findLanguageEntity(Short id)
    {
        EntityManager em = getEntityManager();
        try
        {
            return em.find(LanguageEntity.class, id);
        }
        finally
        {
            em.close();
        }
    }

    public int getLanguageEntityCount()
    {
        EntityManager em = getEntityManager();
        try
        {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<LanguageEntity> rt = cq.from(LanguageEntity.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        }
        finally
        {
            em.close();
        }
    }
    
}

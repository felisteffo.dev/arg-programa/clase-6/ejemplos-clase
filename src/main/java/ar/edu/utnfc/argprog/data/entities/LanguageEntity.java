/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ar.edu.utnfc.argprog.data.entities;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Date;
import jakarta.persistence.*;
import jakarta.persistence.Basic;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;

/**
 *
 * @author Felipe
 */
@Entity
@Table(name = "language")
@NamedQueries(
{
    @NamedQuery(name = "LanguageEntity.findAll", query = "SELECT l FROM LanguageEntity l"),
    @NamedQuery(name = "LanguageEntity.findByLanguageId", query = "SELECT l FROM LanguageEntity l WHERE l.languageId = :languageId"),
    @NamedQuery(name = "LanguageEntity.findByName", query = "SELECT l FROM LanguageEntity l WHERE l.name = :name")
})

public class LanguageEntity implements Serializable
{
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "language_id")
    private Short languageId;
    @Basic(optional = false)
    private String name;
    @Basic(optional = false)
    @Column(name = "last_update")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastUpdate;
    @OneToMany(mappedBy = "languageEntity")
    private Collection<FilmEntity> filmEntityCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "languageEntity1")
    private Collection<FilmEntity> filmEntityCollection1;

    public LanguageEntity()
    {
        this.lastUpdate = new Date();
    }

    public LanguageEntity(Short languageId)
    {
        this.languageId = languageId;
    }

    public LanguageEntity(Short languageId, String name, Date lastUpdate)
    {
        this.languageId = languageId;
        this.name = name;
        this.lastUpdate = lastUpdate;
    }

    public Short getLanguageId()
    {
        return languageId;
    }

    public void setLanguageId(Short languageId)
    {
        this.languageId = languageId;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public Date getLastUpdate()
    {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate)
    {
        this.lastUpdate = lastUpdate;
    }

    public Collection<FilmEntity> getFilmEntityCollection()
    {
        return filmEntityCollection;
    }

    public void setFilmEntityCollection(Collection<FilmEntity> filmEntityCollection)
    {
        this.filmEntityCollection = filmEntityCollection;
    }

    public Collection<FilmEntity> getFilmEntityCollection1()
    {
        return filmEntityCollection1;
    }

    public void setFilmEntityCollection1(Collection<FilmEntity> filmEntityCollection1)
    {
        this.filmEntityCollection1 = filmEntityCollection1;
    }

    @Override
    public int hashCode()
    {
        int hash = 0;
        hash += (languageId != null ? languageId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object)
    {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LanguageEntity))
        {
            return false;
        }
        LanguageEntity other = (LanguageEntity) object;
        if ((this.languageId == null && other.languageId != null) || (this.languageId != null && !this.languageId.equals(other.languageId)))
        {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return "LanguageEntity{" + "languageId=" + languageId + ", name=" + name + '}';
    }

    
    
}

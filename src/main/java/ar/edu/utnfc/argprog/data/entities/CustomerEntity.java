/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ar.edu.utnfc.argprog.data.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import jakarta.persistence.*;

/**
 *
 * @author Felipe
 */
@Entity
@Table(name = "customer")
@NamedQueries(
{
    @NamedQuery(name = "CustomerEntity.findAll", query = "SELECT c FROM CustomerEntity c"),
    @NamedQuery(name = "CustomerEntity.findByCustomerId", query = "SELECT c FROM CustomerEntity c WHERE c.customerId = :customerId"),
    @NamedQuery(name = "CustomerEntity.findByFirstName", query = "SELECT c FROM CustomerEntity c WHERE c.firstName = :firstName"),
    @NamedQuery(name = "CustomerEntity.findByLastName", query = "SELECT c FROM CustomerEntity c WHERE c.lastName = :lastName"),
    @NamedQuery(name = "CustomerEntity.findByEmail", query = "SELECT c FROM CustomerEntity c WHERE c.email = :email"),
    @NamedQuery(name = "CustomerEntity.findByActive", query = "SELECT c FROM CustomerEntity c WHERE c.active = :active"),
    @NamedQuery(name = "CustomerEntity.findByCreateDate", query = "SELECT c FROM CustomerEntity c WHERE c.createDate = :createDate"),
    @NamedQuery(name = "CustomerEntity.findByLastUpdate", query = "SELECT c FROM CustomerEntity c WHERE c.lastUpdate = :lastUpdate")
})
@NamedNativeQueries
({
    @NamedNativeQuery(name = "CustomerEntity.findByCountryNameNative", query = "SELECT " +
                        "    t3.customer_id, " +
                        "    t3.ACTIVE, " +
                        "    t3.create_date, " +
                        "    t3.EMAIL, " +
                        "    t3.first_name, " +
                        "    t3.last_name, " +
                        "    t3.last_update, " +
                        "    t3.address_id, " +
                        "    t3.store_id " +
                        "  FROM customer t3" +
                        "	inner join address t2 on t3.address_id = t2.address_id" +
                        "    inner join city t1 on t2.city_id = t1.city_id" +
                        "    inner join country t0 on t1.country_id = t0.country_id" +
                        "  WHERE t0.COUNTRY LIKE ?", resultClass = CustomerEntity.class)
                        })
public class CustomerEntity implements Serializable
{
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "customer_id")
    private Short customerId;
    @Basic(optional = false)
    @Column(name = "first_name")
    private String firstName;
    @Basic(optional = false)
    @Column(name = "last_name")
    private String lastName;
    private String email;
    @Basic(optional = false)
    private boolean active;
    @Basic(optional = false)
    @Column(name = "create_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Basic(optional = false)
    @Column(name = "last_update")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastUpdate;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "customerEntity")
    private Collection<PaymentEntity> paymentEntityCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "customerEntity")
    private Collection<RentalEntity> rentalEntityCollection;
    @JoinColumn(name = "store_id", referencedColumnName = "store_id")
    @ManyToOne(optional = false)
    private StoreEntity storeEntity;
    @JoinColumn(name = "address_id", referencedColumnName = "address_id")
    @ManyToOne(optional = false)
    private AddressEntity addressEntity;

    public CustomerEntity()
    {
    }

    public CustomerEntity(Short customerId)
    {
        this.customerId = customerId;
    }

    public CustomerEntity(Short customerId, String firstName, String lastName, boolean active, Date createDate, Date lastUpdate)
    {
        this.customerId = customerId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.active = active;
        this.createDate = createDate;
        this.lastUpdate = lastUpdate;
    }

    public Short getCustomerId()
    {
        return customerId;
    }

    public void setCustomerId(Short customerId)
    {
        this.customerId = customerId;
    }

    public String getFirstName()
    {
        return firstName;
    }

    public void setFirstName(String firstName)
    {
        this.firstName = firstName;
    }

    public String getLastName()
    {
        return lastName;
    }

    public void setLastName(String lastName)
    {
        this.lastName = lastName;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public boolean getActive()
    {
        return active;
    }

    public void setActive(boolean active)
    {
        this.active = active;
    }

    public Date getCreateDate()
    {
        return createDate;
    }

    public void setCreateDate(Date createDate)
    {
        this.createDate = createDate;
    }

    public Date getLastUpdate()
    {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate)
    {
        this.lastUpdate = lastUpdate;
    }

    public Collection<PaymentEntity> getPaymentEntityCollection()
    {
        return paymentEntityCollection;
    }

    public void setPaymentEntityCollection(Collection<PaymentEntity> paymentEntityCollection)
    {
        this.paymentEntityCollection = paymentEntityCollection;
    }

    public Collection<RentalEntity> getRentalEntityCollection()
    {
        return rentalEntityCollection;
    }

    public void setRentalEntityCollection(Collection<RentalEntity> rentalEntityCollection)
    {
        this.rentalEntityCollection = rentalEntityCollection;
    }

    public StoreEntity getStoreEntity()
    {
        return storeEntity;
    }

    public void setStoreEntity(StoreEntity storeEntity)
    {
        this.storeEntity = storeEntity;
    }

    public AddressEntity getAddressEntity()
    {
        return addressEntity;
    }

    public void setAddressEntity(AddressEntity addressEntity)
    {
        this.addressEntity = addressEntity;
    }

    @Override
    public int hashCode()
    {
        int hash = 0;
        hash += (customerId != null ? customerId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object)
    {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CustomerEntity))
        {
            return false;
        }
        CustomerEntity other = (CustomerEntity) object;
        if ((this.customerId == null && other.customerId != null) || (this.customerId != null && !this.customerId.equals(other.customerId)))
        {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return "ar.edu.utnfc.argprog.data.entities.CustomerEntity[ customerId=" + customerId + " ]";
    }
    
}

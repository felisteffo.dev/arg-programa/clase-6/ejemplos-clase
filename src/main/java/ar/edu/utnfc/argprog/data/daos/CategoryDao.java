/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ar.edu.utnfc.argprog.data.daos;

import ar.edu.utnfc.argprog.data.daos.exceptions.IllegalOrphanException;
import ar.edu.utnfc.argprog.data.daos.exceptions.NonexistentEntityException;
import ar.edu.utnfc.argprog.data.entities.CategoryEntity;
import java.io.Serializable;
import jakarta.persistence.Query;
import jakarta.persistence.EntityNotFoundException;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Root;
import ar.edu.utnfc.argprog.data.entities.FilmCategoryEntity;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;

/**
 *
 * @author Felipe
 */
public class CategoryDao implements Serializable
{

    public CategoryDao(EntityManagerFactory emf)
    {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager()
    {
        return emf.createEntityManager();
    }

    public void create(CategoryEntity categoryEntity)
    {
        if (categoryEntity.getFilmCategoryEntityCollection() == null)
        {
            categoryEntity.setFilmCategoryEntityCollection(new ArrayList<FilmCategoryEntity>());
        }
        EntityManager em = null;
        try
        {
            em = getEntityManager();
            em.getTransaction().begin();
            Collection<FilmCategoryEntity> attachedFilmCategoryEntityCollection = new ArrayList<FilmCategoryEntity>();
            for (FilmCategoryEntity filmCategoryEntityCollectionFilmCategoryEntityToAttach : categoryEntity.getFilmCategoryEntityCollection())
            {
                filmCategoryEntityCollectionFilmCategoryEntityToAttach = em.getReference(filmCategoryEntityCollectionFilmCategoryEntityToAttach.getClass(), filmCategoryEntityCollectionFilmCategoryEntityToAttach.getFilmCategoryEntityPK());
                attachedFilmCategoryEntityCollection.add(filmCategoryEntityCollectionFilmCategoryEntityToAttach);
            }
            categoryEntity.setFilmCategoryEntityCollection(attachedFilmCategoryEntityCollection);
            em.persist(categoryEntity);
            for (FilmCategoryEntity filmCategoryEntityCollectionFilmCategoryEntity : categoryEntity.getFilmCategoryEntityCollection())
            {
                CategoryEntity oldCategoryEntityOfFilmCategoryEntityCollectionFilmCategoryEntity = filmCategoryEntityCollectionFilmCategoryEntity.getCategoryEntity();
                filmCategoryEntityCollectionFilmCategoryEntity.setCategoryEntity(categoryEntity);
                filmCategoryEntityCollectionFilmCategoryEntity = em.merge(filmCategoryEntityCollectionFilmCategoryEntity);
                if (oldCategoryEntityOfFilmCategoryEntityCollectionFilmCategoryEntity != null)
                {
                    oldCategoryEntityOfFilmCategoryEntityCollectionFilmCategoryEntity.getFilmCategoryEntityCollection().remove(filmCategoryEntityCollectionFilmCategoryEntity);
                    oldCategoryEntityOfFilmCategoryEntityCollectionFilmCategoryEntity = em.merge(oldCategoryEntityOfFilmCategoryEntityCollectionFilmCategoryEntity);
                }
            }
            em.getTransaction().commit();
        }
        finally
        {
            if (em != null)
            {
                em.close();
            }
        }
    }

    public void edit(CategoryEntity categoryEntity) throws IllegalOrphanException, NonexistentEntityException, Exception
    {
        EntityManager em = null;
        try
        {
            em = getEntityManager();
            em.getTransaction().begin();
            CategoryEntity persistentCategoryEntity = em.find(CategoryEntity.class, categoryEntity.getCategoryId());
            Collection<FilmCategoryEntity> filmCategoryEntityCollectionOld = persistentCategoryEntity.getFilmCategoryEntityCollection();
            Collection<FilmCategoryEntity> filmCategoryEntityCollectionNew = categoryEntity.getFilmCategoryEntityCollection();
            List<String> illegalOrphanMessages = null;
            for (FilmCategoryEntity filmCategoryEntityCollectionOldFilmCategoryEntity : filmCategoryEntityCollectionOld)
            {
                if (!filmCategoryEntityCollectionNew.contains(filmCategoryEntityCollectionOldFilmCategoryEntity))
                {
                    if (illegalOrphanMessages == null)
                    {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain FilmCategoryEntity " + filmCategoryEntityCollectionOldFilmCategoryEntity + " since its categoryEntity field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null)
            {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Collection<FilmCategoryEntity> attachedFilmCategoryEntityCollectionNew = new ArrayList<FilmCategoryEntity>();
            for (FilmCategoryEntity filmCategoryEntityCollectionNewFilmCategoryEntityToAttach : filmCategoryEntityCollectionNew)
            {
                filmCategoryEntityCollectionNewFilmCategoryEntityToAttach = em.getReference(filmCategoryEntityCollectionNewFilmCategoryEntityToAttach.getClass(), filmCategoryEntityCollectionNewFilmCategoryEntityToAttach.getFilmCategoryEntityPK());
                attachedFilmCategoryEntityCollectionNew.add(filmCategoryEntityCollectionNewFilmCategoryEntityToAttach);
            }
            filmCategoryEntityCollectionNew = attachedFilmCategoryEntityCollectionNew;
            categoryEntity.setFilmCategoryEntityCollection(filmCategoryEntityCollectionNew);
            categoryEntity = em.merge(categoryEntity);
            for (FilmCategoryEntity filmCategoryEntityCollectionNewFilmCategoryEntity : filmCategoryEntityCollectionNew)
            {
                if (!filmCategoryEntityCollectionOld.contains(filmCategoryEntityCollectionNewFilmCategoryEntity))
                {
                    CategoryEntity oldCategoryEntityOfFilmCategoryEntityCollectionNewFilmCategoryEntity = filmCategoryEntityCollectionNewFilmCategoryEntity.getCategoryEntity();
                    filmCategoryEntityCollectionNewFilmCategoryEntity.setCategoryEntity(categoryEntity);
                    filmCategoryEntityCollectionNewFilmCategoryEntity = em.merge(filmCategoryEntityCollectionNewFilmCategoryEntity);
                    if (oldCategoryEntityOfFilmCategoryEntityCollectionNewFilmCategoryEntity != null && !oldCategoryEntityOfFilmCategoryEntityCollectionNewFilmCategoryEntity.equals(categoryEntity))
                    {
                        oldCategoryEntityOfFilmCategoryEntityCollectionNewFilmCategoryEntity.getFilmCategoryEntityCollection().remove(filmCategoryEntityCollectionNewFilmCategoryEntity);
                        oldCategoryEntityOfFilmCategoryEntityCollectionNewFilmCategoryEntity = em.merge(oldCategoryEntityOfFilmCategoryEntityCollectionNewFilmCategoryEntity);
                    }
                }
            }
            em.getTransaction().commit();
        }
        catch (Exception ex)
        {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0)
            {
                Short id = categoryEntity.getCategoryId();
                if (findCategoryEntity(id) == null)
                {
                    throw new NonexistentEntityException("The categoryEntity with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
        finally
        {
            if (em != null)
            {
                em.close();
            }
        }
    }

    public void destroy(Short id) throws IllegalOrphanException, NonexistentEntityException
    {
        EntityManager em = null;
        try
        {
            em = getEntityManager();
            em.getTransaction().begin();
            CategoryEntity categoryEntity;
            try
            {
                categoryEntity = em.getReference(CategoryEntity.class, id);
                categoryEntity.getCategoryId();
            }
            catch (EntityNotFoundException enfe)
            {
                throw new NonexistentEntityException("The categoryEntity with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<FilmCategoryEntity> filmCategoryEntityCollectionOrphanCheck = categoryEntity.getFilmCategoryEntityCollection();
            for (FilmCategoryEntity filmCategoryEntityCollectionOrphanCheckFilmCategoryEntity : filmCategoryEntityCollectionOrphanCheck)
            {
                if (illegalOrphanMessages == null)
                {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This CategoryEntity (" + categoryEntity + ") cannot be destroyed since the FilmCategoryEntity " + filmCategoryEntityCollectionOrphanCheckFilmCategoryEntity + " in its filmCategoryEntityCollection field has a non-nullable categoryEntity field.");
            }
            if (illegalOrphanMessages != null)
            {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(categoryEntity);
            em.getTransaction().commit();
        }
        finally
        {
            if (em != null)
            {
                em.close();
            }
        }
    }

    public List<CategoryEntity> findCategoryEntityEntities()
    {
        return findCategoryEntityEntities(true, -1, -1);
    }

    public List<CategoryEntity> findCategoryEntityEntities(int maxResults, int firstResult)
    {
        return findCategoryEntityEntities(false, maxResults, firstResult);
    }

    private List<CategoryEntity> findCategoryEntityEntities(boolean all, int maxResults, int firstResult)
    {
        EntityManager em = getEntityManager();
        try
        {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(CategoryEntity.class));
            Query q = em.createQuery(cq);
            if (!all)
            {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        }
        finally
        {
            em.close();
        }
    }

    public CategoryEntity findCategoryEntity(Short id)
    {
        EntityManager em = getEntityManager();
        try
        {
            return em.find(CategoryEntity.class, id);
        }
        finally
        {
            em.close();
        }
    }

    public int getCategoryEntityCount()
    {
        EntityManager em = getEntityManager();
        try
        {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<CategoryEntity> rt = cq.from(CategoryEntity.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        }
        finally
        {
            em.close();
        }
    }
    
}
